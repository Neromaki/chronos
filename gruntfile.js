module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
          development: {
            options: {
              compress: true,
              yuicompress: true,
              optimization: 2
            },
            files: {
              // target.css file: source.less file
              'css/general.css': 'css/less/general.less'
            }
          }
        },
        uglify: {
          development: {
            files: {
              'js/min/custom.min.js': 'js/custom.js'
            }
          }
        },
        watch: {
          development: {
            files: ['css/*', 'js/*'],
            tasks: ['less', 'uglify']
          }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['uglify:development', 'less:development', 'watch:development']);
};