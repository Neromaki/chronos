// If installing Chronos in a subdir (e.g. www.yoursite.com/a_sub_directory), specify it here (former would be '/a_sub_directory')
var subdir = '';
// Otherwise, it will try to detect the most common possible subdirs, and fall back to using root if not
// If any of the AJAX functions do not work, this being incorrectly set up will probably be the cause.
if(window.location.pathname.indexOf('chronos') > -1) { subdir = '/chronos'; }
if(window.location.pathname.indexOf('timesheet') > -1) { subdir = '/timesheet'; }

// ..add in a fix for Internet Explorer (obviously..)
if (!window.location.origin) {
  window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
}

window.base_url = window.location.origin + subdir;

var globalProjects = "";
var globalProjectDescs = "";

function setGlobalProjects(projects) {
	globalProjects = projects;
}


function setglobalProjectDescs(projectDescs) {
	globalProjectDescs = projectDescs;
}

	
$(document).ready(function () {
	

	jQuery.ajax({
	  type: "POST",
	  url: window.base_url + "/projects/functions/getProjectNames"
	})
	  .success(function( data ) {
	  	globalProjects = JSON.parse(data);
	  });
	  
	jQuery('.time-panel-dept').click(function () {
		if(jQuery(this).children('.collapse-indicator').hasClass('glyphicon-chevron-down')) {
			jQuery(this).children('.collapse-indicator').removeClass('glyphicon-chevron-down');
			jQuery(this).children('.collapse-indicator').addClass('glyphicon-chevron-up');
		} else {
			jQuery(this).children('.collapse-indicator').removeClass('glyphicon-chevron-up');
			jQuery(this).children('.collapse-indicator').addClass('glyphicon-chevron-down');
		}
	});
	
	
	var rowNum = 1;
	 
	 $('.addRow').on('click', function() {
		rowNum ++;
		if($(this).parents('form').data('type') == 'time') {
			var row = '<div class="row time-row spacing-bottom-15"><div class="col-xs-12 col-sm-3 col-md-2 col-lg-2"><input type="text" class="form-control date-pick" id="date'+rowNum+'" name="date'+rowNum+'" placeholder="Date" data-date-format="DD/MM/YYYY" required /></div><div class="col-xs-12 col-sm-5 col-md-4 col-lg-3"><input type="text" class="form-control project-pick" id="project'+rowNum+'" name="project'+rowNum+'" placeholder="Project" required /></div><div class="col-xs-12 col-sm-3 col-md-2 col-lg-2"><div class="input-group"><input type="text" class="form-control" id="duration'+rowNum+'" name="duration'+rowNum+'" placeholder="Time" max="24" required /><span class="input-group-addon">hrs</span></div></div><div class="col-sm-1 col-md-1 col-lg-1 hidden-xs"><button type="button" class="btn btn-default btn-sm removeRow"><span class="glyphicon glyphicon-minus"></span></button></div></div>';
		} else if($(this).parents('form').data('type') == 'cost') {
			var row = '<div class="row time-row spacing-bottom-15"><div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 has-feedback"><input type="text" class="form-control date-pick" id="date'+rowNum+'" name="date'+rowNum+'" placeholder="Date" data-date-format="DD/MM/YYYY" required /><span class="glyphicon glyphicon-calendar form-control-feedback feedback-fix"></span></div><div class="col-xs-12 col-sm-5 col-md-4 col-lg-3"><input type="text" class="form-control project-pick" id="project'+rowNum+'" name="project'+rowNum+'" placeholder="Project" required /></div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-2"><div class="input-group"><span class="input-group-addon">&pound;</span><input type="text" class="form-control" id="amount'+rowNum+'" name="amount'+rowNum+'" placeholder="Amount" required /><span class="input-group-addon">.00</span></div></div><div class="col-xs-12 col-sm-11 col-md-9 col-lg-4"><input type="text" class="form-control" id="reason'+rowNum+'" name="reason'+rowNum+'" placeholder="Reason" required /></div><div class="col-sm-1 col-md-1 col-lg-1 hidden-xs"><button type="button" class="btn btn-default btn-sm removeRow"><span class="glyphicon glyphicon-minus"></span></button></div></div>';
		}
		
		jQuery('#form-inner').append(row);
		
		$( '#project'+ rowNum ).autocomplete({source: globalProjects.projects});
		
		$('#date'+ rowNum).datetimepicker({ pickTime:false, defaultDate:moment() });
		
	});
	
	$(document).on('click', '.removeRow', function() {
		jQuery(this).closest('.row').remove();
	});
	
	$(document).on('click', '.removeRowMobile', function() {
		jQuery(this).closest('.row').prev().children('.time-row:last-child').remove();
	});

	$('.project_dept').on('click', function() {
		var dept_id = $(this).data('dept-id');
		$(this).siblings('.project_users').each(function() {
			if($(this).data('dept-id') == dept_id) {
				$(this).toggle();
			}
		});
	});
	
	$('#updateModalProject').autocomplete({source: globalProjects});
});
