<?php 
/**
 * Login Library Class
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 */
 
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Login {

    public function checkLogin()
    {
		$CI =& get_instance();
		
		// If the user isn't logged in, redirect them to the login page with an info alert
		if(!$CI->session->userdata('logged_in'))
		{
			if($CI->uri->segment(1))
				$CI->session->set_flashdata(array('alert' => true, 'type' => 'info', 'message' => '<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong> You must be logged in before you can do anything, please login below.'));
			
			redirect('login');
		}
	}

}

/* End of file Login.php */
/* Location ./application/libraries/Login.php */