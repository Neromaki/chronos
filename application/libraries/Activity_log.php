<?php 
/**
 * Activity Log Library Class
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 */
 
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Activity_log {

	function addLog($user_id = NULL,	// User ID
					$type, 				// Action type (add / edit / delete / log / error)
					$subject, 			// Action subject (admin / project / user / time / error)
					$action)			// Action (desc, 255)
	{
		$CI =& get_instance();
		
		$CI->load->model('log_model');
		
		// Variable checking
		if(!is_numeric($user_id)) {
			return false;
		}
		if(!is_string($type)) {
			return false;
		}
		if(!is_string($subject)) {
			return false;
		}
		
		$CI->log_model->addLog($user_id,
								$type,
								$subject,
								$action);
	}
	
}	

/* End of file Activity_log.php */
/* Location ./application/libraries/Activity_log.php */