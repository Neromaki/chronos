<?php 
/**
 * Paginate Library Class
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 */
 
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Paginate {

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('pagination');
	}
	
    public function init($base_url, $total_rows, $per_page)
    {
		$config['num_links'] = TRUE;
		$config['use_page_numbers'] = TRUE;	
		$config['base_url'] = base_url($base_url);
		$config['per_page'] = $per_page; 
		$config['total_rows'] = $total_rows;
		$config['num_links'] = 2;
		
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';

		// $config['first_link'] = '&larr; First';
		// $config['first_tag_open'] = '<li>';
		// $config['first_tag_close'] = '</li>';

		$config['first_link'] = '';
		$config['first_tag_open'] = '';
		$config['first_tag_close'] = '';

		// $config['last_link'] = 'Last &rarr;';
		// $config['last_tag_open'] = '<li>';
		// $config['last_tag_close'] = '</li>';

		$config['last_link'] = '';
		$config['last_tag_open'] = '';
		$config['last_tag_close'] = '';

		$config['next_link'] = '<span class="glyphicon glyphicon-arrow-right"></span>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '<span class="glyphicon glyphicon-arrow-left"></span>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		
		$this->CI->pagination->initialize($config); 
		
		return $this->CI->pagination->create_links();
	}
	
}

/* End of file Paginate.php */
/* Location ./application/libraries/Paginate.php */