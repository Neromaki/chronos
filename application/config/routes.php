<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| -------------------------------------------------------------------------
| RESERVED ROUTES
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'site';
$route['404_override'] = 'custom_404';

$route['login'] = 'session/login';
$route['logout'] = 'session/logout';

$route['projects'] = 'project';
	$route['projects/functions/(:any)'] = 'project/$1';
	$route['projects/add'] = 'project/add';
	$route['projects/update'] = 'project/update';
	$route['projects/activate'] = 'project/activate';
	$route['projects/delete'] = 'project/delete';
	$route['projects/view'] = 'project/view/$1';
		$route['projects/view/(:any)'] = 'project/view/$1';
	$route['projects/(:any)'] = 'project';

$route['departments'] = 'department';
	$route['departments/add'] = 'department/add';
	$route['departments/update'] = 'department/update';
	$route['departments/complete'] = 'department/complete';
	$route['departments/delete'] = 'department/delete';
	$route['departments/filter'] = 'department';
	$route['departments/view'] = 'department/view/$1';
		$route['departments/view/(:any)'] = 'department/view/$1';
	$route['departments/(:any)'] = 'department';
	
$route['users'] = 'user/users';
	$route['users/functions/(:any)'] = 'user/$1';
	$route['users/view'] = 'user/view/$1';
		$route['users/view/(:any)'] = 'user/view/$1';
	$route['users/update'] = 'user/update';
	$route['users/edit/(:any)'] = 'user/edit/$1';
	$route['users/delete'] = 'user/delete';
	$route['users/filter'] = 'user';
	$route['users/(:any)'] = 'user/users';
	
$route['time'] = 'time/dashboard';
	$route['time/functions/(:any)'] = 'time/$1';
	$route['time/add'] = 'time/add';
	$route['time/update'] = 'time/update';
	$route['time/edit/(:any)'] = 'time/edit/$1';
	$route['time/delete'] = 'time/delete';
	$route['time/filter'] = 'time';
	$route['time/(:any)'] = 'time/dashboard';
	
$route['costs'] = 'cost/dashboard';
	$route['costs/functions/(:any)'] = 'cost/$1';
	$route['costs/add'] = 'cost/add';
	$route['costs/update'] = 'cost/update';
	$route['costs/edit/(:any)'] = 'cost/edit/$1';
	$route['costs/delete'] = 'cost/delete';
	$route['costs/filter'] = 'cost';
	$route['costs/(:any)'] = 'cost/dashboard';
	$route['cost/(:any)'] = 'cost/$1';


$route['logs'] = 'logs/logs';
	$route['logs/functions/(:any)'] = 'logs/$1';
	$route['logs/(:any)'] = 'logs/logs';
	
$route['account'] = 'user/account';
	
$route['about/changelog'] = 'site/changelog';

$route['(:any)'] = 'site/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */