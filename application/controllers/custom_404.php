<?php
/**
 * Custom 404 Controller Class
 *
 *	Is called whenever a user encountered a 404 page-not-found error
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom_404 extends CI_Controller {


	/**
	*	Checks if the user is logged in first (if not, they shouldn't be able to access the areas
	*	they might encounter a 404 in
	*/
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('Login');
			$this->login->checkLogin();
	}


	/**
	*	Loads the 404 view
	*/
	public function index()
	{
		$data['contentlocation'] = 'system/404';
		$this->load->view('system/template', $data);
	}
	
}

/* End of file custom_404.php */
/* Location ./application/controllers/custom_404.php */