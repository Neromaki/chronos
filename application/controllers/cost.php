<?php
/**
 * Cost Controller Class
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cost extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Europe/London');
		
		$this->load->library('Login');
			$this->login->checkLogin();
		
		$this->load->model('cost_model');
		$this->load->model('user_model');
		$this->load->model('department_model');
		$this->load->model('project_model');
		
		// Enables the profiler for debugging output
		if(checkProfiler())
			$this->output->enable_profiler(TRUE);
	}
	

	/**
	*	Cost::dashboard()
	*	Default list view showing all a user's costs
	*/
	public function dashboard()
	{
		if($this->input->post('filter') && ($this->input->post('filter_date_start') && $this->input->post('filter_date_end')))
		{
			$this->form_validation->set_rules('filter_date_start', 'Date start', 'trim|callback_checkPeriod['.$this->input->post('filter_date_end').']');
		}
		
		$this->form_validation->set_rules('filter_date_end', 'Date end', 'trim');
		$this->form_validation->set_rules('filter_project', 'Project', 'min_length[0]');
		
		if($this->input->post('filter'))
		{
			$filter_date_start = trim($this->input->post('filter_date_start'));
			$filter_date_end = trim($this->input->post('filter_date_end'));
			$filter_project = trim($this->input->post('filter_project'));
		}
		$costsPerPage = 20;
		$page = $this->uri->segment(2);
		
		$filter_data = array();
		
		$filter_data['user_id'] = $this->session->userdata('id');

		if(!empty($filter_date_start))
			$filter_data['date_start'] = $filter_date_start;
		if(!empty($filter_date_end))
			$filter_data['date_end'] = $filter_date_end;
		if(!empty($filter_project))
			$filter_data['project'] = $filter_project;
		
		$offset = ($page  === FALSE) ? 0 : ($page * $costsPerPage) - $costsPerPage;
		
		$data['filters'] = $filter_data;
		
		if($this->form_validation->run() == FALSE)
		{
			$filter_data = array();
			$filter_data['user_id'] = $this->session->userdata('id');
		}
		
		// Grab the user's projects, load the view and pass the data over
		$data['costs'] = $this->cost_model->getUserCosts($filter_data,
														$offset,
														$costsPerPage);
		
		$total_rows = $this->cost_model->countCosts($filter_data);
		
		$this->load->library('paginate');
		
		$data['pagination']			= $this->paginate->init('cost', $total_rows, $costsPerPage);

		$projects = $this->project_model->getAllProjectNames();
		foreach($projects as $p) 
		{
			$data['projects'][] = $p->project_name;
		}
		
		$data['contentlocation'] = 'cost/dashboard';
		$this->load->view('system/template', $data);
	}
	
	
	/**
	*	Cost::getCostData()
	*	Get the data for a particular cost object
	*
	*	@param 	int 	$cost_id 	ID of the cost item
	*/
	public function getCostData($cost_id = FALSE)
	{
		if(!$cost_id)
			$cost_id = $this->input->post('cost_id');
			
		if($cost_id)
			$result = $this->cost_model->getCostData($cost_id);
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
		{
			if($result) {
				echo json_encode($result);
			} else {
				echo "false";
			}
		}
		else
		{	
			if($result) {
				return $result;
			} else {
				return FALSE;
			}
		}
	}
	
	
	/**
	*	Cost::add()
	*	Adds a new cost object to the system
	*/
	public function add()
	{
		if($this->input->post('add_day_cost'))
		{
			$form_data = $this->input->post();
			
			
			for($x=1; isset($form_data['date' . $x]); $x++)
			{
				$cost_data[$x]['date'] 		= $this->input->post('date' . $x);
				$cost_data[$x]['project'] 	= $this->input->post('project' . $x);
				$cost_data[$x]['amount']	= $this->input->post('amount' . $x);
				$cost_data[$x]['reason'] 	= $this->input->post('reason' . $x);
				
				// Set the form validation rules
				$this->form_validation->set_rules('date'.$x, 'Date '.$x, 'trim|required');
				$this->form_validation->set_rules('project'.$x, 'Project '.$x, 'trim|required|callback_checkProjectName');
				$this->form_validation->set_rules('amount'.$x, 'Amount '.$x, 'trim|required|callback_checkAmount');
				$this->form_validation->set_rules('reason'.$x, 'Reason '.$x, 'trim|required');
			}
			
			$this->session->unset_userdata('cost_data');
			$this->session->set_userdata('cost_data', $cost_data);
		}
		
		
		if($this->form_validation->run() == FALSE)
		{
			$projects = $this->project_model->getAllProjectNames();
			for($x=0;$x<count($projects);$x++) 
			{
				$data['project_names'][$x] = $projects[$x]->project_name;
				$data['project_descs'][$x] = $projects[$x]->description;
			}
			$data['contentlocation'] = 'cost/add';
			$this->load->view('system/template', $data);
		}
		else
		{
			// Prepare the data so it can be inserted straight into the database
			
			// If we're adding on a per-day basis..
			if($this->input->post('add_day_cost'))
			{
				// We just need to change the date formats of each item to MySQL DATEcost
				foreach($cost_data as &$td)
				{
					// Create a DateTime object from our UK-format
					$date = DateTime::createFromFormat('d/m/Y', $td['date']);
					
					// Convert it to DATEcost and replace the existing
					$td['date'] = $date->format('Y-m-d');
				}
			}
			
			
			$user_id = $this->session->userdata('id');
			$dept_id = $this->user_model->getUserDepartment($user_id);
			
			$errors = array();
			$successes = array();
			
			foreach($cost_data as &$cd)
			{
				$project_id = $this->project_model->getProjectByName($cd['project']);
				$cd['project'] = $project_id;
				$cd['user_id'] = $user_id;
				$cd['dept_id'] = $dept_id;
				
				$result = $this->cost_model->addCost($cd);
				
				if(!$result)
					$errors[] = $cd['date'];
				else
					$added[] = $result;
			}
			
			if(count($errors) == count($cost_data))
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Damnit!</strong> Your costs(s) could not be added, please try again or contact an administrator.'));
			elseif(count($errors) > 0)
			{
				$failed_dates = "";
				for($x=0;$x<count($errors);$x++)
				{
					$failed_dates .= $errors[$x];
					
					if($x < (count($errors)-1))
						$failed_dates .= ", ";
				}
				$this->session->set_flashdata(array('alert' => true, 'type' => 'warning', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Whoops!</strong> Everything except the dates ('.$failed_dates.') were added. Please try adding the failed ones again or contact an administrator.'));
			}
			else
			{
				$user = $this->user_model->getAccountInfo($user_id);
				
				foreach($cost_data as $cd) {
					$project_name = $this->project_model->getProjectName($cd['project']);
					$this->activity_log->addLog($this->session->userdata('id'), 'add', 'cost', "Added cost of ".formatCost($cd['amount'])." ('".$cd['reason']."') to <a href='".base_url('projects/view/'.$cd['project'])."'>".$project_name."</a> for ".$cd['date']);
				}
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Hurrah!</strong> Your cost(s) were added successfully.'));
				$this->session->unset_userdata('cost_data');
			}
			if(!empty($added))
				$this->session->set_flashdata('cost_added', $added);
				
			redirect('costs');
		}
	}
	
	
	/**
	*	Cost::update()
	*	Updates the data for a particular cost object on the system
	*
	*	@param 	int 	$cost_id 	The ID of the cost object to update
	*	@param 	int 	$user_id	The ID of the user updating the cost object
	*	@param 	array 	$data 		Associative array of cost object data
	*/
	public function update($cost_id = FALSE, $user_id = FALSE, $data = FALSE)
	{
		if(!$cost_id)
			$cost_id = $this->input->post('cost_id');
			
		if(!$user_id)
			$user_id = $this->session->userdata('id');
			
		if(!$data)
		{
			$date 		= trim($this->input->post('update_date'));
			$project 	= trim($this->input->post('update_project'));
			$amount 	= trim($this->input->post('update_amount'));
			$reason 	= trim($this->input->post('update_reason'));
			
			$data = array();
			
			if(!empty($date)) {
				// Create a DateTime object from our UK-format
				$date = DateTime::createFromFormat('d/m/Y', $date);
				// Convert it to DATETIME and replace the existing
				$data['date'] = $date->format('Y-m-d');
			}
			if(!empty($project)) {
				if(is_numeric($project)) {
					$data['project_id'] = $project;	
				} else {
					$project_id = $this->project_model->getProjectByName($project);
					if($project_id) {
						$data['project_id'] = $project_id;
					}
				}				
			}
			if(!empty($amount))
				$data['amount'] = $amount;
			if(!empty($amount))
				$data['reason'] = $reason;
		}
		
		if(!empty($data))
		{
			$user = $this->user_model->getAccountInfo($user_id);
			$cost_info = $this->cost_model->getCostData($cost_id);
			
			if(($user->user_id == $user_id) ||
				($user->account_type == 1) ||
				($user->account_type == 2 && $this->user_model->canManageUser($user_id, $data->user_id))
				)
			{
				$this->cost_model->updateCost($cost_id, $data);
				
				if(isset($data['amount']))
					$amount = $amount;
				else
					$amount = $cost_info->amount;
				
				$this->activity_log->addLog($this->session->userdata('id'), 'update', 'cost', "Updated cost to ".formatCost($cost_info->amount)." ('".$cost_info->reason."') to <a href='".base_url('projects/view/'.$cost_info->project_id."'>".$cost_info->project_name."</a> for ".$cost_info->date));
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Hurrah!</strong> That cost was updated successfully.'));
			}
			else
			{
				// cannot delete cost
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Sorry!</strong> Either that cost does not belong to you, or you do not have the permissions to update it.'));
			}
		}
		else
		{
			// cost not found
			$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Drats!</strong> That cost could not be found, thus could not be updated.'));
		}
		redirect('costs');	
	}
	

	/**
	*	Cost::delete()
	*	Deletes a particular cost object from the system
	*
	*	@param 	int 	$cost_id 	The ID of the cost object to update
	*	@param 	int 	$user_id	The ID of the user updating the cost object
	*/
	public function delete($cost_id = FALSE, $user_id = FALSE)
	{
		if(!$cost_id)
			$cost_id = $this->input->post('cost_id');
			
		if(!$user_id)
			$user_id = $this->session->userdata('id');
			
		$data = $this->cost_model->getcostData($cost_id);
		
		if($data)
		{
			$user = $this->user_model->getAccountInfo($user_id);
			
			if(($data->user_id == $user_id) ||
				($user->account_type == 1) ||
				($user->account_type == 2 && $this->user_model->canManageUser($user_id, $data->user_id))
				)
			{
				$this->cost_model->deletecost($cost_id);
				$this->activity_log->addLog($this->session->userdata('id'), 'delete', 'cost', "Deleted cost of ".formatCost($data->amount)." ('".$data->reason."') to <a href='".base_url('projects/view/'.$data->project_id."'>".$data->project_name."</a> for ".$data->date));
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Hurrah!</strong> That cost was deleted successfully.'));
			}
			else
			{
				// cannot delete cost
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Sorry!</strong> Either that cost does not belong to you, or you do not have the permissions to delete it.'));
			}
		}
		else
		{
			// cost not found
			$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Drats!</strong> That cost could not be found, thus could not be deleted.'));
		}
		redirect('costs');
	}
	
	
	/**
	*	Cost::checkProjectName()
	*	Checks if a project name string exists on the system
	*
	*	@param 	string 	$project_name 	The project name string
	*/
	function checkProjectName($project_name)
	{			
		$project_id = $this->project_model->getProjectByName($project_name);
		
		if($project_id)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('checkProjectName', "The project '<strong>$project_name</strong>' was not found.");
			return FALSE;
		}
	}
	

	/**
	*	Cost::checkAmount()
	*	Checks if a supplied amount is numeric and valid
	*
	*	@param 	float 	$amount 	The amount to check
	*/
	function checkAmount($amount)
	{
		if(is_numeric($amount))
		{
			if($amount > 0)
				return TRUE;
			else
			{
				$this->form_validation->set_message('checkAmount', 'Amount must be greater than 0.');	
				return FALSE;
			}
		}
		else
		{
			$this->form_validation->set_message('checkAmount', "Invalid amount '<strong>$amount</strong>' - must be either an integer <em>(1, 2, 3 etc)</em> or decimal <em>(1.5, 2.75 etc)</em>");
			return FALSE;
		}
	}
	

	/**
	*	Cost::checkPeriod()
	*	Checks if a period is valid ($start comes before $end)
	*
	*	@param 	string 	$start 	The start date
	*	@param 	string 	$end 	The end date
	*/
	function checkPeriod($start, $end)
	{
		function shuffleDate($date) 
		{
			$date_bits = explode('/', $date);
			return $date_bits[2].'-'.$date_bits[1].'-'.$date_bits[0];
		}
		$start 	= strtotime(shuffleDate($start));
		$end 	= strtotime(shuffleDate($end));
		
		if($start > $end)
		{
			$this->form_validation->set_message('checkPeriod', 'Start date cannot be after the end date.');	
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}

/* End of file cost.php */
/* Location ./application/controllers/cost.php */