<?php
/**
 * Project Controller Class
 *
 *	Controls the various actions related to project management
 *	e.g. Adding, updating or deleting project, viewing the main project list etc
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends CI_Controller {


	/**
	*	Construct the parent CodeIgniter controller class, sets the default timezone,
	*	checks if the user is logged in and finally loads the related models
	*/
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Europe/London');
		
		$this->load->library('Login');
			$this->login->checkLogin();
			
		// Check to make sure the user is allowed to access this area..
		checkPermissions('all', true);
		
		$this->load->model('site_model');
		$this->load->model('project_model');
		$this->load->model('department_model');
		
		// Enables the profiler for debugging output
		if(checkProfiler())
			$this->output->enable_profiler(TRUE);
	}


	/**
	*	Project::index()
	*	Controls the main project list and the inline "Add Project" form
	*/
	public function index()
	{
		if($this->input->post('add'))
		{
			// Set the form validation rules
			$this->form_validation->set_rules('name', 'Name', 'required');
		}
		
		// If all the rule criteria are not met (failed form submission or no form submission)
		if ($this->form_validation->run() == FALSE)
		{
			//$data['appSettings'] 		= $this->site_model->getAppSettings($this->session->userdata('id')); 
			//$projectsPerPage = $data['appSettings']->projectsPerPage;
			$projectsPerPage = 20;
			$page = $this->uri->segment(2);
			
			
			if($this->input->post('search'))
			{
				$search_string = $this->input->post('search_string');
				$type = $this->input->post('search_type');
				$order = $this->input->post('search_order');
			}
			else
			{
				$search_string = FALSE;
				$order = 'project_name';
				$type = FALSE;
			}

			$offset = ($page  === FALSE) ? 0 : ($page * $projectsPerPage) - $projectsPerPage;
			
			// Grab the user's projects, load the view and pass the data over
			$data['projects'] = $this->project_model->getProjects($search_string,
																	$offset,
																	$projectsPerPage,
																	$order,
																	$type);		// Gets the user's tasks and projects
			
			$total_rows = $this->project_model->countProjects($search_string, $type);
			
			$this->load->library('paginate');
			
			$data['pagination']			= $this->paginate->init('projects', $total_rows, $projectsPerPage);
			$data['contentlocation'] = 'project/projects';
			$this->load->view('system/template', $data);
		}
		else
		{
			if($this->input->post('add'))
			{
				// If the rule criteria ARE met, run the subsidiary addProject() to actually add the data
				$this->addProject();
			}
			else
			{
				redirect('projects');
			}
		}
	}
	
	
	/**
	*	Project::view()
	*	Views a specific project using the supplied project ID
	*
	*	@param	string	$project	The project ID
	*/
	public function view($project)
	{
		$this->session->set_flashdata('project_url', current_url());	
		
		if($data['project_data'] = $this->project_model->getProjectData($project))
		{
			if($this->input->post('filter'))
			{
				$start = DateTime::createFromFormat('d/m/Y', $this->input->post('period_start'))->format('Y-m-d');
				$end = DateTime::createFromFormat('d/m/Y', $this->input->post('period_end'))->format('Y-m-d');

				$data['loggedTime']['custom'] = $this->project_model->getProjectLoggedTime($project, 
																							$start,
																							$end);
				$data['project_data']->custom = $this->project_model->getProjectStats($project, $start, $end);

				$data['project_depts'] = $this->project_model->getProjectDepartmentsTime($project, $start, $end);

				if($data['project_depts'])
				{
					$x=0;
					foreach($data['project_depts'] as &$pt) 
					{
						if($pt->totalTime == NULL && $pt->totalCost == NULL)
						{
							unset($data['project_depts'][$x]);
						}
						else
						{
							$pt->dept_data = $this->project_model->getProjectDepartmentEmployees($project, $pt->dept_id, $start, $end);
							
							foreach($pt->dept_data as &$dd)
							{
								if($dd->totalTime == NULL && $dd->totalCost == NULL)
								{
									unset($dd);
								}
							}
						}
						$x++;
					}
				}
			}
			else
			{
				// Project found, so grab the statistical data and chuck it at the view
				$data['loggedTime'] = array(
										'dateRanges' => array(
															'allTime' => array(
																			'from' => '2000-01-01',
																			'to' => '2037-12-31'
																			),
															'thisWeek' => array(
																			'from' => date('d-m-Y', strtotime('midnight monday this week')),
																			'to' => date('d-m-Y', strtotime('11:59 sunday this week'))
																			),
															'lastWeek' => array(
																			'from' => date('d-m-Y', strtotime('midnight monday last week')),
																			'to' => date('d-m-Y', strtotime('11:59 sunday last week'))
																			)
															)
										);

				$data['loggedTime']['allTime'] = $this->project_model->getProjectLoggedTime($project, 
																							$data['loggedTime']['dateRanges']['allTime']['from'], 
																							$data['loggedTime']['dateRanges']['allTime']['to']);

				// New data additions
				$thisWeekStart	= date('Y-m-d', strtotime(date('Y')."-W".date('W')."-1"));
				$thisWeekEnd	= date('Y-m-d', strtotime(date('Y')."-W".date('W')."-7"));
				$lastWeekStart	= date('Y-m-d', strtotime(date('Y')."-W".(date('W')-1)."-1"));
				$lastWeekEnd	= date('Y-m-d', strtotime(date('Y')."-W".(date('W')-1)."-7"));
				$thisMonthStart	= date('Y-m-d', strtotime("first day of this month"));
				$thisMonthEnd	= date('Y-m-d', strtotime("last day of this month"));
				$lastMonthStart	= date('Y-m-d', strtotime("first day of last month"));
				$lastMonthEnd	= date('Y-m-d', strtotime("last day of last month"));

				
				$data['project_data']->thisWeek = $this->project_model->getProjectStats($project, $thisWeekStart, $thisWeekEnd);
				$data['project_data']->lastWeek = $this->project_model->getProjectStats($project, $lastWeekStart, $lastWeekEnd);
				$data['project_data']->thisMonth = $this->project_model->getProjectStats($project, $thisMonthStart, $thisMonthEnd);
				$data['project_data']->lastMonth = $this->project_model->getProjectStats($project, $lastMonthStart, $lastMonthEnd);

				$data['project_depts'] = $this->project_model->getProjectDepartmentsTime($project);

				if($data['project_depts'])
				{
					foreach($data['project_depts'] as &$pt) 
					{
						$pt->dept_data = $this->project_model->getProjectDepartmentEmployees($project, $pt->dept_id);
						
						foreach($pt->dept_data as &$dd)
						{
							if(($dd->thisWeekTime == NULL && $dd->lastWeekTime == NULL && $dd->thisMonthTime == NULL && $dd->lastMonthTime == NULL) &&
							($dd->thisWeekCost == NULL && $dd->lastWeekCost == NULL && $dd->thisMonthCost == NULL && $dd->lastMonthCost == NULL)) 
							{
								unset($dd);
							}
						}
					}
				}
			}

			$data['contentlocation'] = 'project/view_project';
			$this->load->view('system/template', $data);
		}
		else
		{
			// No project found, set an error alert and redirect back to the main list
			$this->session->set_flashdata(array('alert' => true, 'type' => 'warning', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Dang!</strong> That project was not found.'));
			redirect('projects');
		}
	}
	
	
	/**
	*	Project::add()
	*	Controls the form validation for the "Add Project" function (projects/add, not the inline form)
	*/
	public function add()
	{
		// Set the form validation rules
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');	
		
		// If the rule criteria are not met (page load, no submission)
		if ($this->form_validation->run() == FALSE)
		{
			// Load the view
			$data['contentlocation'] = 'project/add_project';
			$this->load->view('system/template', $data);
		}
		else
		{
			// Otherwise, add the project
			$this->addProject();
		}
	}
	
	
	/**
	*	Project::addProject()
	*	Subsidiary function of index() and add(), actually passes the data to the model for processing
	*/
	public function addProject()
	{
		// Grab the form data via POST requests
		$name			= $this->input->post('name');
		$description	= $this->input->post('description');
		
		// Pass the data to the model to be added, and get the response
		$add_project = $this->project_model->addProject($name,
														$description,
														$this->session->userdata('id'));
		
		// Output success / fail based on the returned value from the model
		switch($add_project['outcome'])
		{
			// Successfully added
			case(1):
				// Log the action
				$this->activity_log->addLog($this->session->userdata('id'), 'add', 'project', "Created new project <a href='" . base_url('projects/view/' . $add_project['project_id']) . "'>$name</a>");				
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Success!</strong> That project was added to the system.'));
			break;
			
			// Error whilst adding
			case(2):
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw snap!</strong> There was a problem adding that project, please try again in a bit.'));
			break;	
		}
		
		// Redirect the user back to the project list
		redirect('projects');
	}
		

	/**
	*	Project::update()
	*	Updates a specific project's data
	*/
	public function update()
	{
		// Grab the relevant POST variables from the HTML form
		$project_id		= $this->input->post('project_id');
		$name			= $this->input->post('name');
		$description	= $this->input->post('description');
		
		// Throw the variable at the relevant model function, and assign the model's response to $add_project
		$update_project = $this->project_model->updateProject($project_id,
																$name,
																$description);
	
		// Based on the model's response, return a specific alert to the user (success, error)
		switch($update_project)
		{
			// Successfully added
			case(1):
				// Log the action
				$this->activity_log->addLog($this->session->userdata('id'), 'update', 'project', "Updated project <a href='" . base_url('projects/view/' . $project_id) . "'>$name</a>");				
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Success!</strong> That project was updated.'));
			break;
			
			// Error whilst adding
			case(2):
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw snap!</strong> There was a problem updating that project, please try again in a bit.'));
			break;
			
			// User is not able to use specified project
			case(3):
				$this->session->set_flashdata(array('alert' => true, 'type' => 'warning', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Hold on!</strong> It seems you don\'t have permission to update that project. Please contact an Administrator.'));
			break;
		}
		
		// Redirect the user back to the project list
		redirect(base_url('projects/view/' . $project_id));
	}
		
		
	/**
	*	Project::delete()
	*	Deletes a specific project from the system
	*/
	public function delete()
	{
		// Get the POST data from the form
		$project_id 		= $this->input->post('project_id');
		$project_name		= $this->project_model->getProjectName($project_id);
		
		// Pass data to the model to delete the project, and store the response
		$delete_project = $this->project_model->deactivateProject($project_id);
		
		// Based on the model's response, return a specific alert to the user (success, error)
		switch($delete_project)
		{
			// Successfully added
			case(1):
				// Log the action
				$this->activity_log->addLog($this->session->userdata('id'), 'delete', 'project', "Deactivated project <a href='" . base_url('projects/view/' . $project_id) . "'>$project_name</a>");				
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Success!</strong> That project was deactivated.'));
			break;
			
			// Error whilst adding
			case(2):
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw snap!</strong> There was a problem deactivating that project, please try again in a bit.'));
			break;
			
			// User is not able to delete specified project
			case(3):
				$this->session->set_flashdata(array('alert' => true, 'type' => 'warning', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Holdl on!</strong> You don\' have permission to deactivate that project.'));
			break;
		}
		
		// Redirect the user back to the project list
		redirect('projects/view/' . $project_id);
	}
	

	/**
	*	Project::activate()
	*	Activate a specific project on the system
	*/
	public function activate()
	{
		// Get the POST data from the form
		$project_id 		= $this->input->post('project_id');
		$project_name		= $this->project_model->getProjectName($project_id);
		
		// Pass data to the model to delete the project, and store the response
		$activate_project = $this->project_model->activateProject($project_id);
		
		// Based on the model's response, return a specific alert to the user (success, error)
		switch($activate_project)
		{
			// Successfully added
			case(1):
				// Log the action
				$this->activity_log->addLog($this->session->userdata('id'), 'activate', 'project', "Re-activated project <a href='" . base_url('projects/view/' . $project_id) . "'>$project_name</a>");				
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Success!</strong> That project was re-activated.'));
			break;
			
			// Error whilst adding
			case(2):
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw snap!</strong> There was a problem activating that project, please try again in a bit.'));
			break;
			
			// User is not able to delete specified project
			case(3):
				$this->session->set_flashdata(array('alert' => true, 'type' => 'warning', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Holdl on!</strong> You don\' have permission to activate that project.'));
			break;
		}
		
		// Redirect the user back to the project list
		redirect('projects/view/' . $project_id);
	}

	
	/**
	*	Project::getProjectNames()
	*	Gets the names of all projects on the system
	*/
	public function getProjectNames()
	{
		$projects = $this->project_model->getAllProjectNames();
		foreach($projects as $p) 
		{
			$data['projects'][] = $p->project_name;
		}
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			if($data) {
				echo json_encode($data);
			} else {
				echo "false";
			}
		}
		else
		{
			if($data) {
				return $data;
			} else {
				return FALSE;
			}
		}		
		
	}
	
}

/* End of file project.php */
/* Location ./application/controllers/project.php */