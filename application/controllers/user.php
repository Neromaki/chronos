<?php
/**
 * User Controller Class
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Europe/London');
		
		// Check if user is logged in
		$this->load->library('Login');
			$this->login->checkLogin();
		
		// Load the models
		$this->load->model('user_model');
		$this->load->model('department_model');
		
		// Enables the profiler for debugging output
		if(checkProfiler())
			$this->output->enable_profiler(TRUE);
	}


	/**
	*	User::users()
	*	Default list view for managers and admins to manage all users on the system
	*/
	public function users()
	{
		// Check to make sure the user is allowed to access this area..
		checkPermissions('management', 'account');
		
		if($this->input->post('add'))
		{
			// Set the form validation rules
			$this->form_validation->set_rules('username', 'Username', 'required|callback_checkUsernameExists');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'valid_email');
			$this->form_validation->set_rules('department', 'Department', 'required');
			$this->form_validation->set_rules('type', 'Account type', 'required');
		}
		
		// If all the rule criteria are not met (failed form submission or no form submission)
		if ($this->form_validation->run() == FALSE)
		{
			$usersPerPage = 20;
			$page = $this->uri->segment(2);
			
			
			if($this->input->post('search'))
			{
				$search_string = $this->input->post('search_string');
				$order = $this->input->post('search_order');
			}
			else
			{
				$search_string = FALSE;
				$order = 'username';
			}
			
			$offset = ($page  === FALSE) ? 0 : ($page * $usersPerPage) - $usersPerPage;
			
			// Grab the user's projects, load the view and pass the data over
			$data['users'] = $this->user_model->getUsers($search_string,
														$offset,
														$usersPerPage,
														$order);		// Gets the user's tasks and projects
			
			$total_rows = $this->user_model->countUsers($search_string);
			
			$this->load->library('paginate');
			
			$data['departments']		= $this->department_model->getDepartments(FALSE, FALSE, FALSE, TRUE);
			$data['pagination']			= $this->paginate->init('users', $total_rows, $usersPerPage);
			$data['contentlocation'] 	= 'user/users';
			$this->load->view('system/template', $data);
		}
		else
		{
			if($this->input->post('add'))
			{
				// If the rule criteria ARE met, run the subsidiary addProject() to actually add the data
				$this->addUser();
			}
			else
			{
				redirect('users');
			}
		}
	}
	
	
	/**
	*	User::view()
	*	Displays all the relevant data for a user, essentially the user "profile"
	*
	*	@param 	int 	$user_id 	The ID of the user to view
	*/
	public function view($user_id)
	{
		$this->session->set_flashdata('user_url', current_url());
		
		// If the supplied slug exists in the database..
		if($data['user_info'] = $this->user_model->getAccountInfo($user_id))
		{
			$data['user_data'] = $this->user_model->getUserData($user_id);
			if($this->session->userdata('account_type') == 1
				|| ($this->session->userdata('account_type') == 2 && $this->user_model->canManageUser($this->session->userdata('id'), $user_id)))
			{
				$data['can_manage'] = TRUE;
			} else {
				$data['can_manage'] = FALSE;
			}

			$thisWeekStart	= date('Y-m-d', strtotime(date('Y')."-W".date('W')."-1"));
			$thisWeekEnd	= date('Y-m-d', strtotime(date('Y')."-W".date('W')."-7"));
			$lastWeekStart	= date('Y-m-d', strtotime(date('Y')."-W".(date('W')-1)."-1"));
			$lastWeekEnd	= date('Y-m-d', strtotime(date('Y')."-W".(date('W')-1)."-7"));
			$thisMonthStart	= date('Y-m-d', strtotime("first day of this month"));
			$thisMonthEnd	= date('Y-m-d', strtotime("last day of this month"));
			$lastMonthStart	= date('Y-m-d', strtotime("first day of last month"));
			$lastMonthEnd	= date('Y-m-d', strtotime("last day of last month"));

			$data['user_data']->thisWeek = $this->user_model->getUserStats($user_id, $thisWeekStart, $thisWeekEnd);
			$data['user_data']->lastWeek = $this->user_model->getUserStats($user_id, $lastWeekStart, $lastWeekEnd);
			$data['user_data']->thisMonth = $this->user_model->getUserStats($user_id, $thisMonthStart, $thisMonthEnd);
			$data['user_data']->lastMonth = $this->user_model->getUserStats($user_id, $lastMonthStart, $lastMonthEnd);

			$data['user_data']->recentProjects = $this->user_model->getUserRecentProjects($user_id);
			
			$data['contentlocation'] = 'user/view_user';
			$this->load->view('system/template', $data);
		}
		else
		{
			// No user found, set an error alert and redirect back to the main list
			$this->session->set_flashdata(array('alert' => true, 'type' => 'warning', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Dang!</strong> That user was not found.'));
			redirect('users');
		}
	}
	
	
	/**
	*	User::edit()
	*	Displays and processes the form for updating an existing user account
	*
	*	@param 	int 	$user_id 	The ID of the user to update
	*/
	public function edit($user_id)
	{
		if($this->session->userdata('id') == $user_id) {
			redirect('account');
		}

		// Check to make sure the user is allowed to access this area..
		checkPermissions('management', true);
		
		// If they are a manager, make sure they are a manager of this department,
		// otherwise, redirect them to the department list (which will redirect to their dept)
		// if(checkPermissions('manager'))
		// {
		// 	if(!canManageUser($user_id))
		// 		redirect('users');
		// }
		
		
		if($this->session->flashdata('user_url'))
			$this->session->keep_flashdata('user_url');
		
		if($this->input->post('changePassword'))
		{
			$this->form_validation->set_rules('newPassword', 'Password', 'required|matches[newPasswordConfirm]');
			$this->form_validation->set_rules('newPasswordConfirm', 'Password confirmation', 'required');
		}
		
		if($this->input->post('updateInfo'))
		{
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'valid_email');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['account_info']		= $this->user_model->getAccountInfo($user_id);
			$data['departments'] 		= $this->department_model->getAllDepartments();
			$data['contentlocation'] = 'user/edit_user';
			$this->load->view('system/template', $data);
		}
		else
		{
			// If changing passwords..
			if($this->input->post('changePassword'))
			{
				$user_id 			= $user_id  = $this->input->post('user_id');
				$newPassword 		= sha1($this->input->post('newPassword'));
				
				switch($this->updatePassword($user_id, $newPassword))
				{
					// Successfully updated
					case(1):
						$this->activity_log->addLog($this->session->userdata('id'), 'update', 'user', "Updated password for user <a href='" . base_url('users/view/' . $user_id) . "'>" . $this->user_model->getUsername($user_id) . "</a>");
						$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Okie dokes!</strong> That user\'s password was updated.'));
					break;
					
					// Incorrect current password
					case(2):
						$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Uh oh!</strong> The current password you entered is incorrect.'));
					break;
					
					// Other error
					case(3):
						$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw man!</strong> There was a problem updating your password, try again in a bit.'));
					break;
				}
				redirect('users/view/' . $user_id);
			}
			
			// If updating info..
			if($this->input->post('updateInfo'))
			{
				$user_id 			= $this->input->post('user_id');
				$name			 	= $this->input->post('name');
				$email		 		= $this->input->post('email');
				$dept_id			= $this->input->post('department');
				$account_type		= $this->input->post('account_type');
				
				$currentData = $this->user_model->getAccountInfo($user_id);
				
				$update_data = array('name' => $name, 'email' => $email, 'dept_id' => $dept_id, 'account_type' => $account_type);
				
				if($this->user_model->updateUser($user_id, $update_data))
					$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Okie dokes!</strong> The user was updated.'));
				else
					$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw man!</strong> There was a problem updating that user, try again in a bit.'));

				redirect('users/view/' . $user_id);
			}
		}
	}
	
	
	/**
	*	User::addUser()
	*	API-style function used by other functions to add users to the system
	*/
	public function addUser()
	{
		// Check to make sure the user is allowed to access this area..
		checkPermissions('management', true);
		
		// Grab the form data via POST requests
		$username		= $this->input->post('username');
		$password		= $this->input->post('password');
		$name			= $this->input->post('name');
		$email			= $this->input->post('email');
		$department		= $this->input->post('department');
		$type			= $this->input->post('type');
		
		// Pass the data to the model to be added, and get the response
		$add_user = $this->user_model->addUser($username,
												sha1($password),
												$name,
												$email,
												$department,
												$type);
		
		// Output success / fail based on the returned value from the model
		switch($add_user['outcome'])
		{
			// Successfully added
			case(1):
				// Log the action
				$this->activity_log->addLog($this->session->userdata('id'), 'add', 'user', "Created new user <a href='" . base_url('users/view/' . $add_user['user_id']) . "'>$username</a>, added them to <a href='" . base_url('departments/view/' . $department) . "'>" . $this->department_model->getDeptName($department) . "</a>");
				
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>User added!</strong>'));
			break;
			
			// Error whilst adding
			case(2):
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw snap!</strong> There was a problem adding the user, please try again in a bit.'));
			break;	
		}
		
		// Redirect the user back to the project list
		redirect('users');
	}
	
	
	/**
	*	User::update()
	*	API-style function used by other functions to update and existing user on the system
	*/
	public function update()
	{	
		if($this->session->flashdata('user_url'))
			$this->session->keep_flashdata('user_url');
			
		// Grab the form data via POST requests
		$user_id		= $this->input->post('user_id');
		$username		= $this->input->post('username');
		$password		= $this->input->post('password');
		$name			= $this->input->post('name');
		$email			= $this->input->post('email');
		$department		= $this->input->post('department');
		$type			= $this->input->post('type');
		
		// Pass the data to the model to be added, and get the response
		$update_user = $this->user_model->updateUser($user_id,
													$username,
													$name,
													$email,
													$department,
													$type);
													
		switch($update_user)
		{
			// Successfully updated
			case(1):
				// Log the action
				$this->activity_log->addLog($this->session->userdata('id'), 'update', 'user', "Updated user <a href='" . base_url('users/view/' . $user_id) . "'>$username</a> ($name)");
				
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Okie dokes!</strong> User details updated.'));
			break;
			
			// Error updating
			case(2):
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Uh oh!</strong> There was a problem updating that user.'));
			break;
		}
		
		if($this->session->flashdata('user_url'))
			redirect($this->session->flashdata('user_url'));
		else
			redirect('users');
	}
	
	
	/**
	*	User::delete()
	*	Deletes a user from the system
	*/
	public function delete()
	{
		// Check to make sure the user is allowed to access this area..
		checkPermissions('management', true);
		
		// Get the POST data from the form
		$user_id	 		= $this->input->post('user_id');
		$username			= $this->user_model->getUsername($user_id);
		
		// Pass data to the model to delete the user, and store the response
		$delete_user = $this->user_model->deactivateUser($user_id);
		
		// Based on the model's response, return a specific alert to the user (success, error)
		switch($delete_user)
		{
			// Successfully added
			case(1):
				// Log the action
				$this->activity_log->addLog($this->session->userdata('id'), 'delete', 'user', "Deactivated user <a href='" . base_url('users/view/' . $user_id) . "'>$username</a>");
				
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>User deactivated</strong> Successfully purged it from the databanks.'));
			break;
			
			// Error whilst adding
			case(2):
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw snap!</strong> There was a problem deactivating that user, please try again in a bit.'));
			break;
		}
		
		// Redirect the user back to the user list
		redirect('users');
	}
	

	/**
	*	User::account()
	*	Allows a user to update their own user account data
	*/
	public function account()
	{	
		if($this->input->post('changePassword'))
		{
			$this->form_validation->set_rules('currentPassword', 'Current password', 'required');
			$this->form_validation->set_rules('newPassword', 'Password', 'required|matches[newPasswordConfirm]');
			$this->form_validation->set_rules('newPasswordConfirm', 'Password confirmation', 'required');
		}
		
		if($this->input->post('updateInfo'))
		{
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'valid_email');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['account_info']		= $this->user_model->getAccountInfo($this->session->userdata('id'));
			$data['contentlocation'] = 'user/account';
			$this->load->view('system/template', $data);
		}
		else
		{
			// If changing passwords..
			if($this->input->post('changePassword'))
			{
				$user_id 			= $this->session->userdata('id');
				$currentPassword 	= sha1($this->input->post('currentPassword'));
				$newPassword 		= sha1($this->input->post('newPassword'));
				
				switch($this->updatePassword($user_id, $newPassword, $currentPassword))
				{
					// Successfully updated
					case(1):
						$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Okie dokes!</strong> Your password was updated.'));
					break;
					
					// Incorrect current password
					case(2):
						$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Uh oh!</strong> The current password you entered is incorrect.'));
					break;
					
					// Other error
					case(3):
						$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw man!</strong> There was a problem updating your password, try again in a bit.'));
					break;
				}
				redirect('account');
			}
			
			// If updating info..
			if($this->input->post('updateInfo'))
			{
				$user_id 			= $this->session->userdata('id');
				$name			 	= $this->input->post('name');
				$email		 		= $this->input->post('email');
				
				$update_data = array('name' => $name, 'email' => $email);
				
				if($this->user_model->updateAccountInfo($user_id, $update_data))
					$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Okie dokes!</strong> Your information was updated.'));
				else
					$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw man!</strong> There was a problem updating your information, try again in a bit.'));

				redirect('account');
			}
		}
	}
	
	
	/**
	*	User::updatePassword()
	*	Updates a user's password (used by update() and account())
	*
	*	@param 	int 	$user_id 		The ID of the user to update
	*	@param 	int 	$new_password 	The new password to update to
	*	@param 	int 	$old_password 	OPTIONAL: The existing password (used by account(), not by update() admin reset)
	*/
	public function updatePassword($user_id, $new_password, $old_password = FALSE)
	{
		if($this->session->flashdata('user_url'))
			$this->session->keep_flashdata('user_url');
			
		if($old_password !== FALSE)
			$old_password = sha1($old_password);
			
		$result = $this->user_model->updatePassword($user_id, $new_password, $old_password);
		
		return $result;
	}
	
	
	/**
	*	User::blowfish()
	*	Generates an encrypted password
	*
	*	@param 	string 	$input 		The password string to encrypt
	*	@param 	int 	$rounds 	OPTIONAL: The number of encryption passes to do on the password string
	*/
	function blowfish($input, $rounds = 5)
	{
		$salt = "";
		$salt_chars = array_merge(range('A','Z'), range('a','z'), range(0,9));
		for($i=0; $i < 22; $i++) {
			$salt .= $salt_chars[array_rand($salt_chars)];
		}
		return crypt($input, sprintf('$2a$%02d$', $rounds) . $salt);
	}
	
	
	/**
	*	User::checkUsernameExists()
	*	API function used by AJAX form validation on user creation
	*/
	public function checkUsernameExists() {
		$username = $this->input->post('username');
		$result = $this->user_model->checkUsernameExists($username);
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
		{
			if($result == TRUE) {
				echo "true";
			} else {
				echo "false";
			}
		}
		else
		{
			if($result == TRUE) {
				$this->form_validation->set_message('checkUsernameExists', 'That username already exists, please try another one.');
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}
}


/* End of file user.php */
/* Location ./application/controllers/user.php */