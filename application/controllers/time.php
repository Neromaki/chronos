<?php
/**
 * Time Controller Class
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Time extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Europe/London');
		
		// Check if user is logged in
		$this->load->library('Login');
			$this->login->checkLogin();
		
		// Load the models
		$this->load->model('time_model');
		$this->load->model('user_model');
		$this->load->model('department_model');
		$this->load->model('project_model');
		
		// Enables the profiler for debugging output
		if(checkProfiler())
			$this->output->enable_profiler(TRUE);
	}
	

	/**
	*	Time::dashboard()
	*	Default list view for /time, listing all times logged for the current user	
	*/
	public function dashboard()
	{
		// If we're filtering between a start AND and end date, check if start is before end
		if($this->input->post('filter') && ($this->input->post('filter_date_start') && $this->input->post('filter_date_end')))
		{
			$this->form_validation->set_rules('filter_date_start', 'Date start', 'trim|callback_checkPeriod['.$this->input->post('filter_date_end').']');
		}
		// If we're filtering the times, perform form validation on the filter fields
		if($this->input->post('filter'))
		{
			$this->form_validation->set_rules('filter_date_end', 'Date end', 'trim|min_length[0]');
			$this->form_validation->set_rules('filter_project', 'Project', 'trim|min_length[0]');

			$filter_date_start = trim($this->input->post('filter_date_start'));
			$filter_date_end = trim($this->input->post('filter_date_end'));
			$filter_project = trim($this->input->post('filter_project'));
		}
		// Set up the pagination logic
		$timePerPage = 20;
		$page = $this->uri->segment(2);
		
		$filter_data = array();		
		$filter_data['user_id'] = $this->session->userdata('id');

		// If we're filtering, collect the relative data to be used to search
		if(!empty($filter_date_start))
			$filter_data['date_start'] = $filter_date_start;
		if(!empty($filter_date_end))
			$filter_data['date_end'] = $filter_date_end;
		if(!empty($filter_project))
			$filter_data['project'] = $filter_project;
		
		$offset = ($page  === FALSE) ? 0 : ($page * $timePerPage) - $timePerPage;
		
		$data['filters'] = $filter_data;
		
		if($this->form_validation->run() == FALSE)
		{
			$filter_data = array();
			$filter_data['user_id'] = $this->session->userdata('id');
		}
		
		// Grab the user's projects, load the view and pass the data over
		$data['time'] = $this->time_model->getUserTime($filter_data,
														$offset,
														$timePerPage);
		// Count the total rows for pagination
		$total_rows = $this->time_model->countTime($filter_data);
		
		// Initiate the pagination library
		$this->load->library('paginate');
		$data['pagination']			= $this->paginate->init('time', $total_rows, $timePerPage);

		// Get all the project names for auto-complete in the view
		$projects = $this->project_model->getAllProjectNames();
		foreach($projects as $p) 
		{
			$data['projects'][] = $p->project_name;
		}
		
		$data['contentlocation'] = 'time/dashboard';
		$this->load->view('system/template', $data);
	}
	
	
	/**
	*	Time::getTimeData()
	*	Gets the related data for a specific logged time (duration, project etc)
	*	Echos a json_encoded object if being accessed by AJAX, or returns a PHP array
	*
	*	@param 	int 	$time_id 	The ID of the logged time
	*	@return array  	The data array for the time ID
	*/
	public function getTimeData($time_id = FALSE)
	{
		if(!$time_id)
			$time_id = $this->input->post('time_id');
			
		if($time_id)
			$result = $this->time_model->getTimeData($time_id);
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
		{
			if($result) {
				echo json_encode($result);
			} else {
				echo "false";
			}
		}
		else
		{
			if($result) {
				return $result;
			} else {
				return FALSE;
			}
		}
	}
	
	
	/**
	*	Time::add()
	*	Allows a user to add one or more time objects to the system
	*/
	public function add()
	{
		// Adding one time entry
		if($this->input->post('add_day_time'))
		{
			// Get all the posted form data
			$form_data = $this->input->post();
			
			// Loop through the POST collecting the data, and validating what is retrieved
			for($x=1; isset($form_data['date' . $x]); $x++)
			{
				$time_data[$x]['date'] 		= $this->input->post('date' . $x);
				$time_data[$x]['project'] 	= $this->input->post('project' . $x);
				$time_data[$x]['duration'] 	= $this->input->post('duration' . $x);
				
				// Set the form validation rules
				$this->form_validation->set_rules('date'.$x, 'Date '.$x, 'trim|required');
				$this->form_validation->set_rules('project'.$x, 'Project '.$x, 'trim|required|callback_checkProjectName');
				$this->form_validation->set_rules('duration'.$x, 'Duration '.$x, 'trim|required|callback_checkDuration');
			}
			
			$this->session->unset_userdata('time_data');
			$this->session->set_userdata('time_data', $time_data);
		}
		// Adding a period (multiple) of time entries
		elseif($this->input->post('add_period_time'))
		{
			$form_data = $this->input->post();
			
			$time_data['period_start'] 		= $this->input->post('period_start');
			$time_data['period_end'] 		= $this->input->post('period_end');
			$time_data['period_project'] 	= $this->input->post('period_project');
			$time_data['period_duration'] 	= $this->input->post('period_duration');
			
			$time_data['period_mondays'] 	= $this->input->post('period_mondays');
			$time_data['period_tuesdays'] 	= $this->input->post('period_tuesdays');
			$time_data['period_wednesdays'] = $this->input->post('period_wednesdays');
			$time_data['period_thursdays'] 	= $this->input->post('period_thursdays');
			$time_data['period_fridays'] 	= $this->input->post('period_fridays');
			$time_data['period_saturdays'] 	= $this->input->post('period_saturdays');
			$time_data['period_sundays'] 	= $this->input->post('period_sundays');
			
			// Set the form validation rules
			$this->form_validation->set_rules('period_start', 'Date start', 'trim|required|callback_checkPeriod['.$this->input->post('period_end').']');
			$this->form_validation->set_rules('period_end', 'Date end', 'trim|required');
			$this->form_validation->set_rules('period_project', 'Project', 'trim|required|callback_checkProjectName');
			$this->form_validation->set_rules('period_duration', 'Duration', 'trim|required|callback_checkDuration');
			
			$this->session->unset_userdata('time_data');
			$this->session->set_userdata('time_data', $time_data);
		}
		
		
		if($this->form_validation->run() == FALSE)
		{
			$projects = $this->project_model->getAllProjectNames();

			for($x=0;$x<count($projects);$x++) 
			{
				$data['project_names'][$x] = $projects[$x]->project_name;
				$data['project_descs'][$x] = $projects[$x]->description;
			}
			$data['contentlocation'] = 'time/add';
			$this->load->view('system/template', $data);
		}
		else
		{
			// Prepare the data so it can be inserted straight into the database
			
			// If we're adding on a per-day basis..
			if($this->input->post('add_day_time'))
			{
				// We just need to change the date formats of each item to MySQL DATETIME
				foreach($time_data as &$td)
				{
					// Create a DateTime object from our UK-format
					$date = DateTime::createFromFormat('d/m/Y', $td['date']);
					
					// Convert it to DATETIME and replace the existing
					$td['date'] = $date->format('Y-m-d');
				}
			}
			
			// If we're adding a period of dates
			elseif($this->input->post('add_period_time'))
			{
				// Create DateTime objects from the period start and end (we have to modify the $end to be +1 day so it actually counts the last day)
				$start = DateTime::createFromFormat('d/m/Y', $time_data['period_start']);
				$end = DateTime::createFromFormat('d/m/Y', $time_data['period_end']);
				$end = $end->modify( '+1 day' ); 
				
				// Copy $time_data, as we'll want to use it for the processed output
				$temp = $time_data;
				$time_data = array();
				
				// Create a DatePeriod object from the start and end dates, getting each intervening day
				$period = new DatePeriod(
					 new DateTime($start->format('Y-m-d')),
					 new DateInterval('P1D'),
					 new DateTime($end->format('Y-m-d'))
				);
				
				$x=0;
				
				// For each date in the DatePeriod object
				foreach($period as $p)
				{
					// We check if the user has specified to include that day
					$day = date('N', $p->getTimestamp());
					$include_day = FALSE;
					
					switch($day)
					{
						case(1):
							if($temp['period_mondays'] == 1)
								$include_day = TRUE;						
						break;
						
						case(2):
							if($temp['period_tuesdays'] == 1)
								$include_day = TRUE;
						break;
						
						case(3):
							if($temp['period_wednesdays'] == 1)
								$include_day = TRUE;
						break;
						
						case(4):
							if($temp['period_thursdays'] == 1)
								$include_day = TRUE;
						break;
						
						case(5):
							if($temp['period_fridays'] == 1)
								$include_day = TRUE;
						break;
						
						case(6):
							if($temp['period_saturdays'] == 1)
								$include_day = TRUE;
						break;
						
						case(7):
							if($temp['period_sundays'] == 1)
								$include_day = TRUE;
						break;
					}
					
					// If the user specified that they want to include the day in their time periods, add it
					if($include_day)
					{
						$time_data[$x]['date'] = $p->format('Y-m-d');
						$time_data[$x]['project'] = $temp['period_project'];
						$time_data[$x]['duration'] = $temp['period_duration'];
						$x++;
					}
				}
			}
			
			$user_id = $this->session->userdata('id');
			$dept_id = $this->user_model->getUserDepartment($user_id);
			
			$errors = array();
			$added = array();
			
			foreach($time_data as &$td)
			{
				$project_id = $this->project_model->getProjectByName($td['project']);
				$project_name = $td['project'];
				$td['project'] = $project_id;
				$td['user_id'] = $user_id;
				$td['dept_id'] = $dept_id;
				
				$result = $this->time_model->addTime($td);
				
				if(!$result)
					$errors[] = $td['date'];
				else {
					$added[] = $result;
					$this->activity_log->addLog($this->session->userdata('id'), 'add', 'time', "Added ".$td['duration']." hour".($td['duration'] != 1 ? 's' : '')." to <a href=".base_url('projects/view/'.$project_id).">".$project_name."</a> for ".date('d/m/y (D)', strtotime($td['date'])));
				}
			}
			
			if(count($errors) == count($time_data))
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Damnit!</strong> Your time(s) could not be added, please try again or contact an administrator.'));
			elseif(count($errors) > 0)
			{
				$failed_dates = "";
				for($x=0;$x<count($errors);$x++)
				{
					$failed_dates .= $errors[$x];
					
					if($x < (count($errors)-1))
						$failed_dates .= ", ";
				}
				$this->session->set_flashdata(array('alert' => true, 'type' => 'warning', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Whoops!</strong> Everything except the dates ('.$failed_dates.') were added. Please try adding the failed ones again or contact an administrator.'));
			}
			else
			{
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Hurrah!</strong> Your time(s) were added successfully.'));
				$this->session->unset_userdata('time_data');
			}
			
			if(!empty($added))
				$this->session->set_flashdata('time_added', $added);

			redirect('time');
		}
	}
	

	/**
	*	Time::update()
	*	Allows a user to update a time entry on the system
	*
	*	@param 	int 	$time_id 	OPTIONAL: The ID of the time object to update (if not being posted)
	*	@param 	int 	$user_id 	OPTIONAL: The ID of the user updating (if not being posted)
	*	@param 	int 	$data 	 	OPTIONAL: Time data to update with (if not being posted)
	*/
	public function update($time_id = FALSE, $user_id = FALSE, $data = FALSE)
	{
		if(!$time_id)
			$time_id = $this->input->post('time_id');
			
		if(!$user_id)
			$user_id = $this->session->userdata('id');
	
		// Gather the form post data for the update		
		if(!$data)
		{
			$date 		= trim($this->input->post('update_date'));
			$project 	= trim($this->input->post('update_project'));
			$duration 	= trim($this->input->post('update_duration'));
			
			$data = array();
			
			if(!empty($date)) {
				// Create a DateTime object from our UK-format
				$date = DateTime::createFromFormat('d/m/Y', $date);
				// Convert it to DATETIME and replace the existing
				$data['date'] = $date->format('Y-m-d');
			}
			// If the project is set..
			if(!empty($project)) {
				// Check if it's numeric (thus an id)
				if(is_numeric($project)) {
					$data['project_id'] = $project;	
				} 
				// If not, it's probably a name, so try and grab the ID using it
				else {
					$project_id = $this->project_model->getProjectByName($project);
					if($project_id) {
						$data['project_id'] = $project_id;
					}
				}				
			}
			if(!empty($duration))
				$data['duration'] = $duration;
		}
		
		// If we've got some data..
		if(!empty($data))
		{
			// Get the current user and time-being-updated info
			$user = $this->user_model->getAccountInfo($user_id);
			$time_info = $this->time_model->getTimeData($time_id);
			
			// Check user permissions
			if(($user->user_id == $user_id) ||
				($user->account_type == 1) ||
				($user->account_type == 2 && $this->user_model->canManageUser($user_id, $data->user_id))
				)
			{
				$this->time_model->updateTime($time_id, $data);
				
				if(isset($data['duration']))
					$duration = $duration;
				else
					$duration = $time_info->duration;
				
				$this->activity_log->addLog($this->session->userdata('id'), 'update', 'time', "Updated time to ".$data['duration']." hour".($data['duration'] != 1 ? 's' : '')." to <a href=".base_url('projects/view/'.$data['project_id'].">".$project."</a> for ".date('d/m/y (D)', strtotime($data['date']))));
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Hurrah!</strong> That time was updated successfully.'));
			}
			else
			{
				// cannot delete time
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Sorry!</strong> Either that time does not belong to you, or you do not have the permissions to update it.'));
			}
		}
		else
		{
			// time not found
			$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Drats!</strong> That time could not be found, thus could not be updated.'));
		}
		redirect('time');	
	}
	

	/**
	*	Time::delete()
	*	Allows a user to delete a time entry from the system
	*
	*	@param 	int 	$time_id 	OPTIONAL: The ID of the time object to update (if not being posted)
	*	@param 	int 	$user_id 	OPTIONAL: The ID of the user updating (if not being posted)
	*/
	public function delete($time_id = FALSE, $user_id = FALSE)
	{
		if(!$time_id)
			$time_id = $this->input->post('time_id');
			
		if(!$user_id)
			$user_id = $this->session->userdata('id');
			
		$data = $this->time_model->getTimeData($time_id);
		
		if($data)
		{
			$user = $this->user_model->getAccountInfo($user_id);
			
			if(($data->user_id == $user_id) ||
				($user->account_type == 1) ||
				($user->account_type == 2 && $this->user_model->canManageUser($user_id, $data->user_id))
				)
			{
				$this->time_model->deleteTime($time_id);
				$this->activity_log->addLog($this->session->userdata('id'), 'delete', 'time', "Deleted time of ".$data->duration." hour".($data->duration != 1 ? 's' : '')." to <a href=".base_url('projects/view/'.$data->project_id).">".$data->project_name."</a> for ".date('d/m/y (D)', strtotime($data->date)));
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Hurrah!</strong> That time was deleted successfully.'));
			}
			else
			{
				// cannot delete time
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Sorry!</strong> Either that time does not belong to you, or you do not have the permissions to delete it.'));
			}
		}
		else
		{
			// time not found
			$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Drats!</strong> That time could not be found, thus could not be deleted.'));
		}
		redirect('time');
	}
	
	
	/**
	*	Time::checkProjectName()
	*	Check that the supplied project name is a valid project in the system
	*
	*	@param 	string 	$project_name 	The project name to search on
	*/
	function checkProjectName($project_name)
	{			
		$project_id = $this->project_model->getProjectByName($project_name);
		
		if($project_id)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('checkProjectName', "The project '<strong>$project_name</strong>' was not found.");
			return FALSE;
		}
	}
	

	/**
	*	Time::checkDuration()
	*	Form validation callback which checks if a supplied duration is valid
	*
	*	@param 	float 	$Duration 	The int / float duration to be checked
	*	@return bool	Returns true if valid, false if not
	*/
	function checkDuration($duration)
	{
		if(is_numeric($duration))
		{
			if($duration > 0)
				return TRUE;
			else
			{
				$this->form_validation->set_message('checkDuration', 'Duration must be greater than 0.');	
				return FALSE;
			}
		}
		else
		{
			$this->form_validation->set_message('checkDuration', "Invalid duration '<strong>$duration</strong>' - must be either an integer <em>(1, 2, 3 etc)</em> or decimal <em>(1.5, 2.75 etc)</em>");
			return FALSE;
		}
	}
	

	/**
	*	Time::checkPeriod()
	*	Form validation callback which checks if a supplied start date is before the supplied end date
	*
	*	@param 	date 	$start 	The start date in UK format (27/01/2014)
	*	@param 	date 	$end 	The end date in UK format (28/01/2014)
	*/
	function checkPeriod($start, $end)
	{
		// Could've used DATETIME instead, but this was before I realised DATETIME was a thing
		// Takes a UK formatted date and returns it as a database format (27/01/2014 -> 2014-01-27)
		function shuffleDate($date) 
		{
			$date_bits = explode('/', $date);
			return $date_bits[2].'-'.$date_bits[1].'-'.$date_bits[0];
		}
		$start 	= strtotime(shuffleDate($start));
		$end 	= strtotime(shuffleDate($end));
		
		if($start > $end)
		{
			$this->form_validation->set_message('checkPeriod', 'Start date cannot be after the end date.');	
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}

/* End of file time.php */
/* Location ./application/controllers/time.php */