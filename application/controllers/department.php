<?php
/**
 * Department Controller Class
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department extends CI_Controller {

	public function __construct()
	{
		// Constructs CodeIgniter's CI_Controller parent
		parent::__construct();
		
		// Sets the default timezone
		date_default_timezone_set('Europe/London');
		
		// Loads the Login library and uses it to check the user's session status
		$this->load->library('Login');
			$this->login->checkLogin();
		
		// Loads the models to be used by the controller
		$this->load->model('site_model');
		$this->load->model('user_model');
		$this->load->model('department_model');
		$this->load->model('project_model');
		
		// Enables the profiler for debugging output
		if(checkProfiler())
			$this->output->enable_profiler(TRUE);
	}

	
	/**
	*	Department::index()
	*	Default list view showing all departments
	*/
	public function index()
	{	
		// Check to make sure the user is allowed to access this area..
		checkPermissions('all', true, true);
		
		// If they are a manager, redirect them to their department page
		// if(checkPermissions('manager')) 
		// {
		// 	redirect('departments/view/'.$this->session->userdata('department'));
		// }		
		
		// Sets the form validation for the Add Task form
		if($this->input->post('add'))
		{
			$this->form_validation->set_rules('name', 'Name', 'required');
		}
		
		if($this->form_validation->run() == FALSE)
		{
			$name = NULL;
			
			if($this->input->post('filter'))
				$name = $this->input->post('name');
			
			$data['appSettings'] 		= $this->site_model->getAppSettings($this->session->userdata('id')); 
			$deptsPerPage = 20;
			
			$page = $this->uri->segment(2);
			
			$offset = ($page  === FALSE) ? 0 : ($page * $deptsPerPage) - $deptsPerPage;
			
			$data['departments'] = $this->getDepartments($name,
											$offset,
											$deptsPerPage);		// Gets the user's tasks and projects
			
			$total_rows = $this->department_model->countDepartments($name);
			
			$this->load->library('paginate');
			
			$data['pagination']			= $this->paginate->init('departments', $total_rows, $deptsPerPage);
			$data['appSettings'] 		= $this->site_model->getAppSettings($this->session->userdata('id')); 
			$data['contentlocation'] 	= 'department/departments';
			$this->load->view('system/template', $data);
		}
		else
		{
			// If the Add Task form has been submitted, process the data
			$this->addDepartment();
		}
	}
	
	
	/**
	*	Department::getDepartments()
	*	Gets all the departments matching a search string
	*
	*	@param 	string 	$name 			The search string (department name) to search with
	*	@param 	int 	$limit_page		Used by the pagination
	*	@param 	int 	$limit_count		Used by the pagination
	*/
	public function getDepartments($name = NULL,
							$limit_page = NULL,
							$limit_count = NULL)
	{
		$user_id = $this->session->userdata('id');
		
		$departments = $this->department_model->getDepartments($name,
																$limit_page,
																$limit_count);
											
		return $departments;
	}

	
	/**
	*	Department::add()
	*	Adds a new department to the system
	*/
	public function add()
	{
		// Check to make sure the user is allowed to access this area..
		checkPermissions('admin', true, true);
			
		// Sets the form validation for the Add Task form
		$this->form_validation->set_rules('name', 'Name', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['contentlocation'] = 'department/add_department';
			$this->load->view('system/template', $data);
		}
		else
		{
			// If the Add Task form has been submitted, process the data
			$this->addDepartment();
		}
	}


		/**
		*	Department::addDepartment()
		*	Actual action for adding tasks to the system. 
		*
		*	@param 	string 	$name 			The search string (department name) to search with
		*	@param 	int 	$limit_page		Used by the pagination
		*	@param 	int 	$limit_count		Used by the pagination
		*/
		public function addDepartment()
		{
			// Check to make sure the user is allowed to access this area..
			checkPermissions('admin', true, true);	
		
			// Grab the relevant POST variables from the HTML form
			$name	= $this->input->post('name');
			
			// Throw the variable at the relevant model function, and assign the model's response to $add_task
			$add_dept = $this->department_model->addDepartment($name);
		
			// Based on the model's response, return a specific alert to the user (success, error)
			switch($add_dept['outcome'])
			{
				// Successfully added
				case(1):
					// Log the action
					$this->activity_log->addLog($this->session->userdata('id'), 'add', 'department', "Added department <a href='" . base_url('departments/view/' . $add_dept['dept_id']) . "'>$name</a>");
					$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Department added!</strong> Ready to have folks assigned to it.'));
				break;
				
				// Error whilst adding
				case(2):
					$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw snap!</strong> There was a problem adding that department, please try again in a bit.'));
				break;
				
				// User is not able to use specified project
				case(3):
					$this->session->set_flashdata(array('alert' => true, 'type' => 'warning', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Hold on!</strong> You are not able to create departments!'));
				break;
			}
			
			redirect('departments');
		}
	
	
	/**
	*	Department::view()
	*	Views all the data for a particular department
	*
	*	@param 	int 	$dept_id 		The ID of the department to view
	*/
	public function view($dept_id)
	{		
		// Check to make sure the user is allowed to access this area..
		checkPermissions('management', true, true);
		
		// If they are a manager, make sure they are a manager of this department,
		// otherwise, redirect them to the department list (which will redirect to their dept)
		/*
		if(checkPermissions('manager'))
		{
			if(!canManageDepartment($dept_id))
				redirect('departments');
		}	
		*/	
		
		$this->session->set_flashdata('dept_url', current_url());
		
		// Check if the department ID actually exists..
		if($data['dept_data'] = $this->department_model->getDepartmentData($dept_id))
		{
			if($this->input->post('filter'))
			{
				$start = DateTime::createFromFormat('d/m/Y', $this->input->post('period_start'))->format('Y-m-d');
				$end = DateTime::createFromFormat('d/m/Y', $this->input->post('period_end'))->format('Y-m-d');

				$data['dept_data']->total = $this->department_model->getDepartmentStats($dept_id, $start, $end);
				$data['dept_users'] = $this->department_model->getDepartmentLoggedTime($dept_id, $start, $end);
			}
			else
			{
				$thisWeekStart	= date('Y-m-d', strtotime(date('Y')."-W".date('W')."-1"));
				$thisWeekEnd	= date('Y-m-d', strtotime(date('Y')."-W".date('W')."-7"));
				$lastWeekStart	= date('Y-m-d', strtotime(date('Y')."-W".(date('W')-1)."-1"));
				$lastWeekEnd	= date('Y-m-d', strtotime(date('Y')."-W".(date('W')-1)."-7"));
				$thisMonthStart	= date('Y-m-d', strtotime("first day of this month"));
				$thisMonthEnd	= date('Y-m-d', strtotime("last day of this month"));
				$lastMonthStart	= date('Y-m-d', strtotime("first day of last month"));
				$lastMonthEnd	= date('Y-m-d', strtotime("last day of last month"));

				
				$data['dept_data']->thisWeek = $this->department_model->getDepartmentStats($dept_id, $thisWeekStart, $thisWeekEnd);
				$data['dept_data']->lastWeek = $this->department_model->getDepartmentStats($dept_id, $lastWeekStart, $lastWeekEnd);
				$data['dept_data']->thisMonth = $this->department_model->getDepartmentStats($dept_id, $thisMonthStart, $thisMonthEnd);
				$data['dept_data']->lastMonth = $this->department_model->getDepartmentStats($dept_id, $lastMonthStart, $lastMonthEnd);
				$data['dept_data']->recentProjects = $this->department_model->getDeptRecentProjects($dept_id);
				
				$data['dept_users'] = $this->department_model->getDepartmentLoggedTime($dept_id);
			}

			$data['departments'] = $this->getDepartments();
			$data['contentlocation'] = 'department/view_department';
			$this->load->view('system/template', $data);
			
		}
		else
		{
			// No department found
			$this->session->set_flashdata(array('alert' => true, 'type' => 'warning', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Dang!</strong> That department was not found.'));
			redirect('departments');
		}
	}
	
	
	/**
	*	Department::update()
	*	Updates the data of a particular department on the system
	*/
	public function update()
	{
		// Check to make sure the user is allowed to access this area..
		checkPermissions('admin', true, true);
			
		// Grab the relevant POST variables from the HTML form
		$dept_id	= $this->input->post('dept_id');
		$name		= $this->input->post('name');
		
		// Throw the variable at the relevant model function, and assign the model's response to $add_task
		$update_department = $this->department_model->updateDepartment($dept_id, $name);
	
		// Based on the model's response, return a specific alert to the user (success, error)
		switch($update_department)
		{
			// Successfully updated
			case(1):
				// Log the action
				$this->activity_log->addLog($this->session->userdata('id'), 'update', 'department', "Updated department <a href='" . base_url('departments/view/' . $dept_id) . "'>$name</a>");	
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Department updated!</strong>'));
			break;
			
			// Error whilst adding
			case(2):
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw snap!</strong> There was a problem updating the department, please try again in a bit.'));
			break;
		}
		
		redirect('departments/view/' . $dept_id);
	}
	

	/**
	*	Department::delete()
	*	Deactivates a particular department on the system
	*/
	public function delete()
	{
		// Check to make sure the user is allowed to access this area..
		checkPermissions('admin', true, true);
			
		$dept_id 		= $this->input->post('dept_id');
		$dept_name		= $this->department_model->getDepartmentName($dept_id);
		$remove_action 	= $this->input->post('remove_action'); // 'delete' or 'migrate'
		$action_groups	= $this->input->post('action_group'); // users, time, costs
		$new_dept_id	= $this->input->post('new_dept_id'); // if new, 'new', if existing dept, dept_id
		$new_dept_name	= $this->input->post('new_dept_name'); // if $new_dept_id = 'new', this will be the dept name
		
		$log = "Deleted department <a href='" . base_url('departments/view/' . $dept_id) . "'>$dept_name</a>.";
		
		if($remove_action == 'delete')
		{
			if(in_array('users'))
				$this->department_model->removeUsers($dept_id);
			if(in_array('time'))
				$this->department_model->removeTime($dept_id);
			if(in_array('costs'))
				$this->department_model->removeCosts($dept_id);
				
			$log .= " Deleted all users, time and costs.";
		}
		elseif($remove_action == 'migrate')
		{
			if($new_dept_id == 'new')
			{
				$new_dept_id = $this->department_model->addDepartment($new_dept_name);
			}
			if(is_numeric($new_dept_id) && $this->department_model->checkDepartmentExists($new_dept_id))
			{			
				if(in_array('users', $action_groups))
					$this->department_model->migrateUsers($dept_id, $new_dept_id);
				if(in_array('time', $action_groups))
					$this->department_model->migrateTime($dept_id, $new_dept_id);
				if(in_array('costs', $action_groups))
					$this->department_model->migrateCosts($dept_id, $new_dept_id);
					
				$new_dept_name = $this->department_model->getDepartmentName($new_dept_id);
					
				$log .= " Migrated all users, time and costs to <a href='" . base_url('departments/view/' . $new_dept_id) . "'>$new_dept_name</a>.";
			}
			else
			{
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Error!</strong> The department you are migrating to does not exist.'));
				if($this->session->flashdata('dept_url'))
					redirect($this->session->flashdata('dept_url'));
				else
					redirect('departments');
			}
		}
		else
		{
			$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Error!</strong> Invalid removal action (should be "remove" or "migrate").'));
			if($this->session->flashdata('dept_url'))
				redirect($this->session->flashdata('dept_url'));
			else
				redirect('departments');
		}
		
		$this->department_model->deactivate($dept_id);
		
		// Log the action
		$this->activity_log->addLog($this->session->userdata('id'), 'delete', 'department', $log);
		$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Department deactivated</strong> Successfully '.($remove_action == 'migrate' ? 'migrated' : 'removed').' department.'));
		redirect('departments');
	}
	
}

/* End of file department.php */
/* Location ./application/controllers/department.php */