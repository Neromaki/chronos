<?php
/**
 * Site Controller Class
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Europe/London');
		
		// Check if the user is logged in
		$this->load->library('Login');
			$this->login->checkLogin();
		
		// Load the models
		$this->load->model('site_model');
		$this->load->model('user_model');
		
		// Enables the profiler for debugging output
		if(checkProfiler())
			$this->output->enable_profiler(TRUE);
	}


	/**
	*	Site::index()
	*	Default home view (when first logged in)
	*/
	public function index()
	{
		$data['contentlocation'] = 'site/home';
		$this->load->view('system/template', $data);
	}
	
	
	/**
	*	Site::settings()
	*	The Chronos settings page
	*/
	public function settings()
	{
		$this->form_validation->set_rules('showTaskAge', 'Show task age', 'minlength[0]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['appSettings'] 		= $this->site_model->getAppSettings($this->session->userdata('id')); 
			$data['contentlocation'] 	= 'site/settings';
			$this->load->view('system/template', $data);
		}
		else
		{
			$settings = array();
			
			// Task settings
			$settings['showTaskAge']	= $this->input->post('showTaskAge');
			$settings['tasksPerPage']	= $this->input->post('tasksPerPage');
			
			// Project settings
			$settings['projectsPerPage']	= $this->input->post('projectsPerPage');
			
			if($this->site_model->updateAppSettings($this->session->userdata('id'), $settings))
				$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Sweet!</strong> Settings successfully saved.'));
			else
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Aw dang!</strong> There was a problem saving your settings, try again in a bit.'));
			
			redirect('settings');
		}
	}
	
	
	/**
	*	Site::changelog()
	*	The internal changelog page (not used in favour of BitBucket)
	*/
	public function changelog()
	{
		$data['contentlocation'] = 'site/changelog';
		$this->load->view('system/template', $data);
	}
	

	/**
	*	Site::about()
	*	The About Chronos page
	*/
	public function about()
	{
		$data['contentlocation'] = 'site/about';
		$this->load->view('system/template', $data);
	}
}

/* End of file site.php */
/* Location ./application/controllers/site.php */