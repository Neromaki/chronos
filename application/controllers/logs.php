<?php
/**
 * Logs Controller Class
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Europe/London');
		
		$this->load->library('Login');
			$this->login->checkLogin();
			
		// Check to make sure the user is allowed to access this area..
		checkPermissions('management', true);	
		
		$this->load->model('log_model');
		
		// Enables the profiler for debugging output
		if(checkProfiler())
			$this->output->enable_profiler(TRUE);
	}
	

	/**
	*	Logs::index()
	*	The main list of logs
	*/
	public function index()
	{			
		$logsPerPage = 20;
		$page = $this->uri->segment(2);
		$filter_data = array();
		
		/*
		if(checkPermissions('manager'))
			$filter_data['department'] = $this->session->userdata('department');
		*/
		
		if($this->input->post('search'))
		{
			$filter_data['start'] = $this->input->post('filter_start');
			$filter_data['end'] = $this->input->post('filter_end');
		}
		
		$offset = ($page  === FALSE) ? 0 : ($page * $logsPerPage) - $logsPerPage;
		
		// Grab the user's projects, load the view and pass the data over
		$data['logs'] = $this->log_model->getLogs($filter_data,
													$offset,
													$logsPerPage);
		
		$total_rows = $this->log_model->countLogs($filter_data);
		
		$this->load->library('paginate');
		
		$data['pagination']			= $this->paginate->init('logs', $total_rows, $logsPerPage);
		$data['contentlocation'] = 'logs/logs';
		$this->load->view('system/template', $data);
	}	
	
}

/* End of file logs.php */
/* Location ./application/controllers/logs.php */