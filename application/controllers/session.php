<?php
/**
 * Session Controller Class
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Session extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Europe/London');
		
		$this->load->model('site_model');
		
		// Enables the profiler for debugging output
		if(checkProfiler())
			$this->output->enable_profiler(TRUE);
	}


	/**
	*	Session::index()
	*	By default, /session will try to load the users view
	*/
	public function index()
	{
		$data['contentlocation'] = 'user/users';
		$this->load->view('system/template', $data);
	}
	

	/**
	*	Session::blowfish()
	*	Generated an encrypted and salted password based on an input string
	*
	*	@param 	string 	$input 	The password string to encrypt
	*	@return int  	The number of encryption passes to perform on the password string
	*/
	function blowfish($input, $rounds = 5)
	{
		$salt = "";
		$salt_chars = array_merge(range('A','Z'), range('a','z'), range(0,9));
		for($i=0; $i < 22; $i++) {
			$salt .= $salt_chars[array_rand($salt_chars)];
		}
		return crypt($input, sprintf('$2a$%02d$', $rounds) . $salt);
	}
	
	
	/**
	*	Session::compare_passwords()
	*	Will compare a plain string password with a hashed one
	*	Hashing the plain one before comparings, obvs.
	*
	*	@param 	string 	$enteredPassword 	A plaintext password
	*	@return string  $hashedPassword 	A hashed password to compare to
	*/
	function compare_passwords($enteredPassword, $hashedPassword)
	{
		if(sha1($enteredPassword) == $hashedPassword)
			return TRUE;
		else
			return FALSE; 
	}	
	
	
	/**
	*	Session::login()
	*	The login view and logic to log a user into the system
	*/
	public function login()
	{
		if($this->session->userdata('logged_in'))
			redirect();
			
		// Set the validation rules for the login form
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if($this->form_validation->run() == FALSE)
			$this->load->view('site/login');
		else
		{					
			// Grab the supplied credentials from $_POST
			$username = $this->input->post('username');
			$password = $this->input->post('password');
				
			if($user_data = $this->site_model->checkUsername($username))
			{	
				// Proceed with login
				if($this->compare_passwords($password, $user_data->password))
				{
					$this->load->model('user_model');
					$user_data = $this->user_model->getAccountInfo($user_data->user_id);
					
					// Build an array of data to be used as session
					$session = array(
						'logged_in' => TRUE,
						'id' => $user_data->user_id,
						'username' => $user_data->username,
						'department' => $user_data->dept_id,
						'department_name' => $user_data->dept_name,
						'account_type' => $user_data->account_type
						);
					
					// Chuck that data into the session
					$this->session->set_userdata($session);
					
					$session_id = session_id();
					
					$this->site_model->insertSessionReference($session_id, $user_data->user_id);
					
					// Successful login
					$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Hurrah!</strong> You have been logged in successfully.'));
					redirect('');
				}
				else
				{
					// Incorrect password
					$this->session->set_flashdata('login_username', $username);
					$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Darn!</strong> Incorrect password.'));
					redirect('login');
				}
			}
			else
			{
				// Username does not exist
				$this->session->set_flashdata('login_username', $username);
				$this->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Drats!</strong> That username does not exist.'));
				redirect('login');
			}

		}
	}
	
	
	/**
	*	Session::logout()
	*	Destroys the current user's session and logs them out
	*/
	public function logout()
	{
		// Unsets all CodeIgniter userdata
		$this->session->sess_destroy();
		
		// Redirects the user to the login screen with a confirmation message
		$this->session->set_flashdata(array('alert' => true, 'type' => 'success', 'message' => '<span class="glyphicon glyphicon-left glyphicon-ok"></span> <strong>Okay!</strong> Successfully logged out.'));
		redirect('login');
	}
	
}

/* End of file session.php */
/* Location ./application/controllers/session.php */