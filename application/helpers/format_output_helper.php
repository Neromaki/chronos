<?php
/**
 * Format Output Helper
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 *
 *	Collection of output formatting functions, including date conversions, time and cost formatting
*/

function getTimeSince($date)
{
	if(!$date)
		return "Never";
		
	$time = strtotime($date);
	$now = time();
	$diff = $now - $time;
	$hours = round(($diff / 60) / 60);
	$days = round($hours / 24);
	if($hours < 24)
		return "Today";
	elseif($hours < 48)
		return "Yesterday";
	elseif($days <= 30)
		return $days . " day".($days > 1 ? 's' : '')." ago";
	elseif($days > 30 && $days < 365)
		return round($days / 30) . " month".(round($days / 30) > 1 ? 's' : '')." ago";
	else
		return round($days / 365) . " year".(round($days / 365) > 1 ? 's' : '')." ago";
}


function formatTime($time, $precision = false)
{
	if(!$precision)
		$precision = 0;

	if(!$time)
		return '-';
	elseif($time == 1)
		return number_format($time, $precision) . ' hour';
	else
		return number_format($time, $precision) . ' hours';
}

function formatDateTime($datetime)
{
	return date('dS M Y (H:i)', strtotime($datetime));
}

function formatCost($cost, $precision = false)
{
	if(!$cost)
		return '-';
	else
	{
		if(!$precision)
			$precision = 0;

		return htmlentities('£') . number_format($cost, $precision);
	}
}


function formatDate($date, $from = 'uk', $to = 'db')
{
	switch($from)
	{
		case('db'):
			// Create a DateTime object from our UK-format
			$from = DateTime::createFromFormat('Y-m-d', $date);
		break;
		
		case('uk'):
		default:
			// Create a DateTime object from our UK-format
			$from = DateTime::createFromFormat('d/m/Y', $date);
		break;
	}
	
	switch($to)
	{
		case('uk'):
			$to = $from->format('d/m/Y');
		break;
		
		case('db'):
		default:
			// Convert it to DATETIME and replace the existing
			$to = $from->format('Y-m-d');
		break;	
	}
	
	return $to;
}

/* End of file format_output_helper.php */
/* Location ./application/helpers/format_output_helper.php */