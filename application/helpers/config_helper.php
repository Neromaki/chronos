<?php
/**
 * Config Helper
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 *
 *	Checks to see if the profiler should be enabled in the requesting controller
 *	Returns true if:
 *		- Constant STATE is set to 'development' (see /config/constants.php)
 *		- If the request isn't being made by an AJAX call (profiler messes this up)
*/
function checkProfiler()
{
	if(STATE == 'development' && 
		(empty($_SERVER['HTTP_X_REQUESTED_WITH']) 
			|| (empty($_SERVER['HTTP_X_REQUESTED_WITH']) 
			&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		)
	) 
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/* End of file config_helper.php */
/* Location ./application/helpers/config_helper.php */