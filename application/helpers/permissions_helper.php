<?php
/**
 * Permissions Helper
 *
 * @package		Chronos
 * @author		Shaun Wall
 * @link		http://www.rpff.co.uk
 *
 *	Checks the logged in user's permissions against a provided argument of account types
 *	Can optionally redirect the user if they are not permitted, and optionally use either a default message, or provide a custom one
 *
 *	@param	mixed	$permittedTypes		The permission set to check against, can be either an array of account 
 *										types or a string (e.g. 'management')
 *
 *	@param	mixed	$redirect			The URL to redirect the user to if they have insufficient permissions: Can be either TRUE to
 *										redirect to the base / home page, a string for custom, or FALSE / omitted to just return true / false
 *
 *	@param	mixed	$message			The message to be provided to the user in the event they fail and are redirected
 *										Can be either TRUE to use the default, FALSE / omitted to not provide a message, or string for custom
*/
function checkPermissions($permittedTypes, $redirect = false, $message = false)
{	
	$CI =& get_instance();
	
	$current_user = $CI->session->userdata('account_type');
	
	if(is_array($permittedTypes))
	{
		if(in_array($current_user, $permittedTypes))
			$permitted = true;
		else
			$permitted = false;
	}
	elseif(is_string($permittedTypes))
	{
		switch(strtolower($permittedTypes))
		{
			case('admin'):
				if($current_user == 1)
					$permitted = true;
				else
					$permitted = false;
			break;
		
			case('manager'):
				if($current_user == 2)
					$permitted = true;
				else
					$permitted = false;
			break;
					
			case('employee'):
				if($current_user == 3)
					$permitted = true;
				else
					$permitted = false;					
			break;
			
			case('management'):
				if($current_user == 1 || $current_user == 2)
					$permitted = true;
				else
					$permitted = false;
			break;
			
			case('all'):
				if($current_user >= 1)
					$permitted = true;
				else
					$permitted = false;				
			break;
			
			case('banned'):
				if($current_user == 0)
					$permitted = true;
				else
					$permitted = false;				
			break;
		}
	}
	
	if($permitted)
	{
		return TRUE;
	}
	else
	{
		if($redirect)
		{
			if($redirect === TRUE)
				$redirect = '';
				
			if($message == TRUE)
				$message = "You do not have the correct permissions to do that.";
			
			if($message)
				$CI->session->set_flashdata(array('alert' => true, 'type' => 'danger', 'message' => '<span class="glyphicon glyphicon-left glyphicon-exclamation-sign"></span> <strong>Hold on!</strong> ' . $message));
			
			redirect($redirect);
		}
		else
		{
			return FALSE;
		}
	}
}


function canManageUser($user_id)
{
	$CI =& get_instance();
	$CI->load->model('user_model');
	
	$current_user = $CI->session->userdata('id');
	
	return $CI->user_model->canManageUser($current_user, $user_id);
}


function canManageDepartment($department_id)
{
	$CI =& get_instance();
	$CI->load->model('department_model');
	
	$current_user = $CI->session->userdata('id');
	
	return $CI->department_model->canManageDepartment($current_user, $department_id);
}

/* End of file permissions_helper.php */
/* Location ./application/helpers/permissions_helper.php */