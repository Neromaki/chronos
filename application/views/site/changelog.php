<div class="page-header">
	<img src="<?php echo base_url('img/icons/chronos.png') ?>" class="view-project-icon">
	<h1>Chronos <small>Changelog</small></h1>
</div>


<div class="panel panel-primary">
	<div class="panel-heading">v0.4</div>
	<table class="table">
		<tr>
			<td><strong>Project / deptartment / user data update</strong></td>
			<td>
				Modified the data obtained and displayed for projects, departments and users to emphasise recent activity
			</td>
		</tr>
		<tr>
			<td><strong>Performances enchancements</strong></td>
			<td>
				Optimised some larger queries and data processing to make Chronos run faster
			</td>
		</tr>
	</table>
</div>

<div class="panel panel-default">
	<div class="panel-heading">v0.3</div>
	<table class="table">
		<tr>
			<td><strong>Enhanced time logging</strong></td>
			<td>
				Implemented better ways to log time: either specific dates, or a configurable date range with optionals days of the week
			</td>
		</tr>
		<tr>
			<td><strong>Enhanced user management</strong></td>
			<td>
				When adding / updating / removing users, more options for handling their related data
			</td>
		</tr>
		<tr>
			<td><strong>Enhanced department management</strong></td>
			<td>
				When adding / updating / removing departments, more options for handling their related data
			</td>
		</tr>
	</table>
</div>

<div class="panel panel-default">
	<div class="panel-heading">v0.2</div>
	<table class="table">
		<tr>
			<td><strong>Time management</strong></td>
			<td>
				All users can now manage their logged time
			</td>
		</tr>
		<tr>
			<td><strong>Cost management</strong></td>
			<td>
				All users can now manage their logged costs
			</td>
		</tr>
		<tr>
			<td><strong>Event logging</strong></td>
			<td>
				Major actions performed on Chronos are now logged, and can be viewed by management
			</td>
		</tr>
	</table>
</div>

<div class="panel panel-default">
	<div class="panel-heading">v0.1</div>
	<table class="table">
		<tr>
			<td><strong>Project management</strong></td>
			<td>
				Implemented functionality to manage projects and view basic statistical data
			</td>
		</tr>
		<tr>
			<td><strong>Department management</strong></td>
			<td>
				Implemented functionality to manage departments and view basic statistical data
			</td>
		</tr>
		<tr>
			<td><strong>User management</strong></td>
			<td>
				Implemented functionality to manage users and view basic statistical data
			</td>
		</tr>
	</table>
</div>