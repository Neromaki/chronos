<div class="page-header">
	<h1>Unity settings</h1>
</div>
	<?php
	if(validation_errors())
	{	?>
		<div class="alert alert-danger">
			<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
			<?php echo validation_errors() ?>
		</div>
		<?php
	}	?>

<form class="form-horizontal" role="form" method="post">


	<h2><span class="glyphicon glyphicon-left glyphicon-tasks"></span> Tasks</h2>
	
	<div class="form-group">
		<label for="showTaskAge" class="col-sm-2 control-label">Show task age</label>
		<div class="col-sm-5">
			<div class="checkbox">
				<input type="checkbox" name="showTaskAge" value="1" <?php if(isset($appSettings->showTaskAge) && $appSettings->showTaskAge) { echo "checked"; } ?>>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label for="tasksPerPage" class="col-sm-2 control-label">No. tasks per page</label>
		<div class="col-sm-7 col-sm-1">
			<select name="tasksPerPage" class="form-control">
				<option value="5" <?php if($appSettings->tasksPerPage == 5) { echo "selected"; } ?>>5</option>
				<option value="10" <?php if($appSettings->tasksPerPage == 10) { echo "selected"; } ?>>10</option>
				<option value="15" <?php if($appSettings->tasksPerPage == 15) { echo "selected"; } ?>>15</option>
				<option value="20" <?php if($appSettings->tasksPerPage == 20) { echo "selected"; } ?>>20</option>
				<option value="30" <?php if($appSettings->tasksPerPage == 30) { echo "selected"; } ?>>30</option>
				<option value="50" <?php if($appSettings->tasksPerPage == 50) { echo "selected"; } ?>>50</option>
			</select>
		</div>
	</div>
	
	<hr>
	
	<h2><span class="glyphicon glyphicon-left glyphicon-folder-open"></span> Projects</h2>
	
	<div class="form-group">
		<label for="projectsPerPage" class="col-sm-2 control-label">No. projects per page</label>
		<div class="col-sm-7 col-sm-1">
			<select name="projectsPerPage" class="form-control">
				<option value="5" <?php if($appSettings->projectsPerPage == 5) { echo "selected"; } ?>>5</option>
				<option value="10" <?php if($appSettings->projectsPerPage == 10) { echo "selected"; } ?>>10</option>
				<option value="15" <?php if($appSettings->projectsPerPage == 15) { echo "selected"; } ?>>15</option>
				<option value="20" <?php if($appSettings->projectsPerPage == 20) { echo "selected"; } ?>>20</option>
				<option value="30" <?php if($appSettings->projectsPerPage == 30) { echo "selected"; } ?>>30</option>
				<option value="50" <?php if($appSettings->projectsPerPage == 50) { echo "selected"; } ?>>50</option>
			</select>
		</div>
	</div>
	
	
	<div class="form-group">
		<div class="col-sm-7 col-md-2 col-lg-2 col-sm-offset-1">
			<button class="btn btn-primary btn-block" type="submit" name="changeSettings" value="1"><span class="glyphicon glyphicon-save"></span> Save changes</button>
		</div>
	</div>
	
</form>