<div class="page-header">
	<h1><span class="glyphicon glyphicon-info-sign"></span> About Chronos</h1>
	<small>(current version: <a href="https://bitbucket.org/Neromaki/chronos/commits/all"><?php echo VERSION ?></a>)</small>
</div>

<p class="lead">Chronos is a light-weight time manager designed to be as easy, clean and efficient as possible to make time management a hassle-free breeze.</p>

<p>It uses the <a href="http://ellislab.com/codeigniter">CodeIgniter PHP framework</a> and <a href="http://getbootstrap.com/">Bootstrap 3 CSS framework</a> 
<em>(using icon-sheets supplied by <a href="http://glyphicons.com/">Glyphicons</a>)</em> to enable it to run smoothly on almost every device, so you can 
manage your work load from your computer, tablet or phone.</p>