<div class="page-header">
	<h1><span class="glyphicon glyphicon-map-marker"></span> Add a track</h1>
</div>

<?php
if(isset($courierId))
{	
	if($trackingStatus)
	{
		$year = date('Y', $trackingStatus[0]->time);
		$month = date('m', $trackingStatus[0]->time);
		$day = date('d', $trackingStatus[0]->time);
		$hour = date('H', $trackingStatus[0]->time);
		$minute = date('i', $trackingStatus[0]->time);
		
		$location = $trackingStatus[0]->location;
	}
	else
		$location = "";
	?>
	<a href="<?php echo base_url('admin/add_track') ?>" class="btn btn-sm btn-default back"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
	
	<div class="panel panel-primary table-shadow">
		<div class="panel-heading">
			<h3 class="panel-title">Add new track</h3>
		</div>
		<div class="panel-body">
			<?php
			if(validation_errors())
			{	?>
				<div class="alert alert-danger">
					<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong><?php echo validation_errors(); ?>
				</div>
				<?php
			}	?>
			<form class="form-horizontal" role="form" method="post">
				<div class="form-group">
					<label for="location" class="col-sm-2 control-label">Location</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="location" name="location" placeholder="e.g. Antarctica" value="<?php echo $location ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="hour" class="col-sm-2 control-label">Date</label>
					<div class="col-sm-2">
						<select name="day" class="form-control">
							<?php
							for($x=1;$x<=30;$x++)
							{
								if($day == $x) { $s = "selected"; } else { $s = ""; }
								echo "<option value='$x' $s>$x</option>";
							}	?>
						</select>
					</div>
					<div class="col-sm-2">
						<select name="month" class="form-control">
							<?php
							for($x=1;$x<=12;$x++)
							{
								if($month == $x) { $s = "selected"; } else { $s = ""; }
								echo "<option value='$x' $s>$x</option>";
							}	?>
						</select>
					</div>
					<div class="col-sm-2">
						<select name="year" class="form-control">
							<option value="2013">2013</option>
							<option value="2014">2014</option>
						</select>
					</div>
				</div>
					
				<div class="form-group">
					<label for="hour" class="col-sm-2 control-label">Time</label>
					<div class="col-sm-2">
						<select name="hour" class="form-control">
							<?php
							for($x=0;$x<=23;$x++)
							{
								if($hour == $x) { $s = "selected"; } else { $s = ""; }
								echo "<option value='$x' $s>$x</option>";
							}	?>
						</select>
					</div>
					<div class="col-sm-2">
						<select name="minute" class="form-control">
							<?php
							for($x=0;$x<60;$x = $x+5)
							{
								if($minute == $x) { $s = "selected"; } else { $s = ""; }
								echo "<option value='$x' $s>$x</option>";
							}	?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="location" class="col-sm-2 control-label">Notes</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="notes" name="notes" placeholder="e.g. Fended off an angry Security Seal">
					</div>
				</div>
				<div class="form-group">
					<label for="colour" class="col-sm-2 control-label">Colour</label>
					<div class="col-sm-4">
						<select name="colour" class="form-control">
							<option value="">White</option>
							<option value="active">Grey</option>
							<option value="success">Green</option>
							<option value="warning">Yellow</option>
							<option value="danger">Red</option>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-3 col-sm-offset-2">
						<button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-plus"></span> Add to track</button>
					</div>
				</div>
				
			</form>
			
		</div>
	</div>
	
	<div class="panel panel-primary table-shadow">
		<div class="panel-heading">
			<h3 class="panel-title">Current track</h3>
		</div>
		
		<?php
		if($trackingStatus)
		{	?>
			<div class="table-responsive">
				<table class="table">
					<thead>
						<th>Location</th>
						<th>Date</th>
						<th>Time</th>
						<th>Notes</th>
					</thead>
					<tbody>
						<?php
						$x = 1;
						foreach($trackingStatus as $ts)
						{	
							if($x == count($trackingStatus))
								$status = 'active';
							else
								$status = $ts->status;
							?>
								<tr class="<?php echo $status ?>">
								<td><?php echo $ts->location ?></td>
								<td><?php echo date('D jS M y', $ts->time) . " (" . date('d/m/Y', $ts->time) . ")" ?></td>
								<td><?php echo date('g:i a', $ts->time) ?></td>
								<td><?php echo $ts->notes ?></td>
							</tr>
							<?php
							$x++;
						}?>
					</tbody>
				</table>
			</div>
			<?php
		}
		else
		{	?>
			<div class="alert alert-info">
				No tracking updates yet
			</div>
			<?php
		}	?>
	</div>
	<?php
}
else
{	?>
	<a href="<?php echo base_url('admin') ?>" class="btn btn-sm btn-default back"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
	
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<th>Courier</th>
			<th>Total logs</th>
			<th colspan="3">Most recent log</th>
			<th></th>
		</thead>
		
		<tbody>
			<?php
			foreach($courierTracks as $ct)
			{	?>
				<tr>
					<td><?php echo $ct->courierName ?></td>
					<td><?php echo $ct->total ?></td>
					<td><?php if($ct->location) { echo $ct->location; } else { echo "-"; } ?></td>
					<td><?php if($ct->time) { echo date('H:i (d/m/y)', $ct->time); } else { echo "-"; } ?></td>
					<td><?php if($ct->notes) { echo $ct->notes; } else { echo "-"; } ?></td>
					<td><a href="<?php echo base_url('admin/add_track/' . $ct->courierId) ?>" class="btn btn-sm btn-primary">Manage ></a></td>
				</tr>
				<?php
			}	?>
		</tbody>
	</table>
	<?php
}	?>