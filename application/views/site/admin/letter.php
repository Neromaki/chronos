<?php
if(isset($data))
{	?>
	<!-- Letter content -->
	<!DOCTYPE html>
	<html>
		<head>
		<style>	
		body {
			width: 1050px;
			font-family: 'Trebuchet MS', sans-serif; /* Website header font: [Georgia, 'Times New Roman', Times, serif] */
			font-size: 21px;
		}
		
		ul {
			list-style: none;
			padding: 0;
		}
		
		.header {
		}
		
			.header > img {
				width: 130px;
				position: absolute;
				left: 795px;
				top: 40px;
			}
			
		#qr {
			position: absolute;
			top: 40px;
			/*left: 420px;*/
			left: 80px;
		}
		
			#qr > img {
				width: 125px;
			}
			
			#qr > p {
				letter-spacing: 10px;
				position: relative;
				top: -30px;
				left: 5px;
				font-size: 20px;
			}
		
		#recipient {
			position: absolute;
			top: 245px;
			left: 84px;
			font-style: normal;
			#line-height: 32px;
		}
			
		#sender {
			text-align: right;
			position: absolute;
			left: 740px;
			top: 240px;
		}
		
		.date {
			position: absolute;
			left: 780px;
			top: 320px;
			
			margin-top: 10px;
			font-size: 16px;
			font-weight: 800;
		}
		
		.spacing-bottom {
			margin-bottom: 30px;
		}
		
		.main {
			padding: 450px 120px 0 80px;	# Top Left Bottom Right
		}
		
		.code {
			font-weight: 800;
			font-size: 21px;
		}
		
		.sign-off {
			margin-top: 80px;
		}
		
		.footer {
			position: absolute;
			top: 1305px;
		}
		</style>
		</head>
		<body>
			<!-- Header -->
			<div class="header">
				<img src="<?php echo base_url('img/logo/logo_text.png') ?>" alt="logo">
			</div>
			
			<div id="qr">
				<img src="<?php echo base_url('img/qr/' . $data->trackingCode . '.png') ?>" alt="qr">
				<p><?php echo $data->trackingCode ?></p>
			</div>
			
			<!-- Main content -->
			<div class="main">
				<address id="recipient">
					<strong><?php echo $data->firstName . " " . $data->lastName ?></strong><br>
					<?php echo $data->address1 ?><br>
					<?php echo $data->city ?><br>
					<?php echo $data->country ?><br>
					<?php echo $data->postCode ?>
				</address>
				
				<address id="sender">
					<strong>Penguin Deliveries</strong><br>
					South Pole<br>
					Antarctica
				</address>
			
				<p class="date">5th December 2013</p>
				
				<p>Hello there <?php echo $data->firstName ?>!</p>
				<p>We hope you're enjoying the festive season!</p>
				<p>By the time this letter reaches you, one of our wonderful penguin couriers - specifically <strong><?php echo $data->courierName ?></strong> - will be well into their journey to deliver a consignment of festive wonder to you.</p>
				<p>You can track your delivery online by popping over to <strong>www.penguindeliveries.co.uk</strong> and typing in your tracking code of <strong><?php echo $data->trackingCode ?></strong>.<br>
				Alternatively, if you have a smartphone with a QR-code app, you can scan the code at the top of this letter to go directly to your tracking page.</p>
				<p>Whilst there is no cost to you for this special delivery, we would ask that you to leave a fish or two outside on the <strong>20th December</strong> when our courier is due to arrive, so he can have a snack, a breather and get some energy up for the return journey.</p>
				<p>We hope you enjoy your delivery, and have a great Christmas!
				
				<p class="sign-off">Have a good one!<br>
				<strong>Penguin Deliveries</strong></p>
				<ul>
					<li><strong>W</strong>: www.penguindeliveries.co.uk</li>
					<li><strong>E</strong>: howdy@penguindeliveries.co.uk</li>
				</ul>
			</div>
			
			<!-- Footer -->
			<div class="footer">
				<img src="<?php echo base_url('img/logo/penguin_blank.png') ?>" alt="logo">
			</div>
			<div>
		<body>
	</html>
	<?php
}
else
{	?>
	<div class="page-header">
		<h1><span class="glyphicon glyphicon-file"></span> Generate a letter</h1>
	</div>

	<a href="<?php echo base_url('admin') ?>" class="btn btn-sm btn-default back"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<th>Code</th>
			<th>Name</th>
			<th></th>
		</thead>
		
		<tbody>
			<?php
			foreach($trackingData as $td)
			{	?>
				<tr>
					<th><?php echo $td->trackingCode ?></th>
					<td><?php echo $td->recipient ?></td>
					<td><a href="<?php echo base_url('letter/' . $td->trackingCode) ?>" class="btn btn-sm btn-primary">Generate letter ></a></td>
				</tr>
				<?php
			}	?>
		</tbody>
	</table>
	<?php
}	?>