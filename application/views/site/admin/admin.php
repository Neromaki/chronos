<div class="page-header">
	<h1><span class="glyphicon glyphicon-wrench"></span> Admin</h1>
</div>
<p class="lead">
	If you just typed '/admin' into the URL hoping to find an administration section, well done because here it is.
	<br>However, Mr / Ms Clever-clogs: <strong>you totally shouldn't be here.</strong> 
	<br>Forget you saw anything, and navigate away before I dispatch some Security Seals your way, buddy!
</p>

<hr>

<h3>Tools: </h3>
<br><a href="<?php echo base_url('admin/add_track') ?>" class="btn btn-primary spacing-bottom-10"><span class="glyphicon glyphicon-map-marker"></span> Add a track</a>
<br><a href="<?php echo base_url('admin/add_item') ?>" class="btn btn-primary spacing-bottom-10"><span class="glyphicon glyphicon-gift"></span> Add an item</a>
<br><a href="<?php echo base_url('admin/letter') ?>" class="btn btn-primary spacing-bottom-10"><span class="glyphicon glyphicon-file"></span> Generate a letter</a>