<div class="page-header">
	<h1><span class="glyphicon glyphicon-gift"></span> Add an item</h1>
</div>

<a href="<?php echo base_url('admin') ?>" class="btn btn-sm btn-default back"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>

<div class="panel panel-primary table-shadow">
	<div class="panel-heading">
		<h3 class="panel-title">Add new item</h3>
	</div>
	<div class="panel-body">
		<?php
		if(validation_errors())
		{	?>
			<div class="alert alert-danger">
				<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong><?php echo validation_errors(); ?>
			</div>
			<?php
		}	?>
		<form class="form-horizontal" role="form" method="post">
		
			<div class="form-group">
				<label for="hour" class="col-sm-2 control-label">Recipient</label>
				<div class="col-sm-2">
					<select name="recipient" class="form-control">
						<?php
						foreach($totalItems as $t)
						{	?>
							<option value="<?php echo $t->trackingCode ?>"  <?php echo set_select('recipient', $t->trackingCode) ?>><?php echo $t->recipient ?></option>
							<?php
						}	?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="hour" class="col-sm-2 control-label">Size</label>
				<div class="col-sm-2">
					<select name="size" class="form-control">
						<option value="Small" <?php echo set_select('size', 'Small') ?>>Small</option>
						<option value="Medium" <?php echo set_select('size', 'Medium') ?>>Medium</option>
						<option value="Large" <?php echo set_select('size', 'Large') ?>>Large</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="location" class="col-sm-2 control-label">Dimensions (cm)</label>
				<div class="col-sm-1">
					<input type="text" class="form-control" id="height" name="height" placeholder="H" value="<?php echo set_value('height') ?>">
				</div>
				<div class="col-sm-1">
					<input type="text" class="form-control" id="width" name="width" placeholder="W" value="<?php echo set_value('width') ?>">
				</div>
				<div class="col-sm-1">
					<input type="text" class="form-control" id="depth" name="depth" placeholder="D" value="<?php echo set_value('depth') ?>">
				</div>
			</div>
			
			<div class="form-group">
				<label for="location" class="col-sm-2 control-label">Weight (kg)</label>
				<div class="col-sm-2">
					<input type="text" class="form-control" id="weight" name="weight" placeholder="e.g. 0.53" value="<?php echo set_value('weight') ?>">
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-3 col-sm-offset-2">
					<button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-plus"></span> Add item</button>
				</div>
			</div>
			
		</form>
		
	</div>
</div>


<div class="panel panel-primary table-shadow">
	<div class="panel-heading">
		<h3 class="panel-title">Summary</h3>
	</div>
	<table class="table table-striped table-hover">
		<thead>
			<th>Name</th>
			<th>Total items</th>
		</thead>
		
		<tbody>
			<?php
			foreach($totalItems as $t)
			{	?>
				<tr>
					<td><?php echo $t->recipient ?></td>
					<td><?php echo $t->total ?></td>
				</tr>
				<?php
			}	?>
		</tbody>
	</table>
</div>


<div class="panel panel-primary table-shadow">
	<div class="panel-heading">
		<h3 class="panel-title">Detailed</h3>
	</div>
	<table class="table table-striped table-hover">
		<thead>
			<th>Name</th>
			<th>Item</th>
		</thead>
		
		<tbody>
			<?php
			foreach($allItems as $t)
			{	?>
				<tr>
					<td><?php echo $t->recipient ?></td>
					<td><?php echo $t->itemDesc ?></td>
				</tr>
				<?php
			}	?>
		</tbody>
	</table>
</div>