<!DOCTYPE html>
<html style="height: 100%;">
	<head>
		<title>Chronos Time Manager</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="icon" href="<?php echo base_url('img/favicon.ico?v=2') ?>" type="image/x-icon" /> 

		<link rel="shortcut icon" href="<?php echo base_url('img/favicon.ico'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/min/bootstrap.min.css'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/style.css'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/login.css'); ?>" />
		<link href='http://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>	
	</head>	
	
	<body style="padding:0; height:100%;">
		<div class="container" style="height:100%">
			<div style="max-width:420px; padding:40px; margin:0 auto; height:100%; background:#fff;">
				<form class="form-signin" role="form" method="post">
					<img class="img-responsive login-logo" src="<?php echo base_url('img/logo/chronos.png') ?>">
					<?php 					
					if($this->session->flashdata('alert'))
					{ ?>
						<div class='alert alert-<?php echo $this->session->flashdata('type'); ?> spacing-top-5'>
							<?php echo $this->session->flashdata('message'); ?>
						</div>
						<?php
					}	?>
					
					<?php 
						if(set_value('username'))
							$username = set_value('username');
						if($this->session->flashdata('login_username'))
							$username = $this->session->flashdata('login_username');
						else
							$username = NULL;
					?>
					<div style="text-align:center">
						<h1>Chronos <br><small>time manager</small></h1>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<input type="text" name="username" class="form-control text-center" placeholder="Username" value="<?php echo $username ?>" required>
						<input type="password" name="password" class="form-control text-center" placeholder="Password" required>
						<!--
						<label class="checkbox">
							<input type="checkbox" value="remember-me"> Remember me
						</label>
						-->
						<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>