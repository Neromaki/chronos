    <div class="home">

		<h1 style="text-align:center;margin-bottom:30px">Quick actions</h1>
		
		
		<div class="row">
		
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-md-offset-2 col-lg-offset-2 action-wrapper">
				<a href="<?php echo base_url('time/add')?>" title="Add time" class="btn btn-default btn-block action">
					<span class="glyphicon glyphicon-time"></span>
					<h2>Add time</h2>
				</a>
			</div>
			
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 action-wrapper">
				<a href="<?php echo base_url('costs/add')?>" title="Add cost" class="btn btn-default btn-block action">
					<span class="glyphicon glyphicon-gbp"></span>
					<h2>Add cost</h2>
				</a>
			</div>
			
		</div>
	</div>