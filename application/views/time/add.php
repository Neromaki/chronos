<div class="page-header">
	<h1>Add Time</h1>

	<a class="btn btn-default" href="<?php echo base_url('time') ?>"><span class="glyphicon glyphicon-chevron-left"></span></a>	

</div>

<?php 				
if(validation_errors())
{	?>
	<div class="alert alert-danger">
		<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
		<?php echo validation_errors() ?>
	</div>
	<?php
}	?>

<div class="add-time page-body">
		<?php		
		$time_data = $this->session->userdata('time_data');
		
		if(set_value('date1')) { $date1 = set_value('date1'); } elseif(isset($time_data[1]['date'])) { $date1 = $time_data[1]['date']; } else { $date1 = ""; }
		if(set_value('project1')) { $project1 = set_value('project1'); } elseif(isset($time_data[1]['project'])) { $project1 = $time_data[1]['project']; } else { $project1 = ""; }
		if(set_value('duration1')) { $duration1 = set_value('duration1'); } elseif(isset($time_data[1]['duration'])) { $duration1 = $time_data[1]['duration']; } else { $duration1 = ""; }
		?>
		<h2>By day</h2>
		<form role="form" action="<?php echo base_url('time/add')?>" method="post" data-type="time">
			<div id="form-inner">
			
				<div class="row spacing-bottom-15">
					<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 <?php if(form_error('date1')) { echo 'has-error'; } ?>">
						<input type="text" class="form-control date-pick" id="date1" name="date1" placeholder="Date" data-date-format="DD/MM/YYYY" value="<?php echo $date1 ?>" required />
					</div>
				
					<div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 <?php if(form_error('project1')) { echo 'has-error'; } ?>">
						<input type="text" class="form-control project-pick" id="project1" name="project1" placeholder="Project" value="<?php echo $project1 ?>" required />
					</div>

					<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 <?php if(form_error('duration1')) { echo 'has-error'; } ?>">
						<div class="input-group">
							<input type="text" class="form-control" id="duration1" name="duration1" placeholder="e.g. 2.75" max="24" value="<?php echo $duration1 ?>" required />
							<span class="input-group-addon">hrs</span>
						</div>
					</div>
					
					<div class="col-sm-1 col-md-1 col-lg-1 hidden-xs">
						<button type="button" id="addTimeRow" class="btn btn-default btn-sm addRow"  data-toggle="tooltip" data-placement="right" title="Add new row"><span class="glyphicon glyphicon-plus"></span></button>
					</div>
				</div>
			
				<?php				
				for($x=2; isset($time_data[$x]['date']); $x++)
				{	
					if(set_value('date'.$x)) { $date = set_value('date'.$x); } elseif(isset($time_data[$x]['date'])) { $date = $time_data[$x]['date']; } else { $date = ""; }
					if(set_value('project'.$x)) { $project = set_value('project'.$x); } elseif(isset($time_data[$x]['project'])) { $project = $time_data[$x]['project']; } else { $project = ""; }
					if(set_value('duration'.$x)) { $duration = set_value('duration'.$x); } elseif(isset($time_data[$x]['duration'])) { $duration = $time_data[$x]['duration']; } else { $duration = ""; }
					?>
					<div class="row time-row spacing-bottom-15">
						<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 <?php if(form_error('date'.$x)) { echo 'has-error'; } ?>">
							<input type="text" class="form-control date-pick" id="date<?php echo $x ?>" name="date<?php echo $x ?>" placeholder="Date" data-date-format="DD/MM/YYYY" value="<?php echo $date ?>" required />
						</div>
					
						<div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 <?php if(form_error('project'.$x)) { echo 'has-error'; } ?>">
							<input type="text" class="form-control project-pick" id="project<?php echo $x ?>" name="project<?php echo $x ?>" placeholder="Project" value="<?php echo $project ?>" required />
						</div>

						<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 <?php if(form_error('duration'.$x)) { echo 'has-error'; } ?>">
							<div class="input-group">
								<input type="text" class="form-control" id="duration<?php echo $x ?>" name="duration<?php echo $x ?>" placeholder="Time" max="24" value="<?php echo $duration ?>" required />
								<span class="input-group-addon">hrs</span>
							</div>
						</div>
						
						<div class="col-sm-1 col-md-1 col-lg-1 hidden-xs">
							<button type="button" class="btn btn-default btn-sm removeRow"><span class="glyphicon glyphicon-minus"></span></button>
						</div>
					</div>
					<?php
				}	?>
				
			</div>

			<div class="row visible-xs row-controls-xs">
				
				<div class="col-xs-6">
					<button type="button" class="btn btn-info btn-sm btn-block addRow">
						<span class="glyphicon glyphicon-left glyphicon-plus"></span> Add row
					</button>
				</div>
				
				<div class="col-xs-6">
					<button type="button" class="btn btn-info btn-sm btn-block removeRowMobile">
						<span class="glyphicon glyphicon-left glyphicon-minus"></span> Remove row
					</button>
				</div>
				
			</div>
			
			<div class="row add-time-button">
				
				<div class="col-xs-8 col-sm-3 col-md-2 col-lg-2 col-xs-offset-2 col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
					<button type="submit" class="btn btn-primary btn-sm btn-block" name="add_day_time" value="true">
						<span class="glyphicon glyphicon-left glyphicon-plus"></span> Add time
					</button>
				</div>
				
			</div>
				
		</form>
		
		
		<hr>
		
		<?php
		if(set_value('period_start')) { $period_start = set_value('period_start'); } elseif(isset($time_data['period_start'])) { $period_start = $time_data['period_start']; } else { $period_start = ""; }
		if(set_value('period_end')) { $period_end = set_value('period_end'); } elseif(isset($time_data['period_end'])) { $period_end = $time_data['period_end']; } else { $period_end = ""; }
		if(set_value('period_project')) { $period_project = set_value('period_project'); } elseif(isset($time_data['period_project'])) { $period_project = $time_data['period_project']; } else { $period_project = ""; }
		if(set_value('period_duration')) { $period_duration = set_value('period_duration'); } elseif(isset($time_data['period_duration'])) { $period_duration = $time_data['period_duration']; } else { $period_duration = ""; }
		?>
		<h2 id="by_period" data-toggle="tooltip" data-placement="right" title="Select the days of the week you wish to add time for over a period (e.g. instead of adding six days seperately above, you can do 'every Monday, Tuesday and Wednesday between these two dates you worked eight hours a day')">By period <i class="glyphicon glyphicon-info-sign"></i></h2>
		<form role="form" action="<?php echo base_url('time/add')?>" method="post">
			<div id="form-inner">
			
				<div class="row spacing-bottom-15">
					<div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 <?php if(form_error('period_start')) { echo 'has-error'; } ?>">
						<input type="text" class="form-control date-pick" id="period_start" name="period_start" placeholder="Start" data-date-format="DD/MM/YYYY" value="<?php echo $period_start ?>" required />
					</div>
					
					<div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 <?php if(form_error('period_end')) { echo 'has-error'; } ?>">
						<input type="text" class="form-control date-pick" id="period_end" name="period_end" placeholder="End" data-date-format="DD/MM/YYYY" value="<?php echo $period_end ?>" required />
					</div>
				
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 <?php if(form_error('period_project')) { echo 'has-error'; } ?>">
						<input type="text" class="form-control project-pick" id="period_project" name="period_project" placeholder="Project" value="<?php echo $period_project ?>" required />
					</div>
					
					<div class="col-xs-8 col-sm-4 col-md-3 col-lg-3 col-xs-offset-2 col-sm-offset-0 <?php if(form_error('period_duration')) { echo 'has-error'; } ?>">
						<div class="input-group">
							<input type="text" class="form-control" id="period_duration" name="period_duration" placeholder="e.g. 1.5" value="<?php echo $period_duration ?>" required />
							<span class="input-group-addon">hours per day</span>
						</div>
					</div>
					
					<div class="clearfix hidden-sm"></div>
					
					<!--
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 <?php if(form_error('period_duration')) { echo 'has-error'; } ?>">
						<label class="checkbox">
							<input type="checkbox" id="period_saturdays" name="period_saturdays" value="1" <?php if(isset($time_data['period_saturdays']) && $time_data['period_saturdays'] == 1) { echo 'checked'; } ?>> Include Saturdays
						</label>
					</div>
					
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 <?php if(form_error('period_duration')) { echo 'has-error'; } ?>">
						<label class="checkbox">
							<input type="checkbox" id="period_sundays" name="period_sundays" value="1" <?php if(isset($time_data['period_sundays']) && $time_data['period_sundays'] == 1) { echo 'checked'; } ?>> Include Sundays
						</label>
					</div>
					-->
					<div class="clearfix"></div>

					<div class="col-xs-12"><p id="days_label">Days of the week</p></div>
					
					<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
						<label class="checkbox">
							<input type="checkbox" id="period_mondays" name="period_mondays" value="1" <?php if((isset($time_data['period_mondays']) && $time_data['period_mondays'] == 1) || !isset($time_data['period_mondays'])) { echo 'checked'; } ?>> Monday
						</label>
					</div>
					<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
						<label class="checkbox">
							<input type="checkbox" id="period_tuesdays" name="period_tuesdays" value="1" <?php if((isset($time_data['period_tuesdays']) && $time_data['period_tuesdays'] == 1) || !isset($time_data['period_tuesdays'])) { echo 'checked'; } ?>> Tuesday
						</label>
					</div>
					<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
						<label class="checkbox">
							<input type="checkbox" id="period_wednesdays" name="period_wednesdays" value="1" <?php if((isset($time_data['period_wednesdays']) && $time_data['period_wednesdays'] == 1) || !isset($time_data['period_wednesdays'])) { echo 'checked'; } ?>> Wednesday
						</label>
					</div>
					<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
						<label class="checkbox">
							<input type="checkbox" id="period_thursdays" name="period_thursdays" value="1" <?php if((isset($time_data['period_thursdays']) && $time_data['period_thursdays'] == 1) || !isset($time_data['period_thursdays'])) { echo 'checked'; } ?>> Thursday
						</label>
					</div>
					<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
						<label class="checkbox">
							<input type="checkbox" id="period_fridays" name="period_fridays" value="1" <?php if((isset($time_data['period_fridays']) && $time_data['period_fridays'] == 1) || !isset($time_data['period_fridays'])) { echo 'checked'; } ?>> Friday
						</label>
					</div>
					<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
						<label class="checkbox">
							<input type="checkbox" id="period_saturdays" name="period_saturdays" value="1" <?php if(isset($time_data['period_saturdays']) && $time_data['period_saturdays'] == 1) { echo 'checked'; } ?>> Saturday
						</label>
					</div>
					<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
						<label class="checkbox">
							<input type="checkbox" id="period_sundays" name="period_sundays" value="1" <?php if(isset($time_data['period_sundays']) && $time_data['period_sundays'] == 1) { echo 'checked'; } ?>> Sunday
						</label>
					</div>
				</div>
				
			</div>
			
			<div class="row add-time-button">
				
				<div class="col-xs-8 col-sm-3 col-md-2 col-lg-2 col-xs-offset-2 col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
					<button type="submit" class="btn btn-primary btn-sm btn-block" name="add_period_time" value="true">
						<span class="glyphicon glyphicon-left glyphicon-plus"></span> Add time
					</button>
				</div>
				
			</div>
				
		</form>

		
</div>

	<style>
	  .ui-autocomplete {
		max-height: 187px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
	  }

	  #addTimeRow + div.tooltip {
	  	width: 100px;
	  }
	</style>


  <script type="text/javascript">
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})

	 $(function () {
		$('.date-pick').datetimepicker({
			pickTime: false,
			defaultDate: moment() 
		});
	});
  
  
  $(function() {
    var project_names = <?php echo json_encode($project_names) ?>;
    var project_descs = <?php echo json_encode($project_descs) ?>;
	
	setGlobalProjects(project_names);
	setglobalProjectDescs(project_descs);
	
    $( ".project-pick" ).autocomplete({
      source: project_names
    });
  });
  // When the project-pick field is defocused (blurred)
  $(document).on('blur', '.project-pick', function() {
  	// Check if the selected project name exists in the globalProjects objects
  	var project_index = $.inArray($(this).val(), globalProjects.projects);

  	// If it does (project_index will be > 0), check if it has a description set, and display it in a popover if so
  	if(project_index >= 0 && globalProjectDescs[project_index] != null) {
  		$(this).popover({trigger:'manual',title:'Project description',content:globalProjectDescs[project_index],placement:'top'});
  		$(this).popover('show');
  		// After X seconds, destroy the popover
  		window.setTimeout(function() {
  			$('.popover').remove();
  			$(this).popover('destroy');
  		}, 5000);
  	}
  });
  </script>