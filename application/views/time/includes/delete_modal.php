<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<form method="post" action="<?php echo base_url('time/delete') ?>">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modalLabel">Delete this time?</h4>
			</div>
			
			<div class="modal-body">
				Are you sure you wish to delete this time?
				
				<dl class="dl-modal">
				
					<dt>Date</dt>
					<dd id="deleteModalDate"></dd>
				
					<dt>Project</dt>
					<dd id="deleteModalProject"></dd>
				
					<dt>Duration</dt>
					<dd id="deleteModalDuration"></dd>
				</dl>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				
				<button type="submit" class="btn btn-danger" name="time_id" id="deleteTime" value="">
					<span class="glyphicon glyphicon-remove"></span> Delete time
				</button>				
			</div>
		</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$('.delete-time').on('click', function() {
		var btn = $(this);
		btn.button('loading');
		jQuery(this).spin('tiny');
		
		jQuery.ajax({
		  type: "POST",
		  url: "<?php echo base_url() ?>time/functions/getTimeData",
		  data: { time_id: jQuery(this).data('id') }
		})
		  .success(function( data ) {
			data = JSON.parse(data);
			$('#deleteModalDate').html(data.date);
			$('#deleteModalProject').html(data.project_name);
			$('#deleteModalDuration').html(data.duration);
			$('#deleteTime').val(data.time_id);
			jQuery(this).spin(false);
			btn.button('reset');
			$('#deleteModal').modal();
		  });
		 
	});
</script>