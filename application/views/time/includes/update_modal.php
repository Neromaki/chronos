<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<form method="post" action="<?php echo base_url('time/update') ?>">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modalLabel">Update time</h4>
			</div>
			
			<div class="modal-body">
				
				<div class="form-group">
					<label for="updateModalDate">Date</label>
					<input type="text" class="form-control" id="updateModalDate" name="update_date" data-date-format="DD/MM/YYYY" placeholder="Date">
					<span class="glyphicon form-control-feedback"></span>
				</div>
				
				<div class="form-group">
					<label for="updateModalProject">Project</label>
					<input type="text" class="form-control" id="updateModalProject" name="update_project" placeholder="Project">
					<span class="glyphicon form-control-feedback"></span>
				</div>
				
				<div class="form-group has-feedback">
					<label for="updateModalDuration">Duration</label>
					<input type="text" class="form-control" id="updateModalDuration" name="update_duration" placeholder="Duration">
					<span class="glyphicon form-control-feedback"></span>
				</div>
				
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				
				<button type="submit" class="btn btn-primary" name="time_id" id="updateTime" value="">
					<span class="glyphicon glyphicon-save"></span> Update time
				</button>				
			</div>
		</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$('.update-time').on('click', function() {
		var btn = $(this);
		btn.button('loading');
		jQuery(this).spin('tiny'); 
		
		jQuery.ajax({
		  type: "POST",
		  url: "<?php echo base_url() ?>time/functions/getTimeData",
		  data: { time_id: jQuery(this).data('id') }
		})
		  .success(function( data ) {
			data = JSON.parse(data);
			var date = moment(data.date, 'YYYY-MM-DD');
			$('#updateModalDate').val(date.format('DD/MM/YYYY'));
			$('#updateModalProject').val(data.project_name);
			$('#updateModalDuration').val(data.duration);
			$('#updateTime').val(data.time_id);
			
			$('#updateModalDate').datetimepicker({ pickTime:false, defaultDate:moment() });	
			btn.button('reset');
			jQuery(this).spin(false);
			$('#updateModal').modal();
		  });
		 
	});
</script>


<script type="text/javascript">

	jQuery('#updateModalDuration').keyup(function () 
	{
		// Get the input (which is this, but this allows for the function to be generic later)
		var input = jQuery(this);
		var wrapper = jQuery(this).closest('.form-group');
		var icon = jQuery(this).next();
		// Remove any icons
		wrapper.removeClass('has-success has-error has-warning');
		icon.removeClass('glyphicon-ok glyphicon-remove glyphicon-warning');
		
		// Remove any error messages if already there
		jQuery('#duration-error').remove();
		
		
		// Check the contents
		if(jQuery.isNumeric(jQuery(this).val())) {
			// If the username doesn't already exist, highlight green, add a friendly icon, clean up and enable submit button
			wrapper.addClass('has-success');
			icon.addClass('glyphicon-ok');
			jQuery('span.help-block', wrapper).remove();
			jQuery('#updateTime').removeAttr('disabled');	
		} else {
			// If username already exists, highlight red, add icon, ensure button is disabled and output error
			wrapper.addClass('has-error');
			icon.addClass('glyphicon-remove');
			jQuery('#updateTime').attr('disabled','disabled');
			wrapper.append('<span class="help-block" id="duration-error">Duration must be numeric</span>');
		}
  

	});
</script>