<div class="page-header">
	<h1>Users</h1>


	<button type="button" class="btn btn-primary btn-sm js" data-toggle="collapse" data-target="#add_user">
	  <span class="glyphicon glyphicon-left glyphicon-plus"></span> Add <b class="caret"></b>
	</button>

	<button type="button" class="btn btn-info btn-sm js" data-toggle="collapse" data-target="#search_user">
	  <span class="glyphicon glyphicon-left glyphicon-search"></span> Search  <b class="caret"></b>
	</button>
	

	<div id="add_user" class="collapse <?php if(validation_errors()) { echo "in"; } ?>">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php 
				if(validation_errors())
				{	?>
					<div class="alert alert-danger">
						<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
						<?php echo validation_errors() ?>
					</div>
					<?php
				}	?>
				
				<form class="form-horizontal" role="form" method="post">
				
					<div class="form-group has-feedback <?php if(form_error('username')) { echo 'has-error'; } ?>">
						<label for="username" class="col-sm-2 control-label">Username</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="add-username" name="username" placeholder="e.g. 'jjefferson'" value="<?php echo set_value('username') ?>">
							<span class="glyphicon form-control-feedback"></span>
						</div>
					</div>
					
					<div class="form-group has-feedback <?php if(form_error('password')) { echo 'has-error'; } ?>">
						<label for="password" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-4">
							<input type="password" class="form-control" name="password" id="add-password" />
							<span class="glyphicon form-control-feedback"></span>
						</div>
					</div>
					
					<div class="form-group has-feedback <?php if(form_error('name')) { echo 'has-error'; } ?>">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="name" id="add-name" placeholder="e.g. 'Jeff Jefferson'" value="<?php echo set_value('name') ?>">
							<span class="glyphicon form-control-feedback"></span>
						</div>
					</div>
					
					<div class="form-group has-feedback <?php if(form_error('email')) { echo 'has-error'; } ?>">
						<label for="email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-4">
							<input type="email" class="form-control" id="add-email" name="email" placeholder="e.g. 'jjefferson@api-tech.com'" value="<?php echo set_value('email') ?>">
							<span class="glyphicon form-control-feedback"></span>
						</div>
					</div>
					
					<div class="form-group">
						<label for="department" class="col-sm-2 control-label">Department</label>
						<div class="col-sm-4">
							<?php
							if(checkPermissions('admin'))
							{	?>
							<select name="department" class="form-control">
								<?php foreach($departments as $d) { ?><option value="<?php echo $d->dept_id ?>"><?php echo $d->dept_name ?></option><?php } ?>
							</select>
								<?php
							}
							else
							{	?>
								<input type="hidden" name="department" value="<?php echo $this->session->userdata('department') ?>" />
								<input type="text" class="form-control" id="dept" name="dept" value="<?php echo $this->session->userdata('department_name') ?>" disabled >
								<?php
							}	?>
						</div>
					</div>
					
					<div class="form-group">
						<label for="type" class="col-sm-2 control-label">Account Type</label>
						<div class="col-sm-4">
							<select name="type" class="form-control">
								<option value="3">Employee</option>
								<option value="2">Manager</option>
								<?php if(checkPermissions('admin')) { ?><option value="1">Admin</option><?php } ?>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-7 col-md-2 col-lg-2 col-sm-offset-2">
							<button class="btn btn-primary btn-block" name="add" value="1" id="submit-btn" type="submit"><span class="glyphicon glyphicon-plus"></span> Add user</button>
						</div>
					</div>
					
				</form>
				
			</div>
		</div>
	</div>
	
	<div id="search_user" class="collapse <?php if($this->input->post('search')) { echo "in"; } ?>">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php 
				if(validation_errors())
				{	?>
					<div class="alert alert-danger">
						<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
						<?php echo validation_errors() ?>
					</div>
					<?php
				}	?>
				
				<form class="form-horizontal" role="form" method="post">
				
					<div class="form-group <?php if(form_error('search_string')) { echo 'has-error'; } ?>">
						<label for="search_string" class="col-sm-2 control-label">Search</label>
						<div class="col-sm-7 col-md-5">
							<input type="text" class="form-control" name="search_string" placeholder="e.g. 'MWall' or 'Marc'" value="<?php if($search = $this->input->post('search_string')) { echo $search; } else { echo set_value('search_string'); } ?>">
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-7 col-md-2 col-lg-2 col-sm-offset-2">
							<button class="btn btn-info btn-block" name="search" value="1" type="submit"><span class="glyphicon glyphicon-search"></span> Search users</button>
						</div>
					</div>
					
				</form>
				
			</div>
		</div>
	</div>

</div>
	
<?php
if($users)
{
	if($pagination)
		echo $pagination;	?>
		
	<table class="table table-striped table-hover table-condensed">
		<thead>
			<th>User</th>
			<th>Name</th>
			<th>Department</th>
		</thead>
		
		<tbody>
			<?php
			foreach($users as $u)
			{	?>
				<tr>
					<td class="title"><?php echo $u->username ?></td>
					<td><?php echo $u->name ?></td>
					<td><?php echo $u->dept_name ?></td>
					<td><a class="btn btn-default btn-mini" href="<?php echo base_url('users/view/' . $u->user_id) ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></td>
				</tr>
				<?php
			} ?>
		</tbody>
	</table>
	<?php
}
else
{	?>
	<div class="alert alert-warning">
		<strong>Sorry!</strong> No users could be found using those search terms
	</div>
	<?php
}	?>

<script type="text/javascript">
	/*
	*	Does some fun AJAX-based form validation
	*/
	// Disable the submit button by default
	//jQuery('#add-btn').attr('disabled','disabled');	
	
	// When the user focuses away (has selected the field, then either tabs or click away)..
	jQuery('#add-username').focusout(function () 
	{
		// Get the input (which is this, but this allows for the function to be generic later)
		var input = jQuery(this);
		var wrapper = jQuery(this).closest('.form-group');
		var icon = jQuery(this).next();
		// Remove any icons
		wrapper.removeClass('has-success has-error has-warning');
		icon.removeClass('glyphicon-ok glyphicon-remove glyphicon-warning');
		
		// Remove any error messages if already there
		jQuery('#username-error').remove();
		
		// AJAX to a function which queries the database to see if the username already exists
		jQuery.ajax({
		  type: "POST",
		  url: "<?php echo base_url() ?>users/functions/checkUsernameExists",
		  data: { username: jQuery(this).val() }
		})
		  .done(function( result ) 
		  {
			// Once the AJAX returns, check the result..
			if(result === 'false') {
				// If the username doesn't already exist, highlight green, add a friendly icon, clean up and enable submit button
				wrapper.addClass('has-success');
				icon.addClass('glyphicon-ok');
				jQuery('span.help-block', wrapper).remove();
				jQuery('#submit-btn').removeAttr('disabled');	
			} else {
				// If username already exists, highlight red, add icon, ensure button is disabled and output error
				wrapper.addClass('has-error');
				icon.addClass('glyphicon-remove');
				jQuery('#submit-btn').attr('disabled','disabled');
				wrapper.append('<span class="help-block" id="username-error">That username already exists</span>');
			}
		  });
  

	});
	
	// When the user focuses away (has selected the field, then either tabs or click away)..
	jQuery('#add-password').focusout(function () 
	{
		// Get the input (which is this, but this allows for the function to be generic later)
		var input = jQuery(this);
		var wrapper = jQuery(this).closest('.form-group');
		var icon = jQuery(this).next();
		// Remove any icons
		wrapper.removeClass('has-success has-error has-warning');
		icon.removeClass('glyphicon-ok glyphicon-remove glyphicon-warning');
		
		// Remove any error messages if already there
		jQuery('#password-error').remove();
		
		if(jQuery(this).val().length > 3)
		{
			// If the input length passes, highlight green, add a friendly icon, clean up and enable submit button
			wrapper.addClass('has-success');
			icon.addClass('glyphicon-ok');
			jQuery('span.help-block', wrapper).remove();
			jQuery('#submit-btn').removeAttr('disabled');
		} else {
			// If username already exists, highlight red, add icon, ensure button is disabled and output error
			wrapper.addClass('has-error');
			icon.addClass('glyphicon-remove');
			jQuery('#submit-btn').attr('disabled','disabled');
			wrapper.append('<span class="help-block" id="password-error">Password must be at least 4 characters long</span>');
		}
	});
	
	// When the user focuses away (has selected the field, then either tabs or click away)..
	jQuery('#add-name').focusout(function () 
	{
		// Get the input (which is this, but this allows for the function to be generic later)
		var input = jQuery(this);
		var wrapper = jQuery(this).closest('.form-group');
		var icon = jQuery(this).next();
		// Remove any icons
		wrapper.removeClass('has-success has-error has-warning');
		icon.removeClass('glyphicon-ok glyphicon-remove glyphicon-warning');
		
		// Remove any error messages if already there
		jQuery('#name-error').remove();
		
		if(jQuery(this).val().length > 3)
		{
			// If the input length passes, highlight green, add a friendly icon, clean up and enable submit button
			wrapper.addClass('has-success');
			icon.addClass('glyphicon-ok');
			jQuery('span.help-block', wrapper).remove();
			jQuery('#submit-btn').removeAttr('disabled');
		} else {
			// If username already exists, highlight red, add icon, ensure button is disabled and output error
			wrapper.addClass('has-error');
			icon.addClass('glyphicon-remove');
			jQuery('#submit-btn').attr('disabled','disabled');
			wrapper.append('<span class="help-block" id="name-error">You must enter a name</span>');
		}
	});
	
	// When the user focuses away (has selected the field, then either tabs or click away)..
	jQuery('#add-email').focusout(function () 
	{
		// Get the input (which is this, but this allows for the function to be generic later)
		var input = jQuery(this);
		var wrapper = jQuery(this).closest('.form-group');
		var icon = jQuery(this).next();
		// Remove any icons
		wrapper.removeClass('has-success has-error has-warning');
		icon.removeClass('glyphicon-ok glyphicon-remove glyphicon-warning');
		
		// Remove any error messages if already there
		jQuery('#email-error').remove();
		
		var emailRegex = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		
		if(emailRegex.test(jQuery(this).val()) || jQuery(this).val().length == 0)
		{
			// If the input length passes, highlight green, add a friendly icon, clean up and enable submit button
			wrapper.addClass('has-success');
			icon.addClass('glyphicon-ok');
			jQuery('span.help-block', wrapper).remove();
			jQuery('#submit-btn').removeAttr('disabled');
		} else {
			// If username already exists, highlight red, add icon, ensure button is disabled and output error
			wrapper.addClass('has-error');
			icon.addClass('glyphicon-remove');
			jQuery('#submit-btn').attr('disabled','disabled');
			wrapper.append('<span class="help-block" id="email-error">You must either provide a valid email, or none</span>');
		}
	});
	
</script>