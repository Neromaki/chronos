<div class="page-header">
	<h1>Account settings</h1>
</div>

<?php
if(validation_errors())
{	?>
	<div class="alert alert-danger">
		<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
		<?php echo validation_errors() ?>
	</div>
	<?php
}	?>

<div class="row">
	<div class="col-xs-11 col-sm-12 col-md-5 col-lg-5 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
		<h2><span class="glyphicon glyphicon-left glyphicon-user"></span> Account info</h2>

		<form class="form-horizontal" role="form" method="post">
			
			<div class="form-group <?php if(form_error('name')) { echo 'has-error'; } ?>">
				<label for="name" class="col-sm-3 col-md-3 col-lg-3 control-label">Name</label>
				<div class="col-xs-11 col-sm-7 col-md-7 col-lg-7">
					<input type="text" class="form-control" name="name" value="<?php echo $account_info->name; ?>">
				</div>
			</div>
			
			<div class="form-group <?php if(form_error('email')) { echo 'has-error'; } ?>">
				<label for="email" class="col-sm-3 col-md-3 col-lg-3 control-label">Email</label>
				<div class="col-xs-11 col-sm-7 col-md-7 col-lg-7">
					<input type="text" class="form-control" name="email" value="<?php echo $account_info->email; ?>">
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-11 col-sm-4 col-md-5 col-lg-4 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
					<input type="hidden" name="user_id" value="<?php echo $account_info->user_id ?>" />
					<button class="btn btn-primary btn-block" type="submit" name="updateInfo" value="1"><span class="glyphicon glyphicon-user"></span> Update info</button>
				</div>
			</div>
			
		</form>
	</div>

	<hr class="hidden-md hidden-lg">	

	<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-sm-offset-1 col-xs-offset-1">
		<h2><span class="glyphicon glyphicon-left glyphicon-lock"></span> Change password</h2>

		<form class="form-horizontal" role="form" method="post">
			
			<div class="form-group <?php if(form_error('currentPassword')) { echo 'has-error'; } ?>">
				<label for="currentPassword" class="col-sm-3 col-md-3 col-lg-3 control-label">Current password</label>
				<div class="col-xs-10 col-sm-7 col-md-7 col-lg-6">
					<input type="password" class="form-control" name="currentPassword">
				</div>
			</div>
			
			<div class="form-group <?php if(form_error('newPassword')) { echo 'has-error'; } ?>">
				<label for="newPassword" class="col-sm-3 col-md-3 col-lg-3 control-label">New password</label>
				<div class="col-xs-10 col-sm-7 col-md-7 col-lg-6">
					<input type="password" class="form-control" name="newPassword">
				</div>
			</div>
			
			<div class="form-group <?php if(form_error('newPasswordConfirm')) { echo 'has-error'; } ?>">
				<label for="newPasswordConfirm" class="col-sm-3 col-md-3 col-lg-3 control-label">Confirm password</label>
				<div class="col-xs-10 col-sm-7 col-md-7 col-lg-6">
					<input type="password" class="form-control" name="newPasswordConfirm">
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-10 col-sm-4 col-md-7 col-lg-5 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
					<input type="hidden" name="user_id" value="<?php echo $account_info->user_id ?>" />
					<button class="btn btn-primary btn-block" type="submit" name="changePassword" value="1"><span class="glyphicon glyphicon-lock"></span> Change password</button>
				</div>
			</div>
			
		</form>
	</div>
</div>