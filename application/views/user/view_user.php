<div class="page-header">
	<h1><?php echo $user_info->username ?></h1>
	<a class="btn btn-default" href="<?php echo base_url('users') ?>"><span class="glyphicon glyphicon-chevron-left"></span></a>
	
	<ul class="list-inline pull-right">
		<?php
		if(checkPermissions('management'))
		{	?>
			<li><a class="btn btn-primary btn js" href="<?php echo base_url('users/edit/' . $user_info->user_id) ?>"><span class="glyphicon glyphicon-edit"></span></a></li>
			<?php
			if($user_info->account_type == 0) 
			{ ?>
			<li><button class="btn btn-info" data-toggle="modal" data-target="#reactivateModal"><span class="glyphicon glyphicon-repeat"></span></button></li>
			<?php } else { ?>
			<li><button class="btn btn-danger" data-toggle="modal" data-target="#deactivateModal"><span class="glyphicon glyphicon-remove"></span></button></li>
			<?php }
		} ?>
	</ul>
	
</div>

	<ul class="list-inline info-list">
		<li id="info-lastlogged"><span class="glyphicon glyphicon-dashboard"></span> Latest log: <strong><?php echo getTimeSince($user_data->lastLogged) ?></strong></li>
		<?php if(!empty($user_info->name)) { ?><li id="info-user-name"><span class="glyphicon glyphicon-user"></span> <?php echo $user_info->name ?></li><?php } ?>
		<?php if(!empty($user_info->email)) { ?><li id="info-user-email"><span class="glyphicon glyphicon-envelope"></span> <?php echo $user_info->email ?></li><?php } ?>
		<?php if(!empty($user_info->user_id)) { ?><li id="info-user-department"><span class="glyphicon glyphicon-briefcase"></span> <a href="<?php echo base_url('departments/view/' . $user_info->user_id) ?>"><?php echo $user_info->dept_name ?></a></li><?php } ?>
		<li id="info-user-type"><span class="glyphicon glyphicon-th-list"></span>
			<?php 
			$type = $user_info->account_type;
			if($type == 0)
				echo "Deactivated";
			elseif($type == 1)
				echo "Administrator";
			elseif($type == 2)
				echo "Department Manager";
			else
				echo "Employee";
			?>
		</li>
	</ul>

	<hr>

<?php 				
if(validation_errors())
{	?>
	<div class="alert alert-danger">
		<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
		<?php echo validation_errors() ?>
	</div>
	<?php
}	?>


<h2 style="margin-top:30px">Totals at a glance</h2>
<div class="table-responsive">
	<table class="table">
		<thead>
			<th></th>
			<th>Duration</th>
			<th>Cost</th>
		</thead>
		
		<tbody>
			<tr>
				<td><strong>This week</strong></td>
				<td><?php echo formatTime($user_data->thisWeek->totalTime, 2) ?></td>
				<td><?php echo formatCost($user_data->thisWeek->totalCost, 2) ?></td>
			</tr>
			<tr>
				<td><strong>Last week</strong></td>
				<td><?php echo formatTime($user_data->lastWeek->totalTime, 2) ?></td>
				<td><?php echo formatCost($user_data->lastWeek->totalCost, 2) ?></td>
			</tr>
			<tr>
				<td><strong>This month</strong></td>
				<td><?php echo formatTime($user_data->thisMonth->totalTime, 2) ?></td>
				<td><?php echo formatCost($user_data->thisMonth->totalCost, 2) ?></td>
			</tr>
			<tr>
				<td><strong>Last month</strong></td>
				<td><?php echo formatTime($user_data->lastMonth->totalTime, 2) ?></td>
				<td><?php echo formatCost($user_data->lastMonth->totalCost, 2) ?></td>
			</tr>
			<tr class="active">
				<td><strong>Total</strong></td>
				<td><strong class="total"><?php echo formatTime($user_data->totalTime, 2) ?></strong></td>
				<td><strong class="total"><?php echo formatCost($user_data->totalCost, 2) ?></strong></td>
			</tr>
		</tbody>
	</table>
</div>

<h2 style="margin-top:40px">Project contributions <small>(in the last 30 days)</small></h2>
<?php
if($user_data->recentProjects)
{ ?>
	<div class="table-responsive table-striped">
		<table class="table">		
			<thead>
				<th>Project</th>
				<th>Time</th>
				<th>Cost</th>
			</thead>
			<tbody>
			<?php
			foreach($user_data->recentProjects as $projects)
			{	?>
				<tr>
					<td><strong><a href="<?php echo base_url('projects/view/'.$projects->project_id) ?>"><?php echo $projects->project_name ?></a></strong></td>
					<td><?php echo formatTime($projects->totalTime, 2) ?></td>
					<td><?php echo formatCost($projects->totalCost, 2) ?></td>
				</tr>
				<?php
			}	?>
			</tbody>
		</table>
	</div>
	<?php
}	
else
{	?>
	<div class="alert alert-info">
		<span class="glyphicon glyphicon-left glyphicon-dashboard"></span> No time has been logged recently
	</div>
	<?php
}	?>
 
<div class="modal fade" id="deactivateModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form method="post" action="<?php echo base_url('users/delete') ?>">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modalLabel">Deactivate this account</h4>
			</div>
			
			<div class="modal-body">
				Are you sure you wish to deactivate this account?
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				
				<button type="submit" class="btn btn-danger" name="user_id" id="remove_user_btn" value="<?php echo $user_info->user_id ?>">
					<span class="glyphicon glyphicon-remove"></span> Deactivate account
				</button>				
			</div>
		</div>
		</form>
	</div>
</div>

<div class="modal fade" id="reactivateModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form method="post" action="<?php echo base_url('users/reactivate') ?>">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modalLabel">Reactivate this account</h4>
			</div>
			
			<div class="modal-body">
				Are you sure you wish to reactivate this account?
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				
				<button type="submit" class="btn btn-info" name="user_id" id="reactivate_user_btn" value="<?php echo $user_info->user_id ?>">
					<span class="glyphicon glyphicon-ok"></span> Reactivate account
				</button>				
			</div>
		</div>
		</form>
	</div>
</div>