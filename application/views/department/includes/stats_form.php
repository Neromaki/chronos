<div id="add_user" class="collapse <?php if(validation_errors()) { echo "in"; } ?>">
	<div class="panel panel-default">
		<div class="panel-body">
			<?php 
			if(validation_errors())
			{	?>
				<div class="alert alert-danger">
					<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
					<?php echo validation_errors() ?>
				</div>
				<?php
			}	?>
			
			<form class="form-horizontal" role="form" method="post">
			
				<div class="form-group has-feedback <?php if(form_error('username')) { echo 'has-error'; } ?>">
					<label for="username" class="col-sm-2 control-label">Username</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="add-username" name="username" placeholder="e.g. 'jjefferson'" value="<?php echo set_value('username') ?>">
						<span class="glyphicon form-control-feedback"></span>
					</div>
				</div>
				
				<div class="form-group has-feedback <?php if(form_error('password')) { echo 'has-error'; } ?>">
					<label for="password" class="col-sm-2 control-label">Password</label>
					<div class="col-sm-4">
						<input type="password" class="form-control" name="password" id="add-password" />
						<span class="glyphicon form-control-feedback"></span>
					</div>
				</div>
				
				<div class="form-group has-feedback <?php if(form_error('name')) { echo 'has-error'; } ?>">
					<label for="name" class="col-sm-2 control-label">Name</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="name" id="add-name" placeholder="e.g. 'Jeff Jefferson'" value="<?php echo set_value('name') ?>">
						<span class="glyphicon form-control-feedback"></span>
					</div>
				</div>
				
				<div class="form-group has-feedback <?php if(form_error('email')) { echo 'has-error'; } ?>">
					<label for="email" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-4">
						<input type="email" class="form-control" id="add-email" name="email" placeholder="e.g. 'jjefferson@api-tech.com'" value="<?php echo set_value('email') ?>">
						<span class="glyphicon form-control-feedback"></span>
					</div>
				</div>
				
				<div class="form-group">
					<label for="department" class="col-sm-2 control-label">Department</label>
					<div class="col-sm-4">
						<?php
						if(checkPermissions('admin'))
						{	?>
						<select name="department" class="form-control">
							<?php foreach($departments as $d) { ?><option value="<?php echo $d->dept_id ?>"><?php echo $d->dept_name ?></option><?php } ?>
						</select>
							<?php
						}
						else
						{	?>
							<input type="hidden" name="department" value="<?php echo $this->session->userdata('department') ?>" />
							<input type="text" class="form-control" id="dept" name="dept" value="<?php echo $this->session->userdata('department_name') ?>" disabled >
							<?php
						}	?>
					</div>
				</div>
				
				<div class="form-group">
					<label for="type" class="col-sm-2 control-label">Account Type</label>
					<div class="col-sm-4">
						<select name="type" class="form-control">
							<option value="3">Employee</option>
							<option value="2">Manager</option>
							<?php if(checkPermissions('admin')) { ?><option value="1">Admin</option><?php } ?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-7 col-md-2 col-lg-2 col-sm-offset-2">
						<button class="btn btn-primary btn-block" name="add" value="1" id="submit-btn" type="submit"><span class="glyphicon glyphicon-plus"></span> Add user</button>
					</div>
				</div>
				
			</form>
			
		</div>
	</div>
</div>