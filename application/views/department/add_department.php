<div class="page-header">
	<h1>Add Task</h1>
	<a class="btn btn-default btn-small" href="<?php echo base_url('tasks') ?>"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
</div>
	<?php 
	if(!$projects)
	{	?>
		<div class="alert alert-info">
			<span class="glyphicon glyphicon-left glyphicon-info-sign"></span> <strong>Hey!</strong> You currently don't have any projects on the system, which is fine as you don't 
			<em>need</em> them, but if you want to categorise your tasks, you should totally <a class="underline" href="<?php echo base_url('projects/add') ?>">go add one first!</a>
		</div>
		<?php
	}
	
	if(validation_errors())
	{	?>
		<div class="alert alert-danger">
			<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
			<?php echo validation_errors() ?>
		</div>
		<?php
	}	?>
	<form class="form-horizontal" role="form" method="post">
	
		<div class="form-group">
			<label for="project" class="col-sm-2 control-label">Project</label>
			<div class="col-sm-7 col-md-3">
				<select name="project" class="form-control">
					<option value="">None</option>
					<?php
					foreach($projects as $p)
					{	?>
						<option value="<?php echo $p->project_id ?>" <?php echo set_select('project', $p->project_id) ?>><?php echo $p->name ?></option>
						<?php
					}	?>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label for="priority" class="col-sm-2 control-label">Priority</label>
			<div class="col-sm-7 col-md-2">
				<select name="priority" class="form-control">
					<option value="3" <?php echo set_select('priority', '3') ?>>Low</option>
					<option value="2" <?php echo set_select('priority', '2') ?>>Medium</option>
					<option value="1" <?php echo set_select('priority', '1') ?>>High</option>
				</select>
			</div>
		</div>
		
		<!--
		<div class="form-group">
			<label for="start-day" class="col-xs-2 control-label">Start</label>
			<div class="col-xs-3 col-sm-2 col-md-1">
				<input type="text" class="form-control" id="start-day" name="start-day" placeholder="DD" maxlength="2" value="<?php echo set_value('start-day') ?>">
			</div>
			<div class="col-xs-3 col-sm-2 col-md-1">
				<input type="text" class="form-control" id="start-month" name="width" placeholder="MM" maxlength="2" value="<?php echo set_value('start-month') ?>">
			</div>
			<div class="col-xs-3 col-sm-2 col-md-1">
				<input type="text" class="form-control" id="start-year" name="start-year" placeholder="YY" maxlength="2" value="<?php echo set_value('start-year') ?>">
			</div>
		</div>
		
		<div class="form-group">
			<label for="due-day" class="col-xs-2 control-label">Due</label>
			<div class="col-xs-3 col-sm-2 col-md-1">
				<input type="text" class="form-control" id="due-day" name="due-day" placeholder="DD" maxlength="2" value="<?php echo set_value('due-day') ?>">
			</div>
			<div class="col-xs-3 col-sm-2 col-md-1">
				<input type="text" class="form-control" id="due-month" name="width" placeholder="MM" maxlength="2" value="<?php echo set_value('due-month') ?>">
			</div>
			<div class="col-xs-3 col-sm-2 col-md-1">
				<input type="text" class="form-control" id="due-year" name="due-year" placeholder="YY" maxlength="2" value="<?php echo set_value('due-year') ?>">
			</div>
		</div>
		-->
		
		<div class="form-group <?php if(form_error('summary')) { echo 'has-error'; } ?>">
			<label for="summary" class="col-sm-2 control-label">Summary</label>
			<div class="col-sm-7 col-md-5">
				<input type="text" class="form-control" name="summary" placeholder="e.g. 'Create a task for that project'" value="<?php echo set_value('summary') ?>">
			</div>
		</div>
		
		<div class="form-group <?php if(form_error('details')) { echo 'has-error'; } ?>">
			<label for="details" class="col-sm-2 control-label">Details</label>
			<div class="col-sm-7 col-md-5">
				<textarea class="form-control" rows="3" name="details" maxlength="160" placeholder="e.g. 'Make sure to create that specific task for the specific project, namely the Awesome Project'"><?php echo set_value('details') ?></textarea>
			</div>
		</div>
					
		<div class="form-group <?php if(form_error('notes')) { echo 'has-error'; } ?>">
			<label for="notes" class="col-sm-2 control-label">Notes</label>
			<div class="col-sm-7 col-md-5">
				<textarea class="form-control" rows="6" name="notes" id="notes" placeholder="e.g. 'Useful notes about this project'"><?php echo set_value('notes') ?></textarea>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-7 col-md-2 col-lg-2 col-sm-offset-2">
				<button class="btn btn-primary btn-block" type="submit"><span class="glyphicon glyphicon-plus"></span> Add task</button>
			</div>
		</div>
		
	</form>
	<!--
				<div class="controls controls-row date-time">
				<div id="start_date" class="input-append date">
					<input class="span2" data-format="dd/MM/yyyy" type="text" name="start_date" value="">
					<span class="add-on">
						<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
					</span>
				</div>
			</div>
	-->