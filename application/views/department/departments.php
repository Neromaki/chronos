<div class="page-header">
	<h1>Departments</h1>

	<?php if(checkPermissions('admin')) { ?>
		<noscript>
			<a class="btn btn-primary btn-sm" href="<?php echo base_url('departments/add') ?>"><span class="glyphicon glyphicon-left glyphicon-plus"></span> Add new task</a>
		</noscript>

		<ul class="list-inline">
			<li>
				<button type="button" class="btn btn-primary btn-sm js" data-toggle="collapse" data-target="#add_dept">
					<span class="glyphicon glyphicon-plus"></span> Add <b class="caret"></b>
				</button>
			</li>
		</ul>
	<?php } ?>

	<div id="add_dept" class="collapse <?php if(validation_errors()) { echo "in"; } ?>">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php 				
				if(validation_errors())
				{	?>
					<div class="alert alert-danger">
						<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
						<?php echo validation_errors() ?>
					</div>
					<?php
				}	?>
				<form class="form-horizontal" role="form" method="post">
					
					<div class="form-group <?php if(form_error('name')) { echo 'has-error'; } ?>">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-3 col-md-3">
							<input type="text" class="form-control" name="name" placeholder="e.g. 'AWSM'"><?php echo set_value('details') ?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-7 col-md-2 col-lg-2 col-sm-offset-2">
							<button class="btn btn-primary btn-block" type="submit" name="add" value="1"><span class="glyphicon glyphicon-plus"></span> Add department</button>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>

</div>

<?php
if($departments)
{	
	if($pagination)
		echo $pagination;
	?>
	<div class="table-responsive">
		<table class="table table-striped table-hover">	
			<thead>
				<th></th>
				<th>Total time</th>
				<th>Total cost</th>
				<th>Last logged</th>
				<th></th>
			<thead>
			<tbody>
				<?php 
				foreach($departments as $d)
				{	?>
					<tr>
						<td class="title"><?php echo $d->dept_name ?></td>
						<td><?php echo formatTime($d->totalTime) ?></td>
						<td><?php echo formatCost($d->totalCost) ?></td>
						<td><?php echo getTimeSince($d->lastLogged) ?></td>
						<td><a class="btn btn-small btn-default" href="<?php echo base_url('departments/view/'.$d->dept_id) ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></td>
					</tr>
					<?php
				}	?>
			</tbody>
		</table>
	</div>
	<?php
}
else
{	?>
	<div class="alert alert-info">
		<span class="glyphicon glyphicon-left glyphicon-info-sign"></span> <strong>Aw man!</strong> You currently don't have any tasks on the system, <a class="underline" href="<?php echo base_url('tasks/add') ?>">go create a task!</a>
	</div>
	<?php
}	?>