	<div class="page-header">
	<h1><?php echo $dept_data->dept_name ?></h1>
	<a class="btn btn-default btn-back" href="<?php echo base_url('departments') ?>"><span class="glyphicon glyphicon-chevron-left"></span></a>

	
	<ul class="list-inline pull-right">
		<li><button class="btn btn-info" data-toggle="collapse" data-target="#filterDepartment"><span class="glyphicon glyphicon-filter"></span></button></li>
		<?php if(checkPermissions('admin')) { ?><li><button class="btn btn-primary" data-toggle="modal" data-target="#editModal"><span class="glyphicon glyphicon-edit"></span></button></li><?php } ?>
		<?php if(checkPermissions('admin')) { ?><li><button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal"><span class="glyphicon glyphicon-remove"></span></button></li><?php } ?>
	</ul>
</div>

<?php
$dept_managers = array();
$total_employees = 0;
$manager_list = ""; 

if($dept_users)
{
	foreach($dept_users as $u) 
	{
		if($u->account_type == 2)
			$dept_managers[] = $u;
			
		$total_employees++;
	}
}

if($dept_managers)
{
	foreach($dept_managers as $dm) {
		$manager_list .= "<a href='".base_url('users/view/'.$dm->user_id)."'>".$dm	->user_name."</a>, "; 
	}
} 
$manager_list = rtrim($manager_list, ', ');
?>
<ul class="list-inline info-list">
	<li><span class="glyphicon glyphicon-dashboard"></span> Latest log: <strong><?php echo getTimeSince($dept_data->lastLogged) ?></strong></li>
	<li><span class="glyphicon glyphicon-user"></span> Employees: <strong><?php echo $total_employees ?></strong></li>
	<li><span class="glyphicon glyphicon-briefcase"></span> Manager(s): <strong><?php echo $manager_list ?></strong></li>
</ul>

<div id="filterDepartment" class="collapse <?php if(validation_errors()) { echo "in"; } ?>">
	<div class="panel panel-default">
		<div class="panel-body">
			<?php 
			if(validation_errors())
			{	?>
				<div class="alert alert-danger">
					<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
					<?php echo validation_errors() ?>
				</div>
				<?php
			}	?>
			
			<form class="form-horizontal" role="form" method="post">
				
				<div class="form-group has-feedback <?php if(form_error('period_start')) { echo 'has-error'; } ?>">
					<label for="name" class="col-sm-2 control-label">Start</label>
					<div class="col-sm-7 col-md-3">
						<input type="text" class="form-control date-pick" id="period_start" name="period_start" placeholder="Start" data-date-format="DD/MM/YYYY" <?php if($this->input->post('period_start')) { ?>value="<?php echo $this->input->post('period_start') ?>"<?php } ?> />
						<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
					</div>
				</div>

				<div class="form-group has-feedback <?php if(form_error('period_end')) { echo 'has-error'; } ?>">
					<label for="description" class="col-sm-2 control-label">End</label>
					<div class="col-sm-7 col-md-3">
						<input type="text" class="form-control date-pick" id="period_end" name="period_end" placeholder="End" data-date-format="DD/MM/YYYY" <?php if($this->input->post('period_end')) { ?>value="<?php echo $this->input->post('period_end') ?>"<?php } ?> />
						<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-7 col-md-2 col-lg-2 col-sm-offset-2">
						<button class="btn btn-info btn-block" name="filter" value="1" type="submit"><span class="glyphicon glyphicon-filter"></span> Filter department</button>
					</div>
				</div>
				
			</form>
			
		</div>
	</div>
</div>

<hr>

<?php if(!$this->input->post('filter')) { ?>
	<h2 style="margin-top:30px">Totals at a glance</h2>
	<div class="table-responsive">
		<table class="table">
			<thead>
				<th></th>
				<th>Duration</th>
				<th>Cost</th>
			</thead>
			
			<tbody>
				<tr>
					<td><strong>This week</strong></td>
					<td><?php echo formatTime($dept_data->thisWeek->totalTime) ?></td>
					<td><?php echo formatCost($dept_data->thisWeek->totalCost) ?></td>
				</tr>
				<tr>
					<td><strong>Last week</strong></td>
					<td><?php echo formatTime($dept_data->lastWeek->totalTime) ?></td>
					<td><?php echo formatCost($dept_data->lastWeek->totalCost) ?></td>
				</tr>
				<tr>
					<td><strong>This month</strong></td>
					<td><?php echo formatTime($dept_data->thisMonth->totalTime) ?></td>
					<td><?php echo formatCost($dept_data->thisMonth->totalCost) ?></td>
				</tr>
				<tr>
					<td><strong>Last month</strong></td>
					<td><?php echo formatTime($dept_data->lastMonth->totalTime) ?></td>
					<td><?php echo formatCost($dept_data->lastMonth->totalCost) ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php
}
else
{
	?>
	<h2 style="margin-top:30px">Totals between <?php echo $this->input->post('period_start') ?> and <?php echo $this->input->post('period_end') ?></h2>
	<div class="table-responsive clearfix" style="float:left;width:100%;">
		<table class="table" style="width:250px">
			<thead>
				<th>Duration</th>
				<th>Cost</th>
			</thead>
			
			<tbody>
				<tr>
					<td><?php echo formatTime($dept_data->total->totalTime) ?></td>
					<td><?php echo formatCost($dept_data->total->totalCost) ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php
}	?>

<?php if(!$this->input->post('filter')) { ?>
	<h2 style="margin-top:40px">Recent overview</h2>
	<?php
	if($dept_users)
	{
		?>
		<div class="table-responsive">
			<table class="table table-striped table-hover table-central">
				<thead>
					<tr>
						<th></th>
						<th colspan="2">This Week</th>
						<th colspan="2">Last Week</th>
						<th colspan="2">This Month</th>
						<th colspan="2">Last Month</th>
						<th colspan="2">Total</th>
						<th></h>
					</tr>
					<tr>
						<th>Username</th>
						<th>Time</th>
						<th>Costs</th>
						<th>Time</th>
						<th>Costs</th>
						<th>Time</th>
						<th>Costs</th>
						<th>Time</th>
						<th>Costs</th>
						<th>Last logged</th>
					</tr>
				</thead>
				
				<tbody>
				<?php
				foreach($dept_users as $u)
				{	?>
					<tr>
						<td><a href="<?php echo base_url('users/view/'.$u->user_id) ?>"><?php echo $u->user_name ?></a></td>
						<td><?php echo formatTime($u->thisWeekTime) ?></td>
						<td><?php echo formatCost($u->thisWeekCost) ?></td>
						<td><?php echo formatTime($u->lastWeekTime) ?></td>
						<td><?php echo formatCost($u->lastWeekCost) ?></td>
						<td><?php echo formatTime($u->thisMonthTime) ?></td>
						<td><?php echo formatCost($u->thisMonthCost) ?></td>
						<td><?php echo formatTime($u->lastMonthTime) ?></td>
						<td><?php echo formatCost($u->lastMonthCost) ?></td>
						<td><?php echo getTimeSince($u->lastLogged) ?></td>
					</tr>
					<?php
				}	?>
				</tbody>
			</table>
		</div>
		<?php
	}
	else
	{	?>
		<div class="alert alert-info">
			There are no users in this department
		</div>
		<?php
	}
}
else
{	?>
	<h2 style="margin-top:40px">User breakdown</h2>
	<?php
	$x=0;
	foreach($dept_users as $u)
	{
		if($u->totalTime == NULL && $u->totalCost == NULL) {
			unset($dept_users[$x]);
		}
		$x++;
	}

	if($dept_users)
	{
		?>
		<div class="table-responsive">
			<table class="table table-striped table-hover table-central">
				<thead>
					<tr>
						<th>Username</th>
						<th>Time</th>
						<th>Costs</th>
					</tr>
				</thead>
				
				<tbody>
				<?php
				foreach($dept_users as $u)
				{	
					if($u->totalTime != NULL || $u->totalCost != NULL)
					{	?>
						<tr>
							<td><a href="<?php echo base_url('users/view/'.$u->user_id) ?>"><?php echo $u->user_name ?></a></td>
							<td><?php echo formatTime($u->totalTime) ?></td>
							<td><?php echo formatCost($u->totalCost) ?></td>
						</tr>
						<?php
					}
				}	?>
				</tbody>
			</table>
		</div>
		<?php
	}
	else
	{	?>
		<div class="alert alert-info">
			No employees have logged time in this period
		</div>
		<?php
	}
}	?>

<?php if(!$this->input->post('filter')) { ?>
<h2 style="margin-top:40px">Project contributions <small>(in the last 30 days)</small></h2>
<?php
if($dept_data->recentProjects)
{ ?>
	<div class="table-responsive table-striped">
		<table class="table">		
			<thead>
				<th>Project</th>
				<th>Time</th>
				<th>Cost</th>
			</thead>
			<tbody>
			<?php
			foreach($dept_data->recentProjects as $projects)
			{	?>
				<tr>
					<td><strong><a href="<?php echo base_url('projects/view/'.$projects->project_id) ?>"><?php echo $projects->project_name ?></a></strong></td>
					<td><?php echo formatTime($projects->totalTime) ?></td>
					<td><?php echo formatCost($projects->totalCost) ?></td>
				</tr>
				<?php
			}	?>
			</tbody>
		</table>
	</div>
	<?php
}	
else
{	?>
	<div class="alert alert-info">
		<span class="glyphicon glyphicon-left glyphicon-dashboard"></span> No time has been logged recently
	</div>
	<?php
}	?>
	<?php
}	?>



<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form method="post" action="<?php echo base_url('departments/delete') ?>">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modalLabel">Really remove this department?</h4>
			</div>
			
			<div class="modal-body">
				<p>I want to deactivate this department and..</p>
				<div class="col-sm-6">
					<div class="form-group">
						<div class="radio">
							<label>
								<input type="radio" name="remove_action" id="department_remove" value="delete"> Remove 
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="remove_action" id="department_migrate" value="migrate" checked> Migrate
							</label>
						</div>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="action_group[]" value="users" checked> Users
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="action_group[]" value="time" checked> Time
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="action_group[]" value="costs" checked> Costs
							</label>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div id="department_migrate_action">
					<p>Migrate selected aspects to this department..</p>
					<div class="form-group">
						<div class="col-sm-7 col-md-4">
							<select name="new_dept_id" id="department_migrate_select" class="form-control">
							<?php 
							foreach($departments as $d)
							{	
								if($d->dept_id != $dept_data->dept_id)
								{	?>
									<option value="<?php echo $d->dept_id ?>"><?php echo $d->dept_name ?></option>
									<?php
								}
							}	?>
								<option value="new">Create new..</option>
							</select>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
					
				<div class="form-group" id="add_dept" style="margin-top:10px;">
					<div class="col-sm-5">
						<input type="text" class="form-control" name="new_dept_name" id="new_dept_name" placeholder="e.g. 'AWSM'">
					</div>
				</div>	
				
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				
				<button type="submit" class="btn btn-danger" name="dept_id" id="remove_dept_btn" value="<?php echo $dept_data->dept_id ?>">
					<span class="glyphicon glyphicon-remove"></span> Remove department
				</button>				
			</div>
		</div>
		</form>
	</div>
</div>


<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modalLabel">Edit department</h4>
				<?php 				
				if(validation_errors())
				{	?>
					<div class="alert alert-danger">
						<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
						<?php echo validation_errors() ?>
					</div>
					<?php
				}	?>
			</div>
			
			<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('departments/update') ?>">
				<div class="modal-body">
					
					<div class="form-group <?php if(form_error('name')) { echo 'has-error'; } ?>">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-7 col-md-9">
							<input type="text" class="form-control" name="name" id="edit_dept_name" placeholder="e.g. 'AWSM'" value="<?php echo $dept_data->dept_name ?>" />
						</div>
					</div>
				
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary" name="dept_id" id="edit_dept_btn" value="<?php echo $dept_data->dept_id ?>"><span class="glyphicon glyphicon-save"></span> Update department</button>
				</div>
			</form>
			
		</div>
	</div>
</div>


<script type="text/javascript">

	jQuery(document).ready(function () {
		// By default, hide the Add Department <div>
		jQuery('#add_dept').hide();

		// If the user chooses to remove the department's subs, hide the migrate <div>
		jQuery('#department_remove').click(function () {
			if(jQuery(this).is(':checked')) {
				jQuery('#department_migrate_action').hide();
			}
		});
		
		// If the user chooses to migrate the department's subs, show the migrate <div>
		jQuery('#department_migrate').click(function () {
			if(jQuery(this).is(':checked')) {
				jQuery('#department_migrate_action').show();
			}
		});	
		
		// If the user chooses to migrate subs to a new department, show the add <div> and disable submit button
		jQuery('#department_migrate_select').change(function () {
			if(jQuery(this).val() == 'new') {
				jQuery('#add_dept').show();
				jQuery('#remove_dept_btn').attr('disabled','disabled');
			} else {
				jQuery('#add_dept').hide();
				jQuery('#remove_dept_btn').removeAttr('disabled');	
			}
		});
		
		// Every time the user modifies the value of the department name, check if the value is not empty, then enable the button
		// Disable it again if the user empties out the field
		jQuery('#new_dept_name').keyup(function () {
			if(jQuery(this).val() != '') {
				jQuery('#remove_dept_btn').removeAttr('disabled');
			} else {
				jQuery('#remove_dept_btn').attr('disabled','disabled');
			}
		});
		
		jQuery('#edit_dept_name').keyup(function () {
			if(jQuery(this).val() != '') {
				jQuery('#edit_dept_btn').removeAttr('disabled');
			} else {
				jQuery('#edit_dept_btn').attr('disabled','disabled');
			}
		});
		
		
	});
</script>

	<script type="text/javascript">
	 $(function () {
		$('.date-pick').datetimepicker({
			pickTime: false,
			defaultDate: moment() 
		});
	});
	</script>