<div class="page-header">
	<h1>Logs</h1>


<?php
if($logs)
{	
	if($pagination)
		echo $pagination;
	?>
	<div class="table-responsive">
		<table class="table table-striped table-hover">	
			<thead>
				<th>#</th>
				<th>User</th>
				<th>Type</th>
				<th>Subject</th>
				<th>Action</th>
				<th>Time</th>
			<thead>
			<tbody>
				<?php 
				foreach($logs as $l)
				{	?>
					<tr <?php if(date('Y-m-d', time()) == date('Y-m-d', strtotime($l->time))) { echo 'class="success"'; } ?>>
						<td><?php echo $l->log_id ?></td>
						<td><a href="<?php echo base_url('users/view/'.$l->user_id) ?>"><?php echo $l->username ?></a></td>
						<td><?php echo ucfirst($l->action_type) ?></td>
						<td><?php echo ucfirst($l->action_subject) ?></td>
						<td><?php echo $l->action ?></td>
						<td><?php echo formatDateTime($l->time) ?></td>						
					</tr>
					<?php
				}	?>
			</tbody>
		</table>
	</div>
	<?php
}
else
{	?>
	<div class="alert alert-info">
		<span class="glyphicon glyphicon-left glyphicon-info-sign"></span> <strong>Hold on there!</strong> You currently don't have any logs on the system, you should wait a while!
	</div>
	<?php
}	