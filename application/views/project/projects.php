<div class="page-header">
	<h1>Projects</h1>

	<?php if(checkPermissions('admin')) { ?>
		<noscript>
			<a class="btn btn-primary btn-sm" href="<?php echo base_url('projects/add') ?>"><span class="glyphicon glyphicon-plus"></span> Add new project</a>
		</noscript>

		<button type="button" class="btn btn-primary btn-sm js" data-toggle="collapse" data-target="#add_project">
		  <span class="glyphicon glyphicon-left glyphicon-plus"></span> Add <b class="caret"></b>
		</button>
	<?php } ?>
	
	<button type="button" class="btn btn-info btn-sm js" data-toggle="collapse" data-target="#search_project">
	  <span class="glyphicon glyphicon-left glyphicon-filter"></span> Filter  <b class="caret"></b>
	</button>
	
	<div id="add_project" class="collapse <?php if(validation_errors()) { echo "in"; } ?>">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php 
				if(validation_errors())
				{	?>
					<div class="alert alert-danger">
						<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
						<?php echo validation_errors() ?>
					</div>
					<?php
				}	?>
				
				<form class="form-horizontal" role="form" method="post">
				
					<div class="form-group <?php if(form_error('name')) { echo 'has-error'; } ?>">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-7 col-md-5">
							<input type="text" class="form-control" name="name" placeholder="e.g. '15823 - Awesome - AWSM-JOB'" value="<?php echo set_value('name') ?>">
						</div>
					</div>
					
					<div class="form-group <?php if(form_error('description')) { echo 'has-error'; } ?>">
						<label for="description" class="col-sm-2 control-label">Description</label>
						<div class="col-sm-7 col-md-5">
							<textarea class="form-control" rows="3" name="description" maxlength="120" placeholder="e.g. 'Awesome project creating robotic beavers with lasers mounted on them'"><?php echo set_value('description') ?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-7 col-md-2 col-lg-2 col-sm-offset-2">
							<button class="btn btn-primary btn-block" name="add" value="1" type="submit"><span class="glyphicon glyphicon-plus"></span> Add project</button>
						</div>
					</div>
					
				</form>
				
			</div>
		</div>
	</div>
	
	<div id="search_project" class="collapse <?php if($this->input->post('search')) { echo "in"; } ?>">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php 
				if(validation_errors())
				{	?>
					<div class="alert alert-danger">
						<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
						<?php echo validation_errors() ?>
					</div>
					<?php
				}	?>
				
				<form class="form-horizontal" role="form" method="post">
				
					<div class="form-group <?php if(form_error('search_string')) { echo 'has-error'; } ?>">
						<label for="search_string" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-7 col-md-5">
							<input type="text" class="form-control" name="search_string" placeholder="e.g. '15823 - Awesome - AWSM-JOB'" value="<?php if($search = $this->input->post('search_string')) { echo $search; } else { echo set_value('search_string'); } ?>">
						</div>
					</div>
					
					<div class="form-group">
						<label for="search_type" class="col-sm-2 control-label">Type</label>
						<div class="col-sm-3 col-md-2">
							<?php if(!$type = $this->input->post('search_type')) { $type = ""; } ?>
							<select class="form-control" name="search_type">
								<option <?php if($type == '') { echo "selected"; } ?> value="">All</option>
								<option <?php if($type == '1') { echo "selected"; } ?> value="1">Active</option>
								<option <?php if($type == '0') { echo "selected"; } ?> value="0">Inactive</option>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label for="search_order" class="col-sm-2 control-label">Order</label>
						<div class="col-sm-3 col-md-2">
							<?php if(!$order = $this->input->post('search_order')) { $order = ""; } ?>
							<select class="form-control" name="search_order">
								<option <?php if($order == 'project_name') { echo "selected"; } ?> value="project_name">Name</option>
								<option <?php if($order == 'totalTime') { echo "selected"; } ?> value="totalTime">Total time</option>
								<option <?php if($order == 'totalCost') { echo "selected"; } ?> value="totalCost">Total cost</option>
								<option <?php if($order == 'lastLogged') { echo "selected"; } ?> value="lastLogged">Last logged</option>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-7 col-md-2 col-lg-2 col-sm-offset-2">
							<button class="btn btn-info btn-block" name="search" value="1" type="submit"><span class="glyphicon glyphicon-filter"></span> Filter projects</button>
						</div>
					</div>
					
				</form>
				
			</div>
		</div>
	</div>
	
	
</div>

<?php
if($projects)
{
	if($pagination)
		echo $pagination;
	?>
	<div class="table-responsive">
		<table class="table table-striped table-hover">	
			<thead>
				<th></th>
				<th>Total time</th>
				<th>Total cost</th>
				<th>Last logged</th>
				<th></th>
			<thead>
			<tbody>
				<?php 
				//var_dump($projects);
				foreach($projects as $p)
				{	?>
					<tr <?php if($p->active == 0) { echo 'class="deactivated"'; } ?>>
						<td><span class="title"><?php echo $p->project_name ?></span></td>
						<td><?php if($p->totalTime > 0) { echo formatTime($p->totalTime); } else { echo '-'; } ?></td>
						<td><?php if($p->totalCost > 0) { echo formatCost($p->totalCost); } else { echo '-'; } ?></td>
						<td><?php if($p->lastLogged) { echo getTimeSince($p->lastLogged); } else { echo '-'; } ?></td>
						<td><a class="btn btn-default btn-mini" href="<?php echo base_url('projects/view/' . $p->project_id) ?>"><span class="glyphicon glyphicon-chevron-right"></span></button></td>
					</tr>
					<?php
				}	?>
			</tbody>
		</table>
	</div>
	<?php
}
else
{	?>
	<div class="alert alert-info">
		<?php if($this->input->post('search')) { ?>
			<span class="glyphicon glyphicon-left glyphicon-info-sign"></span> <strong>Aw man!</strong> No projects were found using your search query. Try a different one!
		<?php } else { ?>
			<span class="glyphicon glyphicon-left glyphicon-info-sign"></span> <strong>Aw man!</strong> You currently don't have any projects on the system, you should totally <a class="underline" href="<?php echo base_url('projects/add') ?>">create one</a> to get started!
		<?php } ?>
	</div>
	<?php
}	