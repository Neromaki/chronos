<div class="page-header">
	<h1>Add Project</h1>
	<a class="btn btn-default btn-small" href="<?php echo base_url('tasks') ?>"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
</div>
	<?php 
	if(validation_errors())
	{	?>
		<div class="alert alert-danger">
			<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
			<?php echo validation_errors() ?>
		</div>
		<?php
	}	?>
	<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
	
		
		<div class="form-group <?php if(form_error('name')) { echo 'has-error'; } ?>">
			<label for="name" class="col-sm-2 control-label">Name</label>
			<div class="col-sm-7 col-md-5">
				<input type="text" class="form-control" name="name" placeholder="e.g. 'Super Awesome Project'" value="<?php echo set_value('name') ?>">
			</div>
		</div>
		
		<div class="form-group <?php if(form_error('description')) { echo 'has-error'; } ?>">
			<label for="description" class="col-sm-2 control-label">Description</label>
			<div class="col-sm-7 col-md-5">
				<textarea class="form-control" rows="3" name="description" maxlength="120" placeholder="e.g. 'Super Secret Awesome project about creating robotic beavers with lasers mounted on them'"><?php echo set_value('description') ?></textarea>
			</div>
		</div>
		
		<div class="form-group <?php if(form_error('slug')) { echo 'has-error'; } ?>">
			<label for="slug" class="col-sm-2 control-label">Slug</label>
			<div class="col-sm-7 col-md-3">
				<input type="text" class="form-control" name="slug" placeholder="e.g. 'superawesome'" value="<?php echo set_value('slug') ?>">
			</div>
		</div>
					
		<div class="form-group <?php if(form_error('notes')) { echo 'has-error'; } ?>">
			<label for="notes" class="col-sm-2 control-label">Notes</label>
			<div class="col-sm-7 col-md-5">
				<textarea class="form-control" rows="6" name="notes" id="notes" placeholder="e.g. 'Useful notes about this project'"><?php echo set_value('notes') ?></textarea>
			</div>
		</div>		
		
		<div class="form-group">
			<label for="project_icon" class="col-sm-2 control-label">Icon <small>(optional)</small></label>
			<div class="col-sm-7 col-md-3">
				<input type="file" class="form-control" name="project_icon">
				<span class="help-block alert alert-info"><span class="glyphicon glyphicon-left glyphicon-info-sign"></span> Icons must be either PNG or JPG, and exactly 40px x 40px</span>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-7 col-md-2 col-lg-2 col-sm-offset-2">
				<button class="btn btn-primary btn-block" type="submit"><span class="glyphicon glyphicon-plus"></span> Add project</button>
			</div>
		</div>
		
	</form>
	<!--
				<div class="controls controls-row date-time">
				<div id="start_date" class="input-append date">
					<input class="span2" data-format="dd/MM/yyyy" type="text" name="start_date" value="">
					<span class="add-on">
						<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
					</span>
				</div>
			</div>
	-->