<div class="page-header">
	<img src="<?php echo base_url('img/icons/'.$project_data->icon_path.'.png') ?>" class="view-project-icon">
	<h1><?php echo $project_data->name ?> <small>Changelog</small></h1>
	<a class="btn btn-default btn-small" href="<?php echo base_url('projects/' . $this->uri->segment(2)) ?>"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
</div>

<?php 	
if($changelog)
{	
	foreach($changelog as $cl)
	{	?>
		<div class="panel panel-default">
			<div class="panel-heading"><?php echo 'v' . $cl->version ?></div>
			<table class="table">
			<?php
			foreach($cl->logs as $l)
			{	?>
				<tr>
					<td><strong><?php echo $l->brief ?></strong></td>
					<td><?php echo $l->log ?></td>
				</tr>
				<?php
			}	?>
			</table>
		</div>
		<?php
	}
}
else
{	?>
	<div class="alert alert-info">
		There are no changelogs for this project yet
	</div>
	<?php
}	?>