<div class="page-header">
	<h1><?php echo $project_data->project_name ?><br/><small><?php echo $project_data->description ?></small></h1>
	<a class="btn btn-default" href="<?php echo base_url('projects') ?>"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<ul class="list-inline pull-right">
		<li><button class="btn btn-info" data-toggle="collapse" data-target="#filterProject"><span class="glyphicon glyphicon-filter"></span></button></li>
		<?php if(checkPermissions('admin')) { ?><li><button class="btn btn-primary" data-toggle="modal" data-target="#editModal"><span class="glyphicon glyphicon-edit"></span></button></li><?php } ?>
		<?php if(checkPermissions('admin') && $project_data->active == 1) { ?><li><button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal"><span class="glyphicon glyphicon-remove"></span></button></li><?php } ?>
		<?php if(checkPermissions('admin') && $project_data->active == 0) { ?><li><button class="btn btn-warning" data-toggle="modal" data-target="#activateModal"><span class="glyphicon glyphicon-repeat"></span></button></li><?php } ?>

	</ul>
</div>

<div id="filterProject" class="collapse <?php if(validation_errors()) { echo "in"; } ?>">
	<div class="panel panel-default">
		<div class="panel-body">
			<?php 
			if(validation_errors())
			{	?>
				<div class="alert alert-danger">
					<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
					<?php echo validation_errors() ?>
				</div>
				<?php
			}	?>
			
			<form class="form-horizontal" role="form" method="post">
				
				<div class="form-group has-feedback <?php if(form_error('period_start')) { echo 'has-error'; } ?>">
					<label for="name" class="col-sm-2 control-label">Start</label>
					<div class="col-sm-7 col-md-3">
						<input type="text" class="form-control date-pick" id="period_start" name="period_start" placeholder="Start" data-date-format="DD/MM/YYYY" <?php if($this->input->post('period_start')) { ?>value="<?php echo $this->input->post('period_start') ?>"<?php } ?> />
						<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
					</div>
				</div>

				<div class="form-group has-feedback <?php if(form_error('period_end')) { echo 'has-error'; } ?>">
					<label for="description" class="col-sm-2 control-label">End</label>
					<div class="col-sm-7 col-md-3">
						<input type="text" class="form-control date-pick" id="period_end" name="period_end" placeholder="End" data-date-format="DD/MM/YYYY" <?php if($this->input->post('period_end')) { ?>value="<?php echo $this->input->post('period_end') ?>"<?php } ?> />
						<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-7 col-md-2 col-lg-2 col-sm-offset-2">
						<button class="btn btn-info btn-block" name="filter" value="1" type="submit"><span class="glyphicon glyphicon-filter"></span> Filter project</button>
					</div>
				</div>
				
			</form>
			
		</div>
	</div>
</div>

<ul class="list-inline info-list">
	<li><span class="glyphicon glyphicon-dashboard"></span> Last logged: <strong><?php echo getTimeSince($project_data->lastLogged) ?></strong></li>
	<li><span class="glyphicon glyphicon-time"></span> Total time: <strong><?php if($this->input->post('filter')) { echo formatTime($project_data->custom->totalTime); } else { echo formatTime($project_data->totalTime); } ?></strong></li>
	<li><span class="glyphicon glyphicon-gbp"></span> Total cost: <strong><?php if($this->input->post('filter')) { echo formatCost($project_data->custom->totalCost); } else { echo formatCost($project_data->totalCost); } ?></strong></li>
</ul>

<hr>

<?php
if($this->input->post('filter'))
{	?>
<h2 style="margin-top:30px">Totals between <?php echo $this->input->post('period_start') ?> and <?php echo $this->input->post('period_end') ?></h2>
<div class="table-responsive clearfix" style="float:left;width:100%;">
	<table class="table" style="width:250px">
		<thead>
			<th>Duration</th>
			<th>Cost</th>
		</thead>
		
		<tbody>
			<tr>
				<td><?php echo formatTime($project_data->custom->totalTime) ?></td>
				<td><?php echo formatCost($project_data->custom->totalCost) ?></td>
			</tr>
		</tbody>
	</table>
</div>
<?php
}
else
	{	?>
<h2 style="margin-top:30px">Recent overview</h2>
<div class="table-responsive">
	<table class="table">
		<thead>
			<th></th>
			<th>Duration</th>
			<th>Cost</th>
		</thead>
		
		<tbody>
			<tr>
				<td><strong>This week</strong></td>
				<td><?php echo formatTime($project_data->thisWeek->totalTime) ?></td>
				<td><?php echo formatCost($project_data->thisWeek->totalCost) ?></td>
			</tr>
			<tr>
				<td><strong>Last week</strong></td>
				<td><?php echo formatTime($project_data->lastWeek->totalTime) ?></td>
				<td><?php echo formatCost($project_data->lastWeek->totalCost) ?></td>
			</tr>
			<tr>
				<td><strong>This month</strong></td>
				<td><?php echo formatTime($project_data->thisMonth->totalTime) ?></td>
				<td><?php echo formatCost($project_data->thisMonth->totalCost) ?></td>
			</tr>
			<tr>
				<td><strong>Last month</strong></td>
				<td><?php echo formatTime($project_data->lastMonth->totalTime) ?></td>
				<td><?php echo formatCost($project_data->lastMonth->totalCost) ?></td>
			</tr>
		</tbody>
	</table>
</div>
<?php
} ?>

<hr>

<h2 style="margin-top:40px">Department breakdown<br /><small>(click each row to view by deptartment employee)</small></h2>
<?php

if($project_depts)
{	
	if($this->input->post('filter'))
	{	?>
	<div class="table-responsive">
		<table class="table table-striped table-hover table-central project-dept-stats">
			<thead>
				<tr class="active">
					<th>Department</th>
					<th>Time</th>
					<th>Cost</th>
					<th></th>
				</tr>
			</thead>
			
			<tbody>
			<?php
			foreach($project_depts as $pd)
			{
				?>
				<tr class="project_dept" data-dept-id="<?php echo $pd->dept_id ?>">
					<td><strong><?php echo $pd->dept_name ?> <b class="caret"></b></strong></td>
					<td><?php echo formatTime($pd->totalTime, 2) ?></td>
					<td><?php echo formatCost($pd->totalCost) ?></td>
					<td><a class="btn btn-default btn-xs" href="<?php echo base_url('departments/view/'.$pd->dept_id) ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></td>
				</tr>
				<?php
				foreach($pd->dept_data as $dept_employee)
				{	
					if(!is_null($dept_employee->totalTime) ||
						!is_null($dept_employee->totalCost))
					{
						?>
						<tr class="project_users info" data-dept-id="<?php echo $pd->dept_id ?>" style="display: none">
							<td><?php echo $dept_employee->user_name ?></td>
							<td><?php echo formatTime($dept_employee->totalTime, 2) ?></td>
							<td><?php echo formatCost($dept_employee->totalCost) ?></td>
							<td><a class="btn btn-default btn-xs" href="<?php echo base_url('users/view/'.$dept_employee->user_id) ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></td>
						</tr>
						<?php
					}
				}
			}	?>
			</tbody>
		</table>
	</div>
		<?php
	}
	else 
	{ ?>
	<div class="table-responsive">
		<table class="table table-striped table-hover table-central project-dept-stats">
			<thead>
				<tr>
					<th></th>
					<th colspan="2">This Week</th>
					<th colspan="2">Last Week</th>
					<th colspan="2">This Month</th>
					<th colspan="2">Last Month</th>
					<th></th>
				</tr>
				<tr class="active">
					<th>Department</th>
					<th>Time</th>
					<th>Costs</th>
					<th>Time</th>
					<th>Costs</th>
					<th>Time</th>
					<th>Costs</th>
					<th>Time</th>
					<th>Costs</th>
					<th></th>
				</tr>
			</thead>
			
			<tbody>
			<?php
			foreach($project_depts as $pd)
			{
				?>
				<tr class="project_dept" data-dept-id="<?php echo $pd->dept_id ?>">
					<td><strong><?php echo $pd->dept_name ?> <b class="caret"></b></strong></td>
					<td><?php echo formatTime($pd->thisWeekTime) ?></td>
					<td><?php echo formatCost($pd->thisWeekCost) ?></td>
					<td><?php echo formatTime($pd->lastWeekTime) ?></td>
					<td><?php echo formatCost($pd->lastWeekCost) ?></td>
					<td><?php echo formatTime($pd->thisMonthTime) ?></td>
					<td><?php echo formatCost($pd->thisMonthCost) ?></td>
					<td><?php echo formatTime($pd->lastMonthTime) ?></td>
					<td><?php echo formatCost($pd->lastMonthCost) ?></td>
					<td><a class="btn btn-default btn-xs" href="<?php echo base_url('departments/view/'.$pd->dept_id) ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></td>
				</tr>
				<?php
				foreach($pd->dept_data as $dept_employee)
				{	
					if(!is_null($dept_employee->thisWeekTime) ||
						!is_null($dept_employee->thisWeekCost) ||
						!is_null($dept_employee->lastWeekTime) ||
						!is_null($dept_employee->lastWeekCost) ||
						!is_null($dept_employee->thisMonthTime) ||
						!is_null($dept_employee->thisMonthCost) ||
						!is_null($dept_employee->lastMonthTime) ||
						!is_null($dept_employee->lastMonthCost))
					{
						?>
						<tr class="project_users info" data-dept-id="<?php echo $pd->dept_id ?>" style="display: none">
							<td><?php echo $dept_employee->user_name ?></td>
							<td><?php echo formatTime($dept_employee->thisWeekTime) ?></td>
							<td><?php echo formatCost($dept_employee->thisWeekCost) ?></td>
							<td><?php echo formatTime($dept_employee->lastWeekTime) ?></td>
							<td><?php echo formatCost($dept_employee->lastWeekCost) ?></td>
							<td><?php echo formatTime($dept_employee->thisMonthTime) ?></td>
							<td><?php echo formatCost($dept_employee->thisMonthCost) ?></td>
							<td><?php echo formatTime($dept_employee->lastMonthTime) ?></td>
							<td><?php echo formatCost($dept_employee->lastMonthCost) ?></td>
							<td><a class="btn btn-default btn-xs" href="<?php echo base_url('users/view/'.$dept_employee->user_id) ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></td>
						</tr>
						<?php
					}
				}
			}	?>
			</tbody>
		</table>
	</div>
	<?php
	}
}
else
{	?>
	<div class="alert alert-info">
		<span class="glyphicon glyphicon-time"></span> No time has been recently logged for this project
	</div>
	<?php
}	?>

<?php
if(!$this->input->post('filter'))
{	?>
<hr>

	<h2>Time logged total</h2>
	
	<div class="row">
	<?php
	if($loggedTime['allTime'])
	{
		$depts = array();
		$temp_dept = "";
		$x = 0;
		foreach($loggedTime['allTime'] as $tw)
		{
			if($tw->dept_id != $temp_dept)
			{
				$x++;
				$depts[$x]['dept_id'] 		= $tw->dept_id;
				$depts[$x]['dept_name'] 	= $tw->dept_name;
				$depts[$x]['dept_duration']	= $tw->duration;
			}
			else
				$depts[$x]['dept_duration']	+= $tw->duration;
				
			$temp_dept = $tw->dept_id;
		}
		?>

		<?php
		if($loggedTime['allTime'])
		{	
			foreach($depts as $d)
			{	?>
				<div class="col-xs-12 col-sm-6">
					<div class="time-panel-dept" data-target="#deptallTime<?php echo $d['dept_id'] ?>trigger">
						<h3><?php echo $d['dept_name']; ?> <br/><small><?php echo formatTime($d['dept_duration']) ?></small></h3>
						<div id="deptallTime<?php echo $d['dept_id'] ?>trigger">
							<table class="table table-hover table-striped" style="width: 50%;">
								<thead>
									<th style="width: 50%;">User</th>
									<th style="width: 50%;">Time</th>
								</thead>
								<tbody>
									<?php
									foreach($loggedTime['allTime'] as $tw)
									{	
										if($tw->dept_id == $d['dept_id'])
										{	?>
										<tr>
											<td><?php echo $tw->user_name ?></td>
											<td><?php echo formatTime($tw->duration) ?></td>
										</tr>
											<?php
										}
									}	?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<?php
			}
		}	
	}
	else
	{	?>
		<div class="alert alert-info">
			<span class="glyphicon glyphicon-time"></span> No time has been logged for this project
		</div>
		<?php
	}	?>
	</div>
	<?php
}	?>
<!--
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modalLabel">Really delete <?php echo $project_data->project_name ?>?</h4>
			</div>
			
			<form method="post" action="<?php echo base_url('projects/delete') ?>">
				<div class="modal-body">
					Are you really sure you want to delete <?php echo $project_data->project_name ?>? 
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-danger" name="project_id" value="<?php echo $project_data->project_id ?>"><span class="glyphicon glyphicon-remove"></span> Delete project</button>
				</div>
			</form>
		</div>
	</div>
</div>
-->

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modalLabel">Edit project</h4>
			</div>
			
			<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('projects/update') ?>">
				<div class="modal-body">
					
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-7 col-md-9">
							<input type="text" class="form-control" name="name" placeholder="e.g. 'Super Awesome Project'" value="<?php echo $project_data->project_name ?>">
						</div>
					</div>
					
					<div class="form-group">
						<label for="description" class="col-sm-2 control-label">Description</label>
						<div class="col-sm-7 col-md-9">
							<textarea class="form-control" rows="3" name="description" maxlength="250" placeholder="e.g. 'Super Secret Awesome project about creating robotic beavers with lasers mounted on them'"><?php echo $project_data->description ?></textarea>
						</div>
					</div>
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary" name="project_id" value="<?php echo $project_data->project_id ?>"><span class="glyphicon glyphicon-save"></span> Update project</button>
				</div>
			</form>
			
		</div>
	</div>
</div>


<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form method="post" action="<?php echo base_url('projects/delete') ?>">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modalLabel">Really deactivate this project?</h4>
			</div>
			
			<div class="modal-body">
				<p>Are you sure you wish to deactivate this project? <em>(you can re-activate it again later)</em></p>
				<p>Deactivating a project means employees will no longer be able to log time against it</p>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				
				<button type="submit" class="btn btn-danger" name="project_id" id="remove_project_btn" value="<?php echo $project_data->project_id ?>">
					<span class="glyphicon glyphicon-remove"></span> Deactivate project
				</button>				
			</div>
		</div>
		</form>
	</div>
</div>

<div class="modal fade" id="activateModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form method="post" action="<?php echo base_url('projects/activate') ?>">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modalLabel">Re-activate this project?</h4>
			</div>
			
			<div class="modal-body">
				<p>Are you sure you wish to re-activate this project?</p>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				
				<button type="submit" class="btn btn-warning" name="project_id" id="activate_project_btn" value="<?php echo $project_data->project_id ?>">
					<span class="glyphicon glyphicon-repeat"></span> Re-activate project
				</button>				
			</div>
		</div>
		</form>
	</div>
</div>

	<script type="text/javascript">
	 $(function () {
		$('.date-pick').datetimepicker({
			pickTime: false,
			defaultDate: moment() 
		});
	});
	</script>