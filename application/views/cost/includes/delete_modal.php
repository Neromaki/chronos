<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<form method="post" action="<?php echo base_url('costs/delete') ?>">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modalLabel">Delete this cost?</h4>
			</div>
			
			<div class="modal-body">
				Are you sure you wish to delete this cost?
				
				<dl class="dl-modal">
				
					<dt>Date</dt>
					<dd id="deleteModalDate"></dd>
				
					<dt>Project</dt>
					<dd id="deleteModalProject"></dd>
				
					<dt>Amount</dt>
					<dd id="deleteModalAmount"></dd>
				
					<dt>Reason</dt>
					<dd id="deleteModalReason"></dd>
				</dl>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				
				<button type="submit" class="btn btn-danger" name="cost_id" id="deleteCost" value="">
					<span class="glyphicon glyphicon-remove"></span> Delete cost
				</button>				
			</div>
		</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$('.delete-cost').on('click', function() {
		var btn = $(this);
		btn.button('loading');
		jQuery(this).spin('tiny');
		
		jQuery.ajax({
		  type: "POST",
		  url: "<?php echo base_url() ?>costs/functions/getCostData",
		  data: { cost_id: jQuery(this).data('id') }
		})
		  .success(function( data ) {
			data = JSON.parse(data);
			$('#deleteModalDate').html(data.date);
			$('#deleteModalProject').html(data.project_name);
			$('#deleteModalAmount').html("&pound;" + data.amount);
			$('#deleteModalReason').html(data.reason);
			$('#deleteCost').val(data.cost_id);
			jQuery(this).spin(false);
			btn.button('reset');
			$('#deleteModal').modal();
		  });
		 
	});
</script>