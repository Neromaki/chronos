<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<form method="post" action="<?php echo base_url('costs/update') ?>">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modalLabel">Update cost</h4>
			</div>
			
			<div class="modal-body">
				
				<div class="form-group">
					<label for="updateModalDate">Date</label>
					<input type="text" class="form-control" id="updateModalDate" name="update_date" data-date-format="DD/MM/YYYY" placeholder="Date">
				</div>
				
				<div class="form-group">
					<label for="updateModalProject">Project</label>
					<input type="text" class="form-control" id="updateModalProject" name="update_project" placeholder="Project">
				</div>
				
				<div class="form-group">
					<label for="updateModalAmount">Amount</label>
					<div class="input-group">
					  <span class="input-group-addon">&pound;</span>
					  <input type="text" class="form-control" style="text-align:right"id="updateModalAmount" name="update_amount" placeholder="Amount">
					  <span class="input-group-addon">.00</span>
					</div>
				</div>
				
				<div class="form-group">
					<label for="updateModalReason">Reason</label>
					<textarea class="form-control" id="updateModalReason" name="update_reason" rows="4"></textarea>
				</div>
					
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				
				<button type="submit" class="btn btn-primary" name="cost_id" id="updateCost" value="">
					<span class="glyphicon glyphicon-save"></span> Update cost
				</button>				
			</div>
		</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$('.update-cost').on('click', function() {
		var btn = $(this);
		btn.button('loading');
		jQuery(this).spin('tiny');
		
		jQuery.ajax({
		  type: "POST",
		  url: "<?php echo base_url() ?>costs/functions/getCostData",
		  data: { cost_id: jQuery(this).data('id') }
		})
		  .success(function( data ) {
			data = JSON.parse(data);
			
			var date = moment(data.date, 'YYYY-MM-DD');
			$('#updateModalDate').val(date.format('DD/MM/YYYY'));
			$('#updateModalProject').val(data.project_name);
			$('#updateModalAmount').val(Math.round(data.amount));
			$('#updateModalReason').val(data.reason);
			$('#updateCost').val(data.cost_id);
			
			$('#updateModalDate').datetimepicker({ pickTime:false, defaultDate:moment() });	
			btn.button('reset');
			jQuery(this).spin(false);
			$('#updateModal').modal();
		  });
		 
	});
</script>


<script type="text/javascript">

	jQuery('#updateModalAmount').keyup(function () 
	{
		// Get the input (which is this, but this allows for the function to be generic later)
		var input = jQuery(this);
		var wrapper = jQuery(this).closest('.form-group');
		var icon = jQuery(this).next();
		// Remove any icons
		wrapper.removeClass('has-success has-error has-warning');
		icon.removeClass('glyphicon-ok glyphicon-remove glyphicon-warning');
		
		// Remove any error messages if already there
		jQuery('#amount-error').remove();
		
		// Check the contents
		if(jQuery.isNumeric(input.val())) {
			// If the username doesn't already exist, highlight green, add a friendly icon, clean up and enable submit button
			wrapper.addClass('has-success');
			jQuery('span.help-block', wrapper).remove();
			jQuery('#updateCost').removeAttr('disabled');	
		} else {
			// If username already exists, highlight red, add icon, ensure button is disabled and output error
			wrapper.addClass('has-error');
			jQuery('#updateCost').attr('disabled','disabled');
			wrapper.append('<span class="help-block" id="amount-error">Duration must be numeric</span>');
		}
  

	});
</script>