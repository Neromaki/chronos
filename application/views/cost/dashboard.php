<div class="page-header">
	<h1>Costs</h1>

	<a href="<?php echo base_url('costs/add') ?>" class="btn btn-primary btn-sm no-js">
	  <span class="glyphicon glyphicon-left glyphicon-plus"></span> Add
	</a>

	<button type="button" class="btn btn-info btn-sm js" data-toggle="collapse" data-target="#filter_time">
	  <span class="glyphicon glyphicon-left glyphicon-filter"></span> Filter  <b class="caret"></b>
	</button>

</div>

<div id="filter_time" class="collapse <?php if($this->input->post('filter')) { echo "in"; } ?>">
	<div class="panel panel-default">
		<div class="panel-body">
			<?php 
			if(validation_errors())
			{	?>
				<div class="alert alert-danger">
					<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
					<?php echo validation_errors() ?>
				</div>
				<?php
			}	?>
			
			<form class="form-horizontal" id="cost_filters" role="form" method="post">
			
				<div class="form-group <?php if(form_error('filter_date_start')) { echo 'has-error'; } ?> has-feedback">
					<label for="filter_date_start" class="col-sm-2 control-label">Start date</label>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
						<input type="text" class="form-control date-pick" id="filter_date_start" name="filter_date_start" placeholder="Start" data-date-format="DD/MM/YYYY" value="<?php if(isset($filters['date_start'])) { echo $filters['date_start']; } ?>" />
						<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
					</div>
				</div>
				
				<div class="form-group <?php if(form_error('filter_date_end')) { echo 'has-error'; } ?> has-feedback">
					<label for="filter_date_start" class="col-sm-2 control-label">End date</label>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
						<input type="text" class="form-control date-pick" id="filter_date_end" name="filter_date_end" placeholder="End" data-date-format="DD/MM/YYYY" value="<?php if(isset($filters['date_end'])) { echo $filters['date_end']; } ?>" />
						<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
					</div>
				</div>			
				
				<div class="form-group <?php if(form_error('search_string')) { echo 'has-error'; } ?> has-feedback">
					<label for="filter_project" class="col-sm-2 control-label">Project</label>
					<div class="col-xs-12 col-sm-7 col-md-5 col-lg-4">
						<input type="text" class="form-control project-pick" id="filter_project" name="filter_project" placeholder="Project" value="<?php if(isset($filters['project'])) { echo $filters['project']; } ?>" />
						<span class="glyphicon glyphicon-folder-open form-control-feedback"></span>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-7 col-md-3 col-lg-3 col-sm-offset-2">
						<button class="btn btn-info" name="filter" value="1" type="submit"><span class="glyphicon glyphicon-filter"></span> Filter costs</button>
						<button class="btn btn-default" id="clearFilters" type="button"><span class="glyphicon glyphicon-remove-circle"></span> Clear</button>
					</div>
				</div>
				
			</form>
			
		</div>
	</div>
</div>


<div class="page-body">
	<?php
	if($costs)
	{	?>
		<?php echo $pagination ?>
		
		<table class="table table-striped table-hover">
			<thead>
				<th>Date</th>
				<th>Project</th>
				<th style="text-align:center">Amount</th>
				<th>Reason</th>
			</thead>
			<tbody>
				<?php
				foreach($costs as $c)
				{	?>
					<tr <?php if(is_array($this->session->flashdata('cost_added'))) { if(in_array($c->cost_id, $this->session->flashdata('cost_added'))) { echo 'class="success"'; } } ?>>
						<td><?php echo date('d/m/Y', strtotime($c->date)) ?></td>
						<td><?php echo $c->project_name ?></td>
						<td style="text-align: center">&pound;<?php echo $c->amount ?></td>
						<td><?php echo substr($c->reason, 0, 120) ?></td>
						<td class="xs">
							<button class="btn btn-primary btn-sm update-cost" data-id="<?php echo $c->cost_id ?>" data-loading-text=" "><span class="glyphicon glyphicon-edit"></span></button>
							<div class="clearfix visible-xs"></div>
							<button class="btn btn-danger btn-sm delete-cost" data-id="<?php echo $c->cost_id ?>" data-loading-text=" "><span class="glyphicon glyphicon-remove"></span></button>
						</td>
					</tr>
					<?php
				}	?>
			</tbody>
		</table>
		<?php
	}
	else 
	{	?>
		<div class="alert alert-info">
			Sorry, no logged costs matching your filters were found.
		</div>
		<?php
	}	?>

</div>

<?php $this->load->view('cost/includes/update_modal') ?>
<?php $this->load->view('cost/includes/delete_modal') ?>

  <script type="text/javascript">
	 $(function () {
		$('.date-pick').datetimepicker({
			pickTime: false
		});
	});
  
  
  $(function() {
	var projects = <?php echo json_encode($projects) ?>;
	
	setGlobalProjects(projects);
	
	$( ".project-pick" ).autocomplete({
	  source: projects
	});
  });
	  
	$('#clearFilters').on('click', function() {
		$('#cost_filters input').each(function() {
			$(this).val('');
		});
	});
  </script>