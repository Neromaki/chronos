<div class="page-header">
	<h1>Add Costs</h1>

	<a class="btn btn-default" href="<?php echo base_url('costs') ?>"><span class="glyphicon glyphicon-chevron-left"></span></a>	

</div>

<?php 				
if(validation_errors())
{	?>
	<div class="alert alert-danger">
		<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Hold up!</strong>:
		<?php echo validation_errors() ?>
	</div>
	<?php
}	?>

<div class="add-cost page-body">
		<?php		
		$cost_data = $this->session->userdata('cost_data');
		
		if(set_value('date1')) { $date1 = set_value('date1'); } elseif(isset($cost_data[1]['date'])) { $date1 = $cost_data[1]['date']; } else { $date1 = ""; }
		if(set_value('project1')) { $project1 = set_value('project1'); } elseif(isset($cost_data[1]['project'])) { $project1 = $cost_data[1]['project']; } else { $project1 = ""; }
		if(set_value('amount1')) { $amount1 = set_value('amount1'); } elseif(isset($cost_data[1]['amount'])) { $amount1 = $cost_data[1]['amount']; } else { $amount1 = ""; }
		if(set_value('reason1')) { $reason1 = set_value('reason1'); } elseif(isset($cost_data[1]['reason'])) { $reason1 = $cost_data[1]['reason']; } else { $reason1 = ""; }
		?>
		<form role="form" action="<?php echo base_url('cost/add')?>" method="post" data-type="cost">
			<div id="form-inner">
			
				<div class="row spacing-bottom-15">
					<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 <?php if(form_error('date1')) { echo 'has-error'; } ?> has-feedback">
						<input type="text" class="form-control date-pick" id="date1" name="date1" placeholder="Date" data-date-format="DD/MM/YYYY" value="<?php echo $date1 ?>" required />
						<span class="glyphicon glyphicon-calendar form-control-feedback feedback-fix"></span>
					</div>
				
					<div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 <?php if(form_error('project1')) { echo 'has-error'; } ?>">
						<input type="text" class="form-control project-pick" id="project1" name="project1" placeholder="Project" value="<?php echo $project1 ?>" required />
					</div>

					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2 <?php if(form_error('amount1')) { echo 'has-error'; } ?>">
						<div class="input-group">
							<span class="input-group-addon">&pound;</span>
							<input type="text" class="form-control" id="amount1" name="amount1" placeholder="Amount" value="<?php echo $amount1 ?>" required />
							<span class="input-group-addon">.00</span>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-11 col-md-9 col-lg-4 <?php if(form_error('reason1')) { echo 'has-error'; } ?>">
						<input type="text" class="form-control" id="reason1" name="reason1" placeholder="Reason" value="<?php echo $reason1 ?>" required />
					</div>
					
					<div class="col-sm-1 col-md-1 col-lg-1 hidden-xs">
						<button type="button" class="btn btn-default btn-sm addRow"><span class="glyphicon glyphicon-plus"></span></button>
					</div>
				</div>
			
				<?php				
				for($x=2; isset($cost_data[$x]['date']); $x++)
				{	
					if(set_value('date'.$x)) { $date = set_value('date'.$x); } elseif(isset($cost_data[$x]['date'])) { $date = $cost_data[$x]['date']; } else { $date = ""; }
					if(set_value('project'.$x)) { $project = set_value('project'.$x); } elseif(isset($cost_data[$x]['project'])) { $project = $cost_data[$x]['project']; } else { $project = ""; }
					if(set_value('amount'.$x)) { $amount = set_value('amount'.$x); } elseif(isset($cost_data[$x]['amount'])) { $amount = $cost_data[$x]['amount']; } else { $amount = ""; }
					if(set_value('reason'.$x)) { $reason = set_value('reason'.$x); } elseif(isset($cost_data[$x]['reason'])) { $reason = $cost_data[$x]['reason']; } else { $reason = ""; }
					?>
					<div class="row time-row spacing-bottom-15">
						<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 <?php if(form_error('date'.$x)) { echo 'has-error'; } ?> has-feedback">
							<input type="text" class="form-control date-pick" id="date<?php echo $x ?>" name="date<?php echo $x ?>" placeholder="Date" data-date-format="DD/MM/YYYY" value="<?php echo $date ?>" required />
							<span class="glyphicon glyphicon-calendar form-control-feedback feedback-fix"></span>
						</div>
					
						<div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 <?php if(form_error('project'.$x)) { echo 'has-error'; } ?>">
							<input type="text" class="form-control project-pick" id="project<?php echo $x ?>" name="project<?php echo $x ?>" placeholder="Project" value="<?php echo $project ?>" required />
						</div>

						<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2 <?php if(form_error('amount'.$x)) { echo 'has-error'; } ?>">
							<div class="input-group">
								<span class="input-group-addon">&pound;</span>
								<input type="text" class="form-control" id="amount<?php echo $x ?>" name="amount<?php echo $x ?>" placeholder="Amount" max="24" value="<?php echo $amount ?>" required />
								<span class="input-group-addon">.00</span>
							</div>
						</div>
						
						<div class="col-xs-12 col-sm-11 col-md-9 col-lg-4 <?php if(form_error('reason'.$x)) { echo 'has-error'; } ?>">
							<input type="text" class="form-control" id="reason<?php echo $x ?>" name="reason<?php echo $x ?>" placeholder="Reason" value="<?php echo $reason ?>" required />
						</div>
						
						<div class="col-sm-1 col-md-1 col-lg-1 hidden-xs">
							<button type="button" class="btn btn-default btn-sm removeRow"><span class="glyphicon glyphicon-minus"></span></button>
						</div>
					</div>
					<?php
				}	?>
				
			</div>

			<div class="row visible-xs row-controls-xs">
				
				<div class="col-xs-6">
					<button type="button" class="btn btn-info btn-sm btn-block addRow">
						<span class="glyphicon glyphicon-left glyphicon-plus"></span> Add row
					</button>
				</div>
				
				<div class="col-xs-6">
					<button type="button" class="btn btn-info btn-sm btn-block removeRowMobile">
						<span class="glyphicon glyphicon-left glyphicon-minus"></span> Remove row
					</button>
				</div>
				
			</div>
			
			<div class="row add-time-button">
				
				<div class="col-xs-8 col-sm-3 col-md-2 col-lg-2 col-xs-offset-2 col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
					<button type="submit" class="btn btn-primary btn-sm btn-block" name="add_day_cost" value="true">
						<span class="glyphicon glyphicon-left glyphicon-plus"></span> Add cost(s)
					</button>
				</div>
				
			</div>
				
		</form>

		
</div>

	<style>
	  .ui-autocomplete {
		max-height: 187px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
	  }
	</style>


  <script type="text/javascript">
	 $(function () {
		$('.date-pick').datetimepicker({
			pickTime: false,
			defaultDate: moment() 
		});
	});
  
  
  $(function() {
    var project_names = <?php echo json_encode($project_names) ?>;
    var project_descs = <?php echo json_encode($project_descs) ?>;
	
	setGlobalProjects(project_names);
	setglobalProjectDescs(project_descs);
	
    $( ".project-pick" ).autocomplete({
      source: project_names
    });
  });
  // When the project-pick field is defocused (blurred)
  $(document).on('blur', '.project-pick', function() {
  	// Check if the selected project name exists in the globalProjects objects
  	var project_index = $.inArray($(this).val(), globalProjects.projects);

  	// If it does (project_index will be > 0), check if it has a description set, and display it in a popover if so
  	if(project_index >= 0 && globalProjectDescs[project_index] != null) {
  		$(this).popover({trigger:'manual',title:'Project description',content:globalProjectDescs[project_index],placement:'top'});
  		$(this).popover('show');
  		// After X seconds, destroy the popover
  		window.setTimeout(function() {
  			$('.popover').remove();
  			$(this).popover('destroy');
  		}, 5000);
  	}
  });
  </script>
  
  
  <script type="text/javascript">
	
  </script>