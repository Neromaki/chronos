
    </div>
	
	<div class="footer">		
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
				<ul class="list-inline" style="text-align:center">
					<li>Chronos <a href="https://bitbucket.org/Neromaki/chronos/commits/all"><?php echo VERSION ?></a></li>
					<li>-</li>
					<li><a href="https://bitbucket.org/Neromaki/chronos/issues/new" title="Submit an issue">Submit a bug / issue</a></li>
					<li>-</li>
					<li>&copy; RPFF 2006 - <?php echo date('Y', time()) ?></li>
				</ul>
			</div>
		</div>
	</div>

	
	<script src="<?php echo base_url('js/min/jquery-ui-1.10.4.custom.min.js'); ?>" ></script>
	<script src="<?php echo base_url('js/min/moment.min.js'); ?>" ></script>
	<script src="<?php echo base_url('js/min/bootstrap-datetimepicker.min.js'); ?>" ></script>
	<script src="<?php echo base_url('js/min/bootstrap.min.js'); ?>" ></script>	
	<script src="<?php echo base_url('js/custom.js'); ?>" ></script>
	<script src="<?php echo base_url('js/min/spin.min.js'); ?>" ></script>
	<script src="<?php echo base_url('js/min/spin.jquery.min.js'); ?>" ></script>

	<script type="text/javascript">$('#by_period').tooltip();</script>
	
	</body>
</html>