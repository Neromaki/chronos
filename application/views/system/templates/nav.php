    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				
				<a href="<?php echo base_url() ?>" id="logo-link"><img class="img-responsive nav-logo" id="nav-logo" src="<?php echo base_url('img/logo/chronos.png') ?>" alt="chronos"></a>
			</div>
		<?php
		$page = $this->uri->segment(1);
		$user_type = $this->session->userdata('account_type');
		?>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <?php if(checkPermissions('management')) { ?><li <?php if($page == "projects") { echo "class='active'"; } ?>><a href="<?php echo base_url('projects') ?>"><span class="glyphicon glyphicon-left glyphicon-folder-open"></span> Projects</a></li><?php } ?>
			<?php if(checkPermissions('management')) { ?><li <?php if($page == "departments") { echo "class='active'"; } ?> id="nav-departments"><a href="<?php echo base_url('departments') ?>"><span class="glyphicon glyphicon-left glyphicon-briefcase"></span> Departments</a></li><?php } ?>
			<?php if(checkPermissions('management')) { ?><li <?php if($page == "users") { echo "class='active'"; } ?> id="nav-users"><a href="<?php echo base_url('users') ?>"><span class="glyphicon glyphicon-left glyphicon-user"></span> Users</a></li><?php } ?>
			<?php if(checkPermissions('all')) { ?><li <?php if($page == "time") { echo "class='active'"; } ?> id="nav-time"><a href="<?php echo base_url('time') ?>"><span class="glyphicon glyphicon-left glyphicon-time"></span> Time</a></li><?php } ?>
			<?php if(checkPermissions('all')) { ?><li <?php if($page == "costs") { echo "class='active'"; } ?> id="nav-costs"><a href="<?php echo base_url('costs') ?>"><span class="glyphicon glyphicon-left glyphicon-gbp"></span> Costs</a></li><?php } ?>
			<?php if(checkPermissions('management')) { ?><li <?php if($page == "logs") { echo "class='active'"; } ?> id="nav-logs"><a href="<?php echo base_url('logs') ?>"><span class="glyphicon glyphicon-left glyphicon-list"></span> Logs</a></li><?php } ?>
			<!--
            <li class="dropdown <?php if($page == "tasks") { echo "active"; } ?>">
              <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo base_url('tasks') ?>"><span class="glyphicon glyphicon-tasks"></span> Tasks <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
			-->
          </ul>
          <ul class="nav navbar-nav navbar-right">
		  <li><a class="navbar-brand hidden-xs">Hello <?php echo $this->session->userdata('username') ?>!</a></li>
            <li class="dropdown <?php if($page == "account" || $page == "settings") { echo "active"; } ?>">
				<a class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-left glyphicon-cog"></span> <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo base_url('account') ?>"><span class="glyphicon glyphicon-left glyphicon-user"></span> Account settings</a></li>
					<!--<li><a href="<?php echo base_url('settings') ?>"><span class="glyphicon glyphicon-left glyphicon-wrench"></span> Chronos settings</a></li>-->
					<li class="divider"></li>
					<li><a href="<?php echo base_url('about') ?>"><span class="glyphicon glyphicon-left glyphicon-info-sign"></span> About Chronos</a></li>
              </ul>
            </li>
            <li><a href="<?php echo base_url('logout') ?>">Logout <span class="glyphicon glyphicon-right glyphicon-log-out"></span></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
	
	<div class="box">
	<noscript>
		<style type="text/css">
			.js { display: none; }
		</style>
		
		<div class="alert alert-warning">
			<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Whoa there!</strong> You don't appear to have Javascript enabled! Whilst Chronos will still work for you,
			it'll operate in a more basic state, with some functionality disabled. We heartily recommend you <a href="http://enable-javascript.com/" target="_blank">enable Javascript</a> and reap the benefits!
		</div>
	</noscript>
	
	<?php 					
		if($this->session->flashdata('alert'))
		{ ?>
			<div class='alert alert-<?php echo $this->session->flashdata('type'); ?> spacing-top-5'>
				<?php echo $this->session->flashdata('message'); ?>
				<button type='button' class='close' data-dismiss='alert'>&times;</button>
			</div>
			<?php
		}	?>