 <!DOCTYPE html>
 <html> 
	<head>
		<title>Chronos Time Manager</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="icon" href="<?php echo base_url('img/favicon.ico?v=2') ?>" type="image/x-icon" /> 

		<link rel="shortcut icon" href="<?php echo base_url('img/favicon.ico'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/jquery-ui-flick/jquery-ui-1.10.4.custom.min.css'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/min/bootstrap-datetimepicker.min.css'); ?>" />
			<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/min/bootstrap.min.css'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/style.css'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/min/general.min.css'); ?>" />
		<link href='http://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>

		<script src="<?php echo base_url('js/min/jquery.min.js'); ?>" ></script>
	</head>	
	<body>
	
		<div class="container">