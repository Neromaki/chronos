<div class="page-header">
	<h1><span class="glyphicon glyphicon-exclamation-sign"></span> Aw snap, <strong>a 404 error!</strong></h1>
	<p class="lead">Sorry, that page couldn't be found!</p>
	<p>Are you sure you typed it correctly? Like, really sure? Wildly press any of the navigation buttons or handy links in the footer until you stumble across the page you're after.</p>
	<p>If you keep getting this pesky error and you're totally sure the page should exist, wave your arms frantically and summon Cthulu, the well-known tech-support guru.
</div>