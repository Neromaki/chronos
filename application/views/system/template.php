<?php
	//Header	
	$this->load->view("system/templates/header");
	
	//Nav
	$this->load->view("system/templates/nav");
	
	//Content
	if(isset($contentdata))
		$this->load->view("$contentlocation", $contentdata);
	else
		$this->load->view("$contentlocation");
	
	//Footer
	$this->load->view("system/templates/footer");
?>
<!--
	<div class="debug" style="position:absolute; bottom:0; right:0; width:40px; border-left:1px solid black; border-top: 1px solid black; text-align:center;">
		<span class="visible-xs">XS</span>
		<span class="visible-sm">SM</span>
		<span class="visible-md">MD</span>
		<span class="visible-lg">LG</span>
	</div>
-->