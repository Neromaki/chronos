<?php 
/** 
* Log Model Class 
* 
* @package		Chronos
* @author		Shaun Wall 
* @link			http://www.rpff.co.uk
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Log_model extends CI_Model 
{	
	const RECENT_LOGS_LIMIT = 50;
	
	/**	* Construct the class, and loads the database	*/
	public function __construct()
	{		
		$this->load->database();
	}
	
	
	
	public function getLogs($data = FALSE,
								$limit_from = 0,
								$limit_count = self::RECENT_LOGS_LIMIT)
	{
		$this->db->select("l.log_id, l.user_id,	u.username,	l.action_type, l.action_subject, l.action, l.time");
		$this->db->from(DB_PREFIX."Logs as l");
		$this->db->join(DB_PREFIX."Users as u", "l.user_id = u.user_id", "LEFT");
		
		
		if(isset($data['start']) && isset($data['end']))
			$this->db->where('date BETWEEN '.$this->db->escape($data['start']).' AND '.$this->db->escape($data['end']));
		elseif(isset($data['start']))
			$this->db->where('date >=', $data['start']);
		elseif(isset($data['end']))
			$this->db->where('date <=', $data['end']);
			
		if(isset($data['department']))
			$this->db->where('u.dept_id', $data['department']);
		
		if($limit_count)
			$this->db->limit($limit_count, $limit_from);
			
		$this->db->order_by('time DESC');
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
			return $query->result();
		else
			return FALSE;
	}
	
	
	public function addLog($user_id,
							$type,
							$subject,
							$action)
	{
		$this->db->insert(DB_PREFIX.'Logs', array('user_id' => $user_id,
													'action_type' => $type,
													'action_subject' => $subject,
													'action' => $action,
													'time' => date('Y-m-d H:i:s', time())
												));
												
		if($this->db->affected_rows() > 0)
			return true;
		else
			return false;
	}
	
	
	public function countLogs($data = FALSE)
	{
		$this->db->select('COUNT(*) as total')->from(DB_PREFIX.'Logs as l');
		$this->db->join(DB_PREFIX."Users as u", "l.user_id = u.user_id", "LEFT");
		
		if(isset($data['start']) && isset($data['end']))
			$this->db->where('date BETWEEN '.$this->db->escape($data['start']).' AND '.$this->db->escape($data['end']));
		elseif(isset($data['start']))
			$this->db->where('date >=', $data['start']);
		elseif(isset($data['end']))
			$this->db->where('date <=', $data['end']);
			
		if(isset($data['department']))
			$this->db->where('u.dept_id', $data['department']);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
			return $query->row()->total;
		else
			return FALSE;
	}
	
}

/* End of file log_model.php */
/* Location ./application/models/log_model.php */