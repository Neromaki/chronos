<?php 
/** 
* Project Model Class 
* 
* @package		Chronos
* @author		Shaun Wall 
* @link			http://www.rpff.co.uk
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project_model extends CI_Model 
{	

	/**	* Construct the class, and loads the database	*/
	public function __construct()
	{		
		$this->load->database();	
	}
	
	/**
	*
	*/
	public function getProjects($search_string = FALSE,
								$limit_from = 0,
								$limit_count = FALSE,
								$order = 'project_name',
								$type = NULL)
	{
		$query_string = "SELECT
							p.*,
							pt.totalTime,
							pc.totalCost,
							ct.lastLogged
							FROM ".DB_PREFIX."Projects as p
								LEFT JOIN (SELECT ct.project_id, SUM(ct.duration) as totalTime FROM ".DB_PREFIX."Time ct GROUP BY ct.project_id) as pt
									ON p.project_id = pt.project_id
								LEFT JOIN (SELECT cc.project_id, SUM(cc.amount) as totalCost FROM ".DB_PREFIX."Cost cc GROUP BY cc.project_id) as pc
									ON p.project_id = pc.project_id
								LEFT JOIN (SELECT ct.project_id, MAX(ct.`date`) as lastLogged FROM ".DB_PREFIX."Time ct GROUP BY ct.project_id) as ct
									ON p.project_id = ct.project_id";
		
		if($search_string || ($type != NULL))
		{
			$query_string .= " WHERE ";
			if($search_string) {
				$query_string .= "p.project_name LIKE '%$search_string%' ";
			}
			if($search_string && ($type != NULL)) {
				$query_string .= "AND ";
			}
			if(($type != NULL)) {
				$query_string .= "p.active = " . $type;
			}
		}
		
		$query_string .= " GROUP BY p.project_id";
			
		if($order)
			$query_string .= " ORDER BY $order " . ($order=='project_name'?'ASC':'DESC');
		
		if($limit_count)
		{
			$query_string .= " LIMIT ";
			if($limit_from)
				$query_string .= $limit_from . ", ";
			
			$query_string .= $limit_count;
		}
		
		$query = $this->db->query($query_string);
		
		if($query->num_rows() > 0)
			return $query->result();
		else
			return FALSE;
	}
	
	
	public function countProjects($search_string = FALSE, $type = NULL)
	{
		$query = $this->db->query("SELECT
									COUNT(*) as total
									FROM ".DB_PREFIX."Projects as p");
		$this->db->select('COUNT(*) as total')->from(DB_PREFIX.'Projects');
		
		if($search_string)
			$this->db->like('project_name', $search_string);
		if(($type != NULL))
			$this->db->where('active', $type);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
			return $query->row()->total;
		else
			return FALSE;
	}
	
	
	public function getProjectData($project_id)
	{
		$query_string = "SELECT
							p.*,
							pt.totalTime,
							pc.totalCost,
							ct.lastLogged
							FROM ".DB_PREFIX."Projects as p
								LEFT JOIN (SELECT ct.project_id, SUM(ct.duration) as totalTime FROM ".DB_PREFIX."Time ct GROUP BY ct.project_id) as pt
									ON p.project_id = pt.project_id
								LEFT JOIN (SELECT cc.project_id, SUM(cc.amount) as totalCost FROM ".DB_PREFIX."Cost cc GROUP BY cc.project_id) as pc
									ON p.project_id = pc.project_id
								LEFT JOIN (SELECT ct.project_id, MAX(ct.`date`) as lastLogged FROM ".DB_PREFIX."Time ct GROUP BY ct.project_id) as ct
									ON p.project_id = ct.project_id
							WHERE p.project_id = " . $project_id;
		$query = $this->db->query($query_string);
		
		if($query->num_rows > 0)
		{
			$data = $query->row();
			return $data;
		}
		else
			return FALSE;	// Project not found
	}
	
	
	public function getProjectLoggedTime($project_id, 
										$start_date, 
										$end_date)
	{
		// echo "ori start: $start_date <br> ori end: $end_date <br>";
		$start_date = $this->db->escape(date('Y-m-d', strtotime($start_date)));
		$end_date = $this->db->escape(date('Y-m-d', strtotime($end_date)));
		// echo "new start: $start_date <br> new end: $end_date <br>";
		
		if($start_date > $end_date)
		{
			$start_temp = $end_date;
			$end_date = $start_date;
			$start_date = $start_temp;
		}
		// echo "sort start: $start_date <br> sort end: $end_date <br><br>";
		
		$query_string = 
		"SELECT
		ct.user_id,
		ct.dept_id,
		cu.username as user_name,
		cd.dept_name,
		SUM(ct.duration) as duration
		FROM ".DB_PREFIX."Time ct
			INNER JOIN ".DB_PREFIX."Users cu
				ON ct.user_id = cu.user_id
			INNER JOIN ".DB_PREFIX."Departments cd
				ON ct.dept_id = cd.dept_id
		WHERE `date` BETWEEN $start_date AND $end_date
			AND project_id = " . $this->db->escape($project_id) . "
		GROUP BY user_id
		ORDER BY dept_id";
		
		$query = $this->db->query($query_string);
		// echo $this->db->last_query() . "<br>";
		
		if($query->num_rows > 0)
			return $query->result();
		else
			return FALSE;
	}


	public function getProjectDepartmentsTime($project_id, 
										$start_date = NULL, 
										$end_date = NULL)
	{
		$project_id = $this->db->escape($project_id);
		
		if($start_date || $end_date)
		{
			// echo "ori start: $start_date <br> ori end: $end_date <br>";
			$start_date = $this->db->escape(date('Y-m-d', strtotime($start_date)));
			$end_date = $this->db->escape(date('Y-m-d', strtotime($end_date)));
			//echo "new start: $start_date <br> new end: $end_date <br>";
				
			if($start_date > $end_date)
			{
				$start_temp = $end_date;
				$end_date = $start_date;
				$start_date = $start_temp;
			}
			
			$query_string = 
			"SELECT
			u.dept_id,
			u.dept_name,
			pt.totalTime,
			pc.totalCost,
			ct.lastLogged
			FROM ".DB_PREFIX."Departments as u
				LEFT JOIN (SELECT ct.dept_id, SUM(ct.duration) as totalTime FROM ".DB_PREFIX."Time ct WHERE `date` BETWEEN $start_date AND $end_date AND ct.project_id = $project_id GROUP BY ct.dept_id) as pt
					ON u.dept_id = pt.dept_id
				LEFT JOIN (SELECT cc.dept_id, SUM(cc.amount) as totalCost FROM ".DB_PREFIX."Cost cc WHERE `date` BETWEEN $start_date AND $end_date AND cc.project_id = $project_id GROUP BY cc.dept_id) as pc
					ON u.dept_id = pc.dept_id
				LEFT JOIN (SELECT ct.dept_id, MAX(ct.`date`) as lastLogged FROM ".DB_PREFIX."Time ct WHERE `date` BETWEEN $start_date AND $end_date AND ct.project_id = $project_id GROUP BY ct.dept_id) as ct
					ON u.dept_id = ct.dept_id
			GROUP BY u.dept_id";
		}
		else
		{
			$thisWeek = date('W');
			$lastWeek = $thisWeek-1;
			
			$thisWeekStart 	= date('Y-m-d', strtotime('midnight last monday'));
			$thisWeekEnd 	= date('Y-m-d', strtotime('23:59 this sunday'));
			$lastWeekStart	= date('Y-m-d', strtotime(date('Y')."-W$lastWeek-1"));
			$lastWeekEnd	= date('Y-m-d', strtotime(date('Y')."-W$lastWeek-7"));
			$thisMonthStart	= date('Y-m-d', strtotime('midnight first day of this month'));
			$thisMonthEnd	= date('Y-m-d', strtotime('23:59 last day of this month'));
			$lastMonthStart	= date('Y-m-d', strtotime('midnight first day of last month'));
			$lastMonthEnd	= date('Y-m-d', strtotime('23:59 last day of last month'));
			$totalStart		= '2000-01-01';
			$totalEnd		= date('Y-m-d', time());
			
			/*
			echo "DATES:<br>";
			echo "This week: $thisWeekStart -> $thisWeekEnd<br>";
			echo "Last week: $lastWeekStart -> $lastWeekEnd<br>";
			echo "This Month: $thisMonthStart -> $thisMonthEnd<br>";
			echo "Last Month: $lastMonthStart -> $lastMonthEnd<br>";
			echo "Total: $totalStart -> $totalEnd<br>";
			die();
			*/
			
			$query_string = 
			"SELECT
			u.dept_id,
			u.dept_name,
			thisWeekTime,
			thisWeekCost,
			lastWeekTime,
			lastWeekCost,
			thisMonthTime,
			thisMonthCost,
			lastMonthTime,
			lastMonthCost
			FROM ".DB_PREFIX."Departments as u
				LEFT JOIN (SELECT dept_id, project_id, SUM(duration) as thisWeekTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$thisWeekStart' AND '$thisWeekEnd' AND project_id = @project_id GROUP BY project_id, dept_id) as twt
					ON u.dept_id = twt.dept_id
				LEFT JOIN (SELECT dept_id, project_id, SUM(amount) as thisWeekCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$thisWeekStart' AND '$thisWeekEnd' AND project_id = @project_id GROUP BY project_id, dept_id) as twc
					ON u.dept_id = twc.dept_id
				LEFT JOIN (SELECT dept_id, project_id, SUM(duration) as lastWeekTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$lastWeekStart' AND '$lastWeekEnd' AND project_id = @project_id GROUP BY project_id, dept_id) as lwt
					ON u.dept_id = lwt.dept_id
				LEFT JOIN (SELECT dept_id, project_id, SUM(amount) as lastWeekCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$lastWeekStart' AND '$lastWeekEnd' AND project_id = @project_id GROUP BY project_id, dept_id) as lwc
					ON u.dept_id = lwc.dept_id
				LEFT JOIN (SELECT dept_id, project_id, SUM(duration) as thisMonthTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$thisMonthStart' AND '$thisMonthEnd' AND project_id = @project_id GROUP BY project_id, dept_id) as tmt
					ON u.dept_id = tmt.dept_id
				LEFT JOIN (SELECT dept_id, project_id, SUM(amount) as thisMonthCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$thisMonthStart' AND '$thisMonthEnd' AND project_id = @project_id GROUP BY project_id, dept_id) as tmc
					ON u.dept_id = tmc.dept_id
				LEFT JOIN (SELECT dept_id, project_id, SUM(duration) as lastMonthTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$lastMonthStart' AND '$lastMonthEnd' AND project_id = @project_id GROUP BY project_id, dept_id) as lmt
					ON u.dept_id = lmt.dept_id
				LEFT JOIN (SELECT dept_id, project_id, SUM(amount) as lastMonthCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$lastMonthStart' AND '$lastMonthEnd' AND project_id = @project_id GROUP BY project_id, dept_id) as lmc
					ON u.dept_id = lmc.dept_id
			WHERE 
			(twt.project_id = @project_id) OR (twc.project_id = @project_id) 
			OR (lwt.project_id = @project_id) OR (lwc.project_id = @project_id) 
			OR (tmt.project_id = @project_id) OR (tmc.project_id = @project_id)
			OR (lmt.project_id = @project_id) OR (lmc.project_id = @project_id)
			GROUP BY u.dept_id";
		}

		//exit($query_string);
		
		$this->db->query("SET @project_id = $project_id;");	
		$query = $this->db->query($query_string);
		
		if($query->num_rows > 0)
			return $query->result();
		else
			return FALSE;
	}


	public function getProjectDepartmentEmployees($project_id, 
												$dept_id, 
												$start_date = NULL, 
												$end_date = NULL)
	{
		$dept_id = $this->db->escape($dept_id);
		
		if($start_date || $end_date)
		{
			// echo "ori start: $start_date <br> ori end: $end_date <br>";
			$start_date = $this->db->escape(date('Y-m-d', strtotime($start_date)));
			$end_date = $this->db->escape(date('Y-m-d', strtotime($end_date)));
			//echo "new start: $start_date <br> new end: $end_date <br>";
				
			if($start_date > $end_date)
			{
				$start_temp = $end_date;
				$end_date = $start_date;
				$start_date = $start_temp;
			}
			
			$query_string = 
			"SELECT
			u.user_id,
			u.username as user_name,
			pt.totalTime,
			pc.totalCost,
			ct.lastLogged
			FROM ".DB_PREFIX."Users as u
				LEFT JOIN (SELECT ct.user_id, ct.dept_id, SUM(ct.duration) as totalTime FROM ".DB_PREFIX."Time ct WHERE `date` BETWEEN $start_date AND $end_date AND project_id = @project_id GROUP BY ct.user_id) as pt
					ON u.user_id = pt.user_id
				LEFT JOIN (SELECT cc.user_id, cc.dept_id, SUM(cc.amount) as totalCost FROM ".DB_PREFIX."Cost cc WHERE `date` BETWEEN $start_date AND $end_date AND project_id = @project_id GROUP BY cc.user_id) as pc
					ON u.user_id = pc.user_id
				LEFT JOIN (SELECT ct.user_id, MAX(ct.`date`) as lastLogged FROM ".DB_PREFIX."Time ct WHERE `date` BETWEEN $start_date AND $end_date AND project_id = @project_id GROUP BY ct.user_id) as ct
					ON u.user_id = ct.user_id
			WHERE ((pt.dept_id = $dept_id) OR (pc.dept_id = $dept_id) OR (u.dept_id = $dept_id))
			GROUP BY u.user_id";
		}
		else
		{
			$thisWeek = date('W');
			$lastWeek = $thisWeek-1;
			
			$thisWeekStart 	= date('Y-m-d', strtotime('midnight last monday'));
			$thisWeekEnd 	= date('Y-m-d', strtotime('23:59 this sunday'));
			$lastWeekStart	= date('Y-m-d', strtotime(date('Y')."-W$lastWeek-1"));
			$lastWeekEnd	= date('Y-m-d', strtotime(date('Y')."-W$lastWeek-7"));
			$thisMonthStart	= date('Y-m-d', strtotime('midnight first day of this month'));
			$thisMonthEnd	= date('Y-m-d', strtotime('23:59 last day of this month'));
			$lastMonthStart	= date('Y-m-d', strtotime('midnight first day of last month'));
			$lastMonthEnd	= date('Y-m-d', strtotime('23:59 last day of last month'));
			$totalStart		= '2000-01-01';
			$totalEnd		= date('Y-m-d', time());
			
			
			$query_string = 
			"SELECT
			u.user_id,
			u.username as user_name,
			u.account_type,
			thisWeekTime,
			thisWeekCost,
			lastWeekTime,
			lastWeekCost,
			thisMonthTime,
			thisMonthCost,
			lastMonthTime,
			lastMonthCost,
			totalTime,
			totalCost,
			lastLogged
			FROM ".DB_PREFIX."Users as u
				LEFT JOIN (SELECT user_id, dept_id, SUM(duration) as thisWeekTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$thisWeekStart' AND '$thisWeekEnd' AND dept_id = $dept_id AND project_id = @project_id GROUP BY user_id) as twt
					ON u.user_id = twt.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(amount) as thisWeekCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$thisWeekStart' AND '$thisWeekEnd' AND dept_id = $dept_id AND project_id = @project_id GROUP BY user_id) as twc
					ON u.user_id = twc.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(duration) as lastWeekTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$lastWeekStart' AND '$lastWeekEnd' AND dept_id = $dept_id AND project_id = @project_id GROUP BY user_id) as lwt
					ON u.user_id = lwt.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(amount) as lastWeekCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$lastWeekStart' AND '$lastWeekEnd' AND dept_id = $dept_id AND project_id = @project_id GROUP BY user_id) as lwc
					ON u.user_id = lwc.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(duration) as thisMonthTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$thisMonthStart' AND '$thisMonthEnd' AND dept_id = $dept_id AND project_id = @project_id GROUP BY user_id) as tmt
					ON u.user_id = tmt.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(amount) as thisMonthCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$thisMonthStart' AND '$thisMonthEnd' AND dept_id = $dept_id AND project_id = @project_id GROUP BY user_id) as tmc
					ON u.user_id = tmc.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(duration) as lastMonthTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$lastMonthStart' AND '$lastMonthEnd' AND dept_id = $dept_id AND project_id = @project_id GROUP BY user_id) as lmt
					ON u.user_id = lmt.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(amount) as lastMonthCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$lastMonthStart' AND '$lastMonthEnd' AND dept_id = $dept_id AND project_id = @project_id GROUP BY user_id) as lmc
					ON u.user_id = lmc.user_id
				LEFT JOIN (SELECT user_id, project_id, dept_id, SUM(duration) as totalTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$totalStart' AND '$totalEnd' AND dept_id = $dept_id AND project_id = @project_id GROUP BY user_id) as tt
					ON u.user_id = tt.user_id
				LEFT JOIN (SELECT user_id, project_id, dept_id, SUM(amount) as totalCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$totalStart' AND '$totalEnd' AND dept_id = $dept_id AND project_id = @project_id GROUP BY user_id) as tc
					ON u.user_id = tc.user_id
				LEFT JOIN (SELECT user_id, MAX(`date`) as lastLogged FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$totalStart' AND '$totalEnd' AND dept_id = $dept_id AND project_id = @project_id GROUP BY user_id) as ll
					ON u.user_id = ll.user_id
			WHERE ((tt.project_id = @project_id) OR (tc.project_id = @project_id) AND (tt.dept_id = $dept_id) OR (tc.dept_id = $dept_id))
			GROUP BY u.user_id";
		}

		$this->db->query("SET @project_id = $project_id;");
		$query = $this->db->query($query_string);
		
		if($query->num_rows > 0)
			return $query->result();
		else
			return FALSE;
	}


    public function getProjectStats($project_id, 
                                        $start_date, 
                                        $end_date)
    {
        // echo "ori start: $start_date <br> ori end: $end_date <br>";
        $start_date = $this->db->escape(date('Y-m-d', strtotime($start_date)));
        $end_date = $this->db->escape(date('Y-m-d', strtotime($end_date)));
        //echo "new start: $start_date <br> new end: $end_date <br>";
        
        
        if($start_date > $end_date)
        {
            $start_temp = $end_date;
            $end_date = $start_date;
            $start_date = $start_temp;
        }
         //echo "sort start: $start_date <br> sort end: $end_date <br><br>";
        
        $query_string = 
        "SELECT
        pt.totalTime,
        pc.totalCost
        FROM ".DB_PREFIX."Projects as p
            LEFT JOIN (SELECT ct.project_id, SUM(ct.duration) as totalTime FROM ".DB_PREFIX."Time ct WHERE `date` BETWEEN $start_date AND $end_date GROUP BY ct.project_id) as pt
                ON p.project_id = pt.project_id
            LEFT JOIN (SELECT cc.project_id, SUM(cc.amount) as totalCost FROM ".DB_PREFIX."Cost cc WHERE `date` BETWEEN $start_date AND $end_date GROUP BY cc.project_id) as pc
                ON p.project_id = pc.project_id
        WHERE p.project_id = " . $this->db->escape($project_id) . "
        GROUP BY p.project_id";
        
        $query = $this->db->query($query_string);
        
        if($query->num_rows > 0)
            return $query->row();
        else
            return FALSE;
    }

	
	
	/**
	*	Adds a project to the system
	*
	*	@param	string	$name			The name of the project (e.g. Super Awesome Project)
	*	@param	string	$description	The project description (e.g. The project of super awesomeness
	*/
	public function addProject($name,
								$description)
	{
		$this->db->insert(DB_PREFIX.'Projects', array('project_name' => $name,
													'description' => $description,
													'created' => date('Y-m-d', time())));
		$data['project_id'] = $this->db->insert_id();
		
		if($this->db->affected_rows() > 0) {
			$data['outcome'] = 1;	// Successfully added
		}
		else {
			$data['outcome'] = 2;	// Error adding
		}
		return $data;
	}
	
	
	
	
	public function updateProject($project_id,
									$name,
									$description)
	{
		$update_data = array('project_name' => $name,
							'description' => $description);
							
		$this->db->update(DB_PREFIX.'Projects', $update_data, array('project_id' => $project_id));

		if($this->db->affected_rows() > 0)
			return 1;	// Successfully deleted
		else
		{
			$query = $this->db->get_where(DB_PREFIX.'Projects', $update_data);
			if($query->num_rows > 0)
				return 1;
			else
				return 2;	// Error adding
		}
	}
	

	public function activateProject($project_id)
	{		
		//$this->db->delete(DB_PREFIX.'Projects', array('project_id' => $project_id));
		$this->db->update(DB_PREFIX.'Projects', array('active' => 1), array('project_id' => $project_id));
		
		if($this->db->affected_rows() > 0)
			return 1;	// Successfully deleted
		else
			return 2;	// Error delete
	}

	public function deactivateProject($project_id)
	{		
		//$this->db->delete(DB_PREFIX.'Projects', array('project_id' => $project_id));
		$this->db->update(DB_PREFIX.'Projects', array('active' => 0), array('project_id' => $project_id));
		
		if($this->db->affected_rows() > 0)
			return 1;	// Successfully deleted
		else
			return 2;	// Error delete
	}
	
	
	public function getProjectName($project_id)
	{
		$query = $this->db->get_where(DB_PREFIX.'Projects', array('project_id' => $project_id));
		
		if($query->num_rows() > 0)
			return $query->row()->project_name;
		else
			return FALSE;
	}
	
	
	public function getProjectByName($project_name)
	{
		$query = $this->db->get_where(DB_PREFIX.'Projects', array('project_name' => $project_name));
		
		if($query->num_rows() > 0)
			return $query->row()->project_id;
		else
			return FALSE;
	}
	
	
	public function getAllProjects()
	{
		$query = $this->db->get_where(DB_PREFIX.'Projects', array('active' => 1));
		
		if($query->num_rows() > 0)
			return $query->result();
		else
			return FALSE;
	}
	
	public function getAllProjectNames()
	{
		$query = $this->db->select('project_name, description')->from(DB_PREFIX.'Projects')->where('active = 1')->get();
		
		if($query->num_rows() > 0)
			return $query->result();
		else
			return FALSE;
	}
	
}

/* End of file project_model.php */
/* Location ./application/models/project_model.php */