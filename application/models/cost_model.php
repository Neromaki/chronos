<?php 
/** 
* Time Model Class 
* 
* @package		Chronos
* @author		Shaun Wall 
* @link			http://www.rpff.co.uk
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cost_model extends CI_Model 
{	

	/**	* Construct the class, and loads the database	*/
	public function __construct()
	{		
		$this->load->database();	
	}

	
	public function getUserCosts($data, $offset = FALSE, $limit = FALSE)
	{
		if(isset($data['date_start'])) {
			$data['date_start'] = formatDate($data['date_start']);
		}
		if(isset($data['date_end'])) {
			$data['date_end'] = formatDate($data['date_end']);
		}
	
	
		$this->db->select('*')->from(DB_PREFIX.'Cost')->join(DB_PREFIX.'Projects', DB_PREFIX.'Cost.project_id = '.DB_PREFIX.'Projects.project_id');
		$this->db->where(array('user_id' => $data['user_id']));
		
		if($offset !== FALSE && $limit !== FALSE)
			$this->db->limit($limit, $offset);
		
		if(isset($data['date_start']) && isset($data['date_end']))
			$this->db->where('date BETWEEN '.$this->db->escape($data['date_start']).' AND '.$this->db->escape($data['date_end']));
		elseif(isset($data['date_start']))
			$this->db->where('date >=', $data['date_start']);
		elseif(isset($data['date_end']))
			$this->db->where('date <=', $data['date_end']);
			
		if(isset($data['project']))
			$this->db->like('project_name', $data['project']);
		
		$this->db->order_by('date', 'DESC');
		
		$result = $this->db->get();
		
		if($result->num_rows > 0)
			return $result->result();
		else
			return FALSE;
	}
	
	public function countCosts($data)
	{
		if(isset($data['date_start'])) {
			$data['date_start'] = formatDate($data['date_start']);
		}
		if(isset($data['date_end'])) {
			$data['date_end'] = formatDate($data['date_end']);
		}
		
		$this->db->select('*')->from(DB_PREFIX.'Cost')->join(DB_PREFIX.'Projects', DB_PREFIX.'Cost.project_id = '.DB_PREFIX.'Projects.project_id');
		$this->db->where(array('user_id' => $data['user_id']));
		
		if(isset($data['date_start']) && isset($data['date_end']))
			$this->db->where('date BETWEEN '.$this->db->escape($data['date_start']).' AND '.$this->db->escape($data['date_end']));
		elseif(isset($data['date_start']))
			$this->db->where('date >=', $data['date_start']);
		elseif(isset($data['date_end']))
			$this->db->where('date <=', $data['date_end']);
			
		if(isset($data['project']))
			$this->db->like('project_name', $data['project']);
		
		$this->db->order_by('date', 'DESC');
		
		$result = $this->db->count_all_results();
		
		if($result > 0)
			return $result;
		else
			return FALSE;
	}
	
	
	public function addCost($data)
	{
		$insert_data = array(
			'project_id' => $data['project'],
			'user_id' => $data['user_id'],
			'dept_id' => $data['dept_id'],
			'date' => $data['date'],
			'amount' => $data['amount'],
			'reason' => $data['reason']
		);
		$this->db->insert(DB_PREFIX.'Cost', $insert_data);
		
		if($this->db->affected_rows() > 0)
			return $this->db->insert_id();
		else
			return FALSE;
	}
	
	public function getCostData($cost_id)
	{
		$this->db->select('*')->from(DB_PREFIX.'Cost')->join(DB_PREFIX.'Projects', DB_PREFIX.'Cost.project_id = '.DB_PREFIX.'Projects.project_id');
		$this->db->join(DB_PREFIX.'Users', DB_PREFIX.'Cost.user_id = '.DB_PREFIX.'Users.user_id');
		$this->db->where(array('cost_id' => $cost_id));
		
		$result = $this->db->get();
		
		if($result->num_rows > 0)
			return $result->row();
		else
			return FALSE;
	}
	

	
	public function updateCost($cost_id, $data)
	{
		$this->db->update(DB_PREFIX.'Cost', $data, array('cost_id' => $cost_id));
		
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;		
	}
	
	
	public function deleteCost($cost_id)
	{
		$this->db->where(array('cost_id' => $cost_id));
		$this->db->delete(DB_PREFIX.'Cost');
		return TRUE;
	}
}

/* End of file cost_model.php */
/* Location ./application/models/cost_model.php */