<?php 
/** 
* Site Model Class 
* 
* @package		Chronos
* @author		Shaun Wall 
* @link			http://www.rpff.co.uk
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_model extends CI_Model 
{	

	/**	* Construct the class, and loads the database	*/
	public function __construct()
	{		
		$this->load->database();	
	}
	
	
	public function checkUsername($username)
	{
		$query = $this->db->get_where(DB_PREFIX.'Users', array('username' => $username));
		
		if($query->num_rows > 0)
			return $query->row();
		else
			return FALSE;
	}
	
	
	public function insertSessionReference($session_id, $user_id)
	{
		$this->db->insert('chronos_SessionsReference', array('session_id' => $session_id,
													'user_id' => $user_id));
		
		return TRUE;
	}
	
	
	public function getAppSettings($user_id)
	{
		$query = $this->db->get_where('chronos_AppSettings', array('user_id' => $user_id));
		
		if($query->num_rows > 0)
			return $query->row();
		else
			return FALSE;
	}
	
	
	public function updateAppSettings($user_id, $settings)
	{
		try
		{
			$this->db->update('chronos_AppSettings', $settings, array('user_id' => $user_id));
		}
		catch (Exception $e)
		{
			return FALSE;
		}
			
		return TRUE;
	}
}

/* End of file site_model.php */
/* Location ./application/models/site_model.php */