<?php 
/** 
* Department Model Class 
* 
* @package		Chronos
* @author		Shaun Wall 
* @link			http://www.rpff.co.uk
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department_model extends CI_Model 
{	

	/**	* Construct the class, and loads the database	*/
	public function __construct()
	{		
		$this->load->database();	
	}
	
	
	public function addDepartment($name)
	{
		$this->db->insert(DB_PREFIX.'Departments', array('dept_name' => $name));
		$data['dept_id'] = $this->db->insert_id();
		
		if($this->db->affected_rows() > 0) {
			$data['outcome'] = 1;	// Successfully added
		}
		else {
			$data['outcome'] = 2;	// Error adding
		}
		return $data;
	}
	
	public function getDeptName($dept_id)
	{
		$query = $this->db->get_where(DB_PREFIX.'Departments', array('dept_id' => $dept_id));
		if($query->num_rows > 0)
			return $query->row()->dept_name;
		else
			return false;	
	}
	
	
	public function getDepartments($search,
									$limit_page,
									$limit_count,
									$basic = FALSE)
	{
		if($basic)
		{
			$query_string = "SELECT * FROM ".DB_PREFIX."Departments";
		}
		else
		{
			$query_string = "SELECT
								d.*,
								pt.totalTime,
								pc.totalCost,
								ct.lastLogged
								FROM ".DB_PREFIX."Departments as d
									LEFT JOIN (SELECT ct.dept_id, SUM(ct.duration) as totalTime FROM ".DB_PREFIX."Time ct GROUP BY ct.dept_id) as pt
										ON d.dept_id = pt.dept_id
									LEFT JOIN (SELECT cc.dept_id, SUM(cc.amount) as totalCost FROM ".DB_PREFIX."Cost cc GROUP BY cc.dept_id) as pc
										ON d.dept_id = pc.dept_id
									LEFT JOIN (SELECT ct.dept_id, MAX(ct.`date`) as lastLogged FROM ".DB_PREFIX."Time ct GROUP BY ct.dept_id) as ct
										ON d.dept_id = ct.dept_id";
			
			if($search)
				$query_string .= " WHERE d.dept_name LIKE '%$search%'";
				
			if(!$search)
				$query_string .= " WHERE d.active > 0";
			
			$query_string .= " GROUP BY d.dept_id";
			
			if($limit_count)
			{
				$query_string .= " LIMIT ";
				if($limit_page)
					$query_string .= $limit_page . ", ";
				
				$query_string .= $limit_count;
			}
		}
		
		$query = $this->db->query($query_string);
		
		if($query->num_rows() > 0)
			return $query->result();
		else
			return FALSE;
	}
	
	
	public function countDepartments($search_string = FALSE)
	{
		$query = $this->db->query("SELECT
									COUNT(*) as total
									FROM ".DB_PREFIX."Departments as d");
		$this->db->select('COUNT(*) as total')->from(DB_PREFIX.'Departments');
		
		if($search_string)
			$this->db->like('dept_name', $search_string);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
			return $query->row()->total;
		else
			return FALSE;
	}
	
	
	public function getDepartmentData($dept_id)
	{
		$query_string = "SELECT
							d.*,
							pt.totalTime,
							pc.totalCost,
							ct.lastLogged
							FROM ".DB_PREFIX."Departments as d
								LEFT JOIN (SELECT ct.dept_id, SUM(ct.duration) as totalTime FROM ".DB_PREFIX."Time ct GROUP BY ct.dept_id) as pt
									ON d.dept_id = pt.dept_id
								LEFT JOIN (SELECT cc.dept_id, SUM(cc.amount) as totalCost FROM ".DB_PREFIX."Cost cc GROUP BY cc.dept_id) as pc
									ON d.dept_id = pc.dept_id
								LEFT JOIN (SELECT ct.dept_id, MAX(ct.`date`) as lastLogged FROM ".DB_PREFIX."Time ct GROUP BY ct.dept_id) as ct
									ON d.dept_id = ct.dept_id
								WHERE d.dept_id = " . $this->db->escape($dept_id);
		$query = $this->db->query($query_string);
		// echo $this->db->last_query() . "<br>";
		
		if($query->num_rows > 0)
		{
			$data = $query->row();
			return $data;
		}
		else
			return FALSE;	// Project not found
	}
	
	
	public function getDepartmentLoggedTime($dept_id, 
										$start_date = NULL, 
										$end_date = NULL)
	{
		$dept_id = $this->db->escape($dept_id);
		
		if($start_date || $end_date)
		{
			$start_date = $this->db->escape(date('Y-m-d', strtotime($start_date)));
			$end_date = $this->db->escape(date('Y-m-d', strtotime($end_date)));
				
			if($start_date > $end_date)
			{
				$start_temp = $end_date;
				$end_date = $start_date;
				$start_date = $start_temp;
			}
			
			$query_string = 
			"SELECT
			u.user_id,
			u.username as user_name,
			u.account_type,
			pt.totalTime,
			pc.totalCost,
			ct.lastLogged
			FROM ".DB_PREFIX."Users as u
				LEFT JOIN (SELECT ct.user_id, SUM(ct.duration) as totalTime FROM ".DB_PREFIX."Time ct WHERE `date` BETWEEN $start_date AND $end_date AND ct.dept_id = $dept_id GROUP BY ct.user_id) as pt
					ON u.user_id = pt.user_id
				LEFT JOIN (SELECT cc.user_id, SUM(cc.amount) as totalCost FROM ".DB_PREFIX."Cost cc WHERE `date` BETWEEN $start_date AND $end_date AND cc.dept_id = $dept_id GROUP BY cc.user_id) as pc
					ON u.user_id = pc.user_id
				LEFT JOIN (SELECT ct.user_id, MAX(ct.`date`) as lastLogged FROM ".DB_PREFIX."Time ct WHERE `date` BETWEEN $start_date AND $end_date AND ct.dept_id = $dept_id GROUP BY ct.user_id) as ct
					ON u.user_id = ct.user_id
			WHERE u.dept_id = $dept_id
				AND u.account_type > 0
			GROUP BY u.user_id
			ORDER BY u.username";
		}
		else
		{
			$thisWeek = date('W');
			$lastWeek = $thisWeek-1;
			
			$thisWeekStart 	= date('Y-m-d', strtotime('midnight last monday'));
			$thisWeekEnd 	= date('Y-m-d', strtotime('23:59 this sunday'));
			$lastWeekStart	= date('Y-m-d', strtotime(date('Y')."-W$lastWeek-1"));
			$lastWeekEnd	= date('Y-m-d', strtotime(date('Y')."-W$lastWeek-7"));
			$thisMonthStart	= date('Y-m-d', strtotime('midnight first day of this month'));
			$thisMonthEnd	= date('Y-m-d', strtotime('23:59 last day of this month'));
			$lastMonthStart	= date('Y-m-d', strtotime('midnight first day of last month'));
			$lastMonthEnd	= date('Y-m-d', strtotime('23:59 last day of last month'));
			$totalStart		= '2000-01-01';
			$totalEnd		= date('Y-m-d', time());
			
			
			$query_string = 
			"SELECT
			u.user_id,
			u.username as user_name,
			u.account_type,
			thisWeekTime,
			thisWeekCost,
			lastWeekTime,
			lastWeekCost,
			thisMonthTime,
			thisMonthCost,
			lastMonthTime,
			lastMonthCost,
			totalTime,
			totalCost,
			lastLogged
			FROM ".DB_PREFIX."Users as u
				LEFT JOIN (SELECT user_id, dept_id, SUM(duration) as thisWeekTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$thisWeekStart' AND '$thisWeekEnd' AND dept_id = $dept_id GROUP BY user_id) as twt
					ON u.user_id = twt.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(amount) as thisWeekCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$thisWeekStart' AND '$thisWeekEnd' AND dept_id = $dept_id GROUP BY user_id) as twc
					ON u.user_id = twc.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(duration) as lastWeekTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$lastWeekStart' AND '$lastWeekEnd' AND dept_id = $dept_id GROUP BY user_id) as lwt
					ON u.user_id = lwt.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(amount) as lastWeekCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$lastWeekStart' AND '$lastWeekEnd' AND dept_id = $dept_id GROUP BY user_id) as lwc
					ON u.user_id = lwc.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(duration) as thisMonthTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$thisMonthStart' AND '$thisMonthEnd' AND dept_id = $dept_id GROUP BY user_id) as tmt
					ON u.user_id = tmt.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(amount) as thisMonthCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$thisMonthStart' AND '$thisMonthEnd' AND dept_id = $dept_id GROUP BY user_id) as tmc
					ON u.user_id = tmc.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(duration) as lastMonthTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$lastMonthStart' AND '$lastMonthEnd' AND dept_id = $dept_id GROUP BY user_id) as lmt
					ON u.user_id = lmt.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(amount) as lastMonthCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$lastMonthStart' AND '$lastMonthEnd' AND dept_id = $dept_id GROUP BY user_id) as lmc
					ON u.user_id = lmc.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(duration) as totalTime FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$totalStart' AND '$totalEnd' AND dept_id = $dept_id GROUP BY user_id) as tt
					ON u.user_id = tt.user_id
				LEFT JOIN (SELECT user_id, dept_id, SUM(amount) as totalCost FROM ".DB_PREFIX."Cost WHERE `date` BETWEEN '$totalStart' AND '$totalEnd' AND dept_id = $dept_id GROUP BY user_id) as tc
					ON u.user_id = tc.user_id
				LEFT JOIN (SELECT user_id, MAX(`date`) as lastLogged FROM ".DB_PREFIX."Time WHERE `date` BETWEEN '$totalStart' AND '$totalEnd' GROUP BY user_id) as ll
					ON u.user_id = ll.user_id
			WHERE u.dept_id = $dept_id
				AND u.account_type > 0
			GROUP BY u.user_id
			ORDER BY u.username";
		}
		
		$query = $this->db->query($query_string);
		
		if($query->num_rows > 0)
			return $query->result();
		else
			return FALSE;
	}
	
    public function getDepartmentStats($dept_id, 
                                        $start_date, 
                                        $end_date)
    {
        // echo "ori start: $start_date <br> ori end: $end_date <br>";
        $start_date = $this->db->escape(date('Y-m-d', strtotime($start_date)));
        $end_date = $this->db->escape(date('Y-m-d', strtotime($end_date)));
        //echo "new start: $start_date <br> new end: $end_date <br>";
        
        
        if($start_date > $end_date)
        {
            $start_temp = $end_date;
            $end_date = $start_date;
            $start_date = $start_temp;
        }
         //echo "sort start: $start_date <br> sort end: $end_date <br><br>";
        
        $query_string = 
        "SELECT
        pt.totalTime,
        pc.totalCost
        FROM ".DB_PREFIX."Departments as d
            LEFT JOIN (SELECT ct.dept_id, SUM(ct.duration) as totalTime FROM ".DB_PREFIX."Time ct WHERE `date` BETWEEN $start_date AND $end_date GROUP BY ct.dept_id) as pt
                ON d.dept_id = pt.dept_id
            LEFT JOIN (SELECT cc.dept_id, SUM(cc.amount) as totalCost FROM ".DB_PREFIX."Cost cc WHERE `date` BETWEEN $start_date AND $end_date GROUP BY cc.dept_id) as pc
                ON d.dept_id = pc.dept_id
        WHERE d.dept_id = " . $this->db->escape($dept_id) . "
        GROUP BY d.dept_id";
        
        $query = $this->db->query($query_string);
        
        if($query->num_rows > 0)
            return $query->row();
        else
            return FALSE;
    }


    public function getDeptRecentProjects($dept_id, 
			                                $start_date = false, 
			                                $end_date = false)
    {
    	if(!$start_date)
        	$start_date = $this->db->escape(date('Y-m-d', strtotime("00:01 30 days ago")));

        if(!$end_date)
        	$end_date = $this->db->escape(date('Y-m-d', time()));
        
        
        if($start_date > $end_date)
        {
            $start_temp = $end_date;
            $end_date = $start_date;
            $start_date = $start_temp;
        }
        
        $query_string = 
        "SELECT
		p.project_id,
		p.project_name,
		t.totalTime,
		c.totalCost
		FROM ".DB_PREFIX."Projects p
			LEFT JOIN (select project_id, sum(duration) as totalTime from ".DB_PREFIX."Time where dept_id = $dept_id AND date BETWEEN $start_date AND $end_date GROUP BY project_id) as t
				ON p.project_id = t.project_id
			LEFT JOIN (select project_id, sum(amount) as totalCost from ".DB_PREFIX."Cost where dept_id = $dept_id AND date BETWEEN $start_date AND $end_date GROUP BY project_id) as c
				ON p.project_id = c.project_id
		WHERE t.totalTime IS NOT NULL OR c.totalCost IS NOT NULL
		GROUP BY p.project_id";
        
        $query = $this->db->query($query_string);
        
        if($query->num_rows > 0)
            return $query->result();
        else
            return FALSE;
    }

	
	public function checkDepartmentExists($dept_id)
	{
		$query = $this->db->get_where(DB_PREFIX."Departments", array('dept_id' => $dept_id));
		
		if($query->num_rows > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	
	public function migrateUsers($dept_from, $dept_to)
	{
		$this->db->where('dept_id', $dept_from);
		$this->db->update(DB_PREFIX.'Users', array('dept_id' => $dept_to));
		
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	public function migrateTime($dept_from, $dept_to)
	{
		$this->db->where('dept_id', $dept_from);
		$this->db->update(DB_PREFIX.'Time', array('dept_id' => $dept_to));
		
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	public function migrateCosts($dept_from, $dept_to)
	{
		$this->db->where('dept_id', $dept_from);
		$this->db->update(DB_PREFIX.'Cost', array('dept_id' => $dept_to));
		
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	public function deactivate($dept_id)
	{
		$this->db->where('dept_id', $dept_id);
		$this->db->update(DB_PREFIX.'Departments', array('active' => 0));
		
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	
	public function updateDepartment($dept_id, $name)
	{
		$this->db->where('dept_id', $dept_id);
		$this->db->update(DB_PREFIX.'Departments', array('dept_name' => $name));
		
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	
	public function getDepartmentName($department_id)
	{
		$query = $this->db->get_where(DB_PREFIX.'Departments', array('dept_id' => $department_id));
		
		if($query->num_rows() > 0)
			return $query->row()->dept_name;
		else
			return FALSE;
	}
	
	
	public function getAllDepartments()
	{
		$query = $this->db->get(DB_PREFIX.'Departments');
		
		if($query->num_rows() > 0)
			return $query->result();
		else
			return FALSE;
	}
	
	
	public function canManageDepartment($user_id, $department_id)
	{
		$query = $this->db->get_where(DB_PREFIX.'Users', array('user_id' => $user_id, 'dept_id' => $department_id, 'account_type' => 2));
		
		if($query->num_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}

}

/* End of file department_model.php */
/* Location ./application/models/department_model.php */