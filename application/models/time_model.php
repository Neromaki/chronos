<?php 
/** 
* Time Model Class 
* 
* @package		Chronos
* @author		Shaun Wall 
* @link			http://www.rpff.co.uk
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Time_model extends CI_Model 
{	

	/**	* Construct the class, and loads the database	*/
	public function __construct()
	{		
		$this->load->database();	
	}

	
	public function getUserTime($data, $offset = FALSE, $limit = FALSE)
	{
		if(isset($data['date_start'])) {
			$data['date_start'] = formatDate($data['date_start']);
		}
		if(isset($data['date_end'])) {
			$data['date_end'] = formatDate($data['date_end']);
		}
	
	
		$this->db->select('*')->from(DB_PREFIX.'Time')->join(DB_PREFIX.'Projects', DB_PREFIX.'Time.project_id = '.DB_PREFIX.'Projects.project_id');
		$this->db->where(array('user_id' => $data['user_id']));
		
		if($offset !== FALSE && $limit !== FALSE)
			$this->db->limit($limit, $offset);
		
		if(isset($data['date_start']) && isset($data['date_end']))
			$this->db->where('date BETWEEN '.$this->db->escape($data['date_start']).' AND '.$this->db->escape($data['date_end']));
		elseif(isset($data['date_start']))
			$this->db->where('date >=', $data['date_start']);
		elseif(isset($data['date_end']))
			$this->db->where('date <=', $data['date_end']);
			
		if(isset($data['project']))
			$this->db->like('project_name', $data['project']);
		
		$this->db->order_by('date', 'DESC');
		
		$result = $this->db->get();
		
		if($result->num_rows > 0)
			return $result->result();
		else
			return FALSE;
	}
	
	public function countTime($data)
	{
		if(isset($data['date_start'])) {
			$data['date_start'] = formatDate($data['date_start']);
		}
		if(isset($data['date_end'])) {
			$data['date_end'] = formatDate($data['date_end']);
		}
		
		$this->db->select('*')->from(DB_PREFIX.'Time')->join(DB_PREFIX.'Projects', DB_PREFIX.'Time.project_id = '.DB_PREFIX.'Projects.project_id');
		$this->db->where(array('user_id' => $data['user_id']));
		
		if(isset($data['date_start']) && isset($data['date_end']))
			$this->db->where('date BETWEEN '.$this->db->escape($data['date_start']).' AND '.$this->db->escape($data['date_end']));
		elseif(isset($data['date_start']))
			$this->db->where('date >=', $data['date_start']);
		elseif(isset($data['date_end']))
			$this->db->where('date <=', $data['date_end']);
			
		if(isset($data['project']))
			$this->db->like('project_name', $data['project']);
		
		$this->db->order_by('date', 'DESC');
		
		$result = $this->db->count_all_results();
		
		if($result > 0)
			return $result;
		else
			return FALSE;
	}
	
	
	public function addTime($data)
	{
		$insert_data = array(
			'project_id' => $data['project'],
			'user_id' => $data['user_id'],
			'dept_id' => $data['dept_id'],
			'date' => $data['date'],
			'duration' => $data['duration']
		);
		$this->db->insert(DB_PREFIX.'Time', $insert_data);
		
		if($this->db->affected_rows() > 0)
			return $this->db->insert_id();
		else
			return FALSE;
	}
	
	public function getTimeData($time_id)
	{
		$this->db->select('*')->from(DB_PREFIX.'Time')->join(DB_PREFIX.'Projects', DB_PREFIX.'Time.project_id = '.DB_PREFIX.'Projects.project_id');
		$this->db->join(DB_PREFIX.'Users', DB_PREFIX.'Time.user_id = '.DB_PREFIX.'Users.user_id');
		$this->db->where(array('time_id' => $time_id));
		
		$result = $this->db->get();
		
		if($result->num_rows > 0)
			return $result->row();
		else
			return FALSE;
	}
	

	
	public function updateTime($time_id, $data)
	{
		$this->db->update(DB_PREFIX.'Time', $data, array('time_id' => $time_id));
		
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;		
	}
	
	
	public function deleteTime($time_id)
	{
		$this->db->where(array('time_id' => $time_id));
		$this->db->delete(DB_PREFIX.'Time');
		return TRUE;
	}
}

/* End of file time_model.php */
/* Location ./application/models/time_model.php */