<?php 
/** 
* User Model Class 
* 
* @package		Chronos
* @author		Shaun Wall 
* @link			http://www.rpff.co.uk
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model 
{	

	/**	* Construct the class, and loads the database	*/
	public function __construct()
	{		
		$this->load->database();	
	}
	

	/**
	*	User_model::getUsers()
	*	Gets all users based on a search string
	*
	*	@param 	string 	$search_string 		The search string being used
	*	@param 	int 	$limit_from 		OPTIONAL: Used by the pagination
	*	@param 	int 	$limit_count 		OPTIONAL: Used by the pagination
	*	@param 	int 	$order 		 		OPTIONAL: What result column to order by
	*	@return object 						The result object of found users, FALSE if none found
	*/	
	public function getUsers($search_string = FALSE,
								$limit_from = 0,
								$limit_count = FALSE,
								$order = 'username')
	{
		$query_string = "SELECT
							u.user_id,
							u.username,
							u.name,
							u.registered,
							d.dept_name
							FROM ".DB_PREFIX."Users as u
								LEFT JOIN ".DB_PREFIX."Departments as d
									ON u.dept_id = d.dept_id";
		if($search_string)
			$query_string .= " WHERE (u.username LIKE '%$search_string%') OR (u.name LIKE '%$search_string%')";
		else
			$query_string .= " WHERE u.account_type > 0";
			
		if($order)
			$query_string .= " ORDER BY $order ASC";
		
		if($limit_count)
		{
			$query_string .= " LIMIT ";
			if($limit_from)
				$query_string .= $limit_from . ", ";
			
			$query_string .= $limit_count;
		}
		
		$query = $this->db->query($query_string);
		
		if($query->num_rows() > 0)
			return $query->result();
		else
			return FALSE;
	}


	/**
	*	User_model::getUserStats()
	*	Gets a specified user's logged time and costs between two dates
	*
	*	@param 	int 	$user_id 		The user to get stats for
	*	@param 	string 	$start_date 	The start date in UK format (27/01/2014)
	*	@param 	string 	$end_date 		The end date in UK format (27/01/2014)
	*	@return object 					The result object of stats, FALSE if none found
	*/	
    public function getUserStats($user_id, 
	                                $start_date, 
	                                $end_date)
    {
        $start_date = $this->db->escape(date('Y-m-d', strtotime($start_date)));
        $end_date = $this->db->escape(date('Y-m-d', strtotime($end_date)));
        
        
        if($start_date > $end_date)
        {
            $start_temp = $end_date;
            $end_date = $start_date;
            $start_date = $start_temp;
        }
        
        $query_string = 
        "SELECT
        pt.totalTime,
        pc.totalCost
        FROM ".DB_PREFIX."Users as d
            LEFT JOIN (SELECT ct.user_id, SUM(ct.duration) as totalTime FROM ".DB_PREFIX."Time ct WHERE `date` BETWEEN $start_date AND $end_date GROUP BY ct.user_id) as pt
                ON d.user_id = pt.user_id
            LEFT JOIN (SELECT cc.user_id, SUM(cc.amount) as totalCost FROM ".DB_PREFIX."Cost cc WHERE `date` BETWEEN $start_date AND $end_date GROUP BY cc.user_id) as pc
                ON d.user_id = pc.user_id
        WHERE d.user_id = " . $this->db->escape($user_id) . "
        GROUP BY d.user_id";
        
        $query = $this->db->query($query_string);
        
        if($query->num_rows > 0)
            return $query->row();
        else
            return FALSE;
    }


	/**
	*	User_model::getUserRecentProjects()
	*	Gets a list of projects which a specified user has contributed to 
	*	(via time or costs) between two dates
	*
	*	@param 	int 	$user_id 		The user to get stats for
	*	@param 	string 	$start_date 	The start date in UK format (27/01/2014)
	*	@param 	string 	$end_date 		The end date in UK format (27/01/2014)
	*	@return object 					The result object of recent project, FALSE if none found
	*/	
    public function getUserRecentProjects($user_id, 
			                                $start_date = false, 
			                                $end_date = false)
    {
    	if(!$start_date)
        	$start_date = $this->db->escape(date('Y-m-d', strtotime("00:01 30 days ago")));

        if(!$end_date)
        	$end_date = $this->db->escape(date('Y-m-d', time()));
        
        
        if($start_date > $end_date)
        {
            $start_temp = $end_date;
            $end_date = $start_date;
            $start_date = $start_temp;
        }
        
        $query_string = 
        "SELECT
		p.project_id,
		p.project_name,
		t.totalTime,
		c.totalCost
		FROM ".DB_PREFIX."Projects p
			LEFT JOIN (select project_id, sum(duration) as totalTime from ".DB_PREFIX."Time where user_id = $user_id AND date BETWEEN $start_date AND $end_date GROUP BY project_id) as t
				ON p.project_id = t.project_id
			LEFT JOIN (select project_id, sum(amount) as totalCost from ".DB_PREFIX."Cost where user_id = $user_id AND date BETWEEN $start_date AND $end_date GROUP BY project_id) as c
				ON p.project_id = c.project_id
		WHERE t.totalTime IS NOT NULL OR c.totalCost IS NOT NULL
		GROUP BY p.project_id";
        
        $query = $this->db->query($query_string);
        
        if($query->num_rows > 0)
            return $query->result();
        else
            return FALSE;
    }

	
	/**
	*	User_model::addUser()
	*	Adds a user to the system
	*
	*	@param 	string 	$username 		The user's username
	*	@param 	string 	$password 		The user's password
	*	@param 	string 	$name 			The user's real name
	*	@param 	string 	$email 			The user's email address
	*	@param 	int 	$department 	ID of the department which the user will be a part of
	*	@param 	int 	$type 			The user's account type (Admin, Manager, Employee)
	*	@return bool 					Whether the user creation was successful
	*/
	public function addUser($username,
							$password,
							$name,
							$email,
							$department,
							$type)
	{		
		$this->db->insert(DB_PREFIX.'Users', array('username' => $username,
													'password' => $password,
													'name' => $name, 
													'email' => $email,
													'dept_id' => $department,
													'account_type' => $type,
													'registered' => date('YYYY-m-d', time())));
		$data['user_id'] = $this->db->insert_id();
		
		if($this->db->affected_rows() > 0)
			$data['outcome'] = 1;
		else
			$data['outcome'] = 2;
		
		return $data;
	}
	

	/**
	*	User_model::countUsers()
	*	Complementary to getUsers(), runs the same query and counts the number of rows returned
	*
	*	@param 	string 	$search_string	The search string to search with
	*	@return int 					The number of rows returned
	*/
	public function countUsers($search_string = FALSE)
	{
		$this->db->select('COUNT(*) as total')->from(DB_PREFIX.'Users');
		
		if($search_string)
		{
			$this->db->or_like('name', $search_string);
			$this->db->or_like('username', $search_string);
		}
		else {
			$this->db->where('account_type >', 0);
		}
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
			return $query->row()->total;
		else
			return FALSE;
	}


	/**
	*	User_model::getAccountInfo()
	*	Returns account data for the specific user 
	*
	*	@param 	int 	$user_id 		The user ID to get info for
	*	@return object 					Object of user data
	*/
	public function getAccountInfo($user_id)
	{
		$this->db->join(DB_PREFIX.'Departments d', 'u.dept_id = d.dept_id');
		$query = $this->db->get_where(DB_PREFIX.'Users u', array('user_id' => $user_id));
		
		if($query->num_rows > 0)
			return $query->row();
		else
			return FALSE;
	}
	
	
	/**
	*	User_model::getUsername()
	*	Returns the username for a specified user ID
	*
	*	@param 	int 	$user_id 		The user ID to get info for
	*	@return string 					The username, FALSE if user not found
	*/
	public function getUsername($user_id)
	{
		$query = $this->db->get_where(DB_PREFIX.'Users', array('user_id' => $user_id));
		
		if($query->num_rows() > 0)
			return $query->row()->username;
		else
			return FALSE;
	}
	
	
	/**
	*	User_model::updateAccountInfo()
	*	Updates a user's account details
	*
	*	@param 	int 	$user_id 		The user ID to update
	*	@param 	array 	$data 			Associative array (keys matching database columns) of data to update with
	*	@return bool 					Whether or not the user was successfully updated
	*/
	public function updateAccountInfo($user_id, $data)
	{
		$user_data = $this->db->get_where(DB_PREFIX.'Users', array('user_id' => $user_id))->row();

		// Checks if there are actually any changes in the account information
		$changes = TRUE;
		foreach($data as $key => $val) {
			if(property_exists($user_data, $key) && $user_data->$key != $val) {
				$changes = TRUE;
				break;
			} else {
				$changes = FALSE;
			}
		}

		if($changes)
		{
			$this->db->update(DB_PREFIX.'Users', $data, array('user_id' => $user_id));
			
			if($this->db->affected_rows() > 0)
				return TRUE;
			else
				return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	
	/**
	*	User_model::updatePassword()
	*	Updates a user's password
	*
	*	@param 	int 	$user_id 			The user ID to update
	*	@param 	string 	$newPassword 		The new password, already salted and hashed (see blowfish() in Users())
	*	@param 	string 	$currentPassword	OPTIONAL: The user's current password (admins / managers bypass this)
	*	@return bool 						Whether or not the user's password was successfully updated
	*/
	public function updatePassword($user_id, $newPassword, $currentPassword = FALSE)
	{
		$acc_type = $this->getAccountType($this->session->userdata('id'));
		
		if($acc_type == 1 || $acc_type == 2)
		{
			$can_update_pass = TRUE;
		}
		else
		{
			$this->db->select('user_id')->from(DB_PREFIX.'Users')->where('password', $currentPassword);
			$query = $this->db->get();
			
			
			if($query->num_rows > 0)
				$can_update_pass = TRUE;
			else
				$can_update_pass = FALSE;
		}
		

		if($can_update_pass)
		{
			$user_data = $this->db->get_where(DB_PREFIX.'Users', array('user_id' => $user_id))->row();

			if($user_data->password != $newPassword)
			{
				$this->db->update(DB_PREFIX.'Users', array('password' => $newPassword), array('user_id' => $user_id));
				if($this->db->affected_rows() > 0)
					return 1;
				else
					return 2;
			}
			else {
				return 1;
			}
		}
		else
			return 3;
	}


	/**
	*	User_model::checkUsernameExists()
	*	Checks whether a supplied username already exists on the system
	*
	*	@param 	string 	$username	The username to check
	*	@return bool 				Whether or not the username is available
	*/
	public function checkUsernameExists($username) {
		$query = $this->db->get_where(DB_PREFIX.'Users', array('username' => $username));
		if($query->num_rows > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	
	/**
	*	User_model::updateUser()
	*	Alias function for updateAccountInfo
	*
	*	@param 	int 	$user_id 		The user ID to update
	*	@param 	array 	$data 			Associative array (keys matching database columns) of data to update with
	*	@return bool 					Whether or not the user was successfully updated
	*/
	public function updateUser($user_id, $data)
	{
		return $this->updateAccountInfo($user_id, $data);
	}
	
	
	/**
	*	User_model::deactivateUser()
	*	Marks a particular user as deactivated
	*
	*	@param 	int 	$user_id 		The user ID to deactivate
	*	@return int 					Status code of action (1 = success, 2 = error)
	*/
	public function deactivateUser($user_id)
	{
		$this->db->update(DB_PREFIX.'Users', array('account_type' => 0), array('user_id' => $user_id));
		
		if($this->db->affected_rows() > 0)
			return 1;	// Successfully deleted
		else
			return 2;	// Error delete
	}
	
	
	/**
	*	User_model::getUserData()
	*	Similar to getUserStats(), but gets *all* time and costs, as well as when the last logged action was
	*
	*	@param 	int 	$user_id 		The user ID to get data for
	*	@return object 					Object of data
	*/
	public function getUserData($user_id)
	{
		$query_string = "SELECT
							ut.totalTime,
							uc.totalCost,
							ct.lastLogged
							FROM ".DB_PREFIX."Users as u
								LEFT JOIN (SELECT ct.user_id, SUM(ct.duration) as totalTime FROM ".DB_PREFIX."Time ct GROUP BY ct.user_id) as ut
									ON u.user_id = ut.user_id
								LEFT JOIN (SELECT cc.user_id, SUM(cc.amount) as totalCost FROM ".DB_PREFIX."Cost cc GROUP BY cc.user_id) as uc
									ON u.user_id = uc.user_id
								LEFT JOIN (SELECT ct.user_id, MAX(ct.`date`) as lastLogged FROM ".DB_PREFIX."Time ct GROUP BY ct.user_id) as ct
									ON u.user_id = ct.user_id
							WHERE u.user_id = " . $user_id;
		$query = $this->db->query($query_string);
		
		if($query->num_rows > 0)
		{
			$data = $query->row();
			return $data;
		}
		else
			return FALSE;	// User not found
	}
	
	
	/**
	*	User_model::getAccountType()
	*	Similar to getUserStats(), but gets *all* time and costs, as well as when the last logged action was
	*
	*	@param 	int 	$user_id 		The user ID to get data for
	*	@return object 					Object of data
	*/
	public function getAccountType($user_id)
	{
		$query = $this->db->get_where(DB_PREFIX.'Users', array('user_id' => $user_id));
		
		if($query->num_rows > 0)
			return $query->row()->account_type;
		else
			return FALSE;
	}
	
	
	/**
	*	User_model::canManageUser()
	*	Checks if $manager_id is in the same department as $user_id, thus able to update their accounts
	*
	*	@param 	int 	$manager_id		The user ID of the manager account
	*	@param 	int 	$user_id 		The user ID to check against
	*	@return bool 					TRUE if the manager can, FALSE if not
	*/
	public function canManageUser($manager_id, $user_id)
	{
		$this->db->select('m.user_id')->from(DB_PREFIX.'Users m')->join(DB_PREFIX.'Users e', 'm.dept_id = e.dept_id')->where(array('m.user_id' => $manager_id, 'e.user_id' => $user_id))->where('(m.account_type = 1 OR m.account_type = 2)');
		$query = $this->db->get();
		
		if($query->num_rows > 0)
			return TRUE;
		else
			return FALSE;
	}
	

	/**
	*	User_model::getUserDepartment()
	*	Gets the department ID of a specific user
	*
	*	@param 	int 	$user_id 		The user ID to check against
	*	@return int 					The ID of the department which the user is part of
	*/
	public function getUserDepartment($user_id)
	{
		$this->db->select('dept_id')->from(DB_PREFIX.'Users')->where(array('user_id' => $user_id));
		$query = $this->db->get();
		
		if($query->num_rows > 0)
			return $query->row()->dept_id;
		else
			return FALSE;
	}
	
}

/* End of file user_model.php */
/* Location ./application/models/user_model.php */