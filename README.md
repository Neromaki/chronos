# Chronos Time Manager #

### What? ###

* Chronos is  a lightweight, responsive time tracking and management tool for project-orientated companies.
* Built specifically for API Technologies, but can be used by anyone!
* Currently v1.0
