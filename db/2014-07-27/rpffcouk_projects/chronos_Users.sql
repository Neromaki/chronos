-- --------------------------------------------------------
-- Host:                         rpff.co.uk
-- Server version:               5.5.37-cll - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table rpffcouk_projects.chronos_Users
CREATE TABLE IF NOT EXISTS `chronos_Users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(120) NOT NULL,
  `password` varchar(40) NOT NULL,
  `name` varchar(120) NOT NULL,
  `email` varchar(120) DEFAULT NULL,
  `dept_id` int(11) NOT NULL,
  `registered` date NOT NULL,
  `account_type` int(5) NOT NULL DEFAULT '3' COMMENT '0 = Disabled, 1 = Admin, 2 = Dept Manager, 3 = Employee',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

-- Dumping data for table rpffcouk_projects.chronos_Users: ~49 rows (approximately)
/*!40000 ALTER TABLE `chronos_Users` DISABLE KEYS */;
INSERT IGNORE INTO `chronos_Users` (`user_id`, `username`, `password`, `name`, `email`, `dept_id`, `registered`, `account_type`) VALUES
	(1, 'Admin', '522c1ffeabed5e3935cfd02b3321c644ad94f99b', 'Administrator', 'admin@apitech.com', 14, '2014-03-26', 1),
	(7, 'BPearce', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'B Pearce', 'bpearce@apitech.com', 7, '2014-03-26', 3),
	(9, 'MRead', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'M Read', 'mread@apitech.com', 7, '2014-03-26', 3),
	(10, 'JAllen', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'J Allen', 'jallen@apitech.com', 7, '2014-03-26', 3),
	(11, 'MWhelband', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'M Whelband', 'mwhelband@apitech.com', 6, '2014-03-26', 3),
	(12, 'SButcher', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'S Butcher', 'sbutcher@apitech.com', 6, '2014-03-26', 3),
	(13, 'MHarvey', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'M Harvey', 'mharvey@apitech.com', 10, '2014-03-26', 3),
	(14, 'NHill', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'N Hill', 'nhill@apitech.com', 10, '2014-03-26', 3),
	(16, 'JBirrell', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'J Birrell', 'jbirrell@apitech.com', 4, '2014-03-26', 3),
	(17, 'NChadwick', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'N Chadwick', 'nchadwick@apitech.com', 4, '2014-03-26', 3),
	(18, 'CEmery', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'C Emery', 'cemery@apitech.com', 9, '2014-03-26', 3),
	(19, 'MBrookes', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'M Brookes', 'mbrookes@apitech.com', 9, '2014-03-26', 3),
	(21, 'CKnowles', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'C Knowles', 'cknowles@apitech.com', 7, '2014-03-26', 3),
	(22, 'DLawn', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'D Lawn', 'dlawn@apitech.com', 6, '2014-03-26', 3),
	(23, 'WYates', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'W Yates', 'wyates@apitech.com', 6, '2014-03-26', 3),
	(24, 'AHemp', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'A Hemp#', 'ahemp@apitech.com', 6, '2014-03-26', 3),
	(25, 'JCrossley', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'J Crossley', 'jcrossley@apitech.com', 7, '2014-03-26', 3),
	(27, 'MChilvers', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'M Chilvers', 'mchilvers@apitech.com', 10, '2014-03-26', 3),
	(28, 'DSayles', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'D Sayles', 'dsayles@apitech.com', 7, '2014-03-26', 3),
	(29, 'AFrosdick', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'A Frosdick', 'afrosdick@apitech.com', 7, '2014-03-26', 3),
	(30, 'SMoore', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'S Moore', 'smoore@apitech.com', 10, '2014-03-26', 3),
	(31, 'MHouston', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'M Houston', 'mhouston@apitech.com', 10, '2014-03-26', 3),
	(32, 'JBlagbrough', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'J Blagbrough', 'jblagbrough@apitech.com', 7, '2014-03-26', 3),
	(33, 'ATooke', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'A Tooke', 'atooke@apitech.com', 7, '2014-03-26', 3),
	(34, 'LSmethurst', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'L Smethurst', 'lsmethurst@apitech.com', 7, '2014-03-26', 3),
	(36, 'MHowchin', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'M Howchin', 'mhowchin@apitech.com', 1, '2014-03-26', 1),
	(38, 'DWhelband', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'D Whelband', 'dwhelband@apitech.com', 10, '2014-03-26', 3),
	(40, 'BJones', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'B Jones', 'bjones@apitech.com', 10, '2014-03-26', 3),
	(44, 'APowley', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'A Powley', 'apowley@apitech.com', 10, '2014-03-26', 3),
	(46, 'SWhymark', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'S Whymark', 'swhymark@apitech.com', 10, '2014-03-26', 3),
	(52, 'ABrookes', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Adrian Brooke', 'abrookes@apitech.com', 10, '2014-03-26', 2),
	(54, 'DLane', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'D Lane', 'dlane@apitech.com', 7, '2014-03-26', 3),
	(55, 'DThrower', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'D Thrower', 'dthrower@apitech.com', 9, '2014-03-26', 3),
	(56, 'WChiang', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'W Chaing', 'wchiang@apitech.com', 7, '2014-03-26', 3),
	(57, 'MRunicles', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'M Runicles', 'mrunicles@apitech.com', 7, '2014-03-26', 3),
	(61, 'CWoodend', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'C Woodend', 'cwoodend@apitech.com', 7, '2014-03-26', 3),
	(62, 'EngAdmin', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Eng Admin', 'engadmin@apitech.com', 1, '2014-03-26', 1),
	(72, 'MNewton', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'M Newton', 'mnewton@apitech.com', 10, '2014-03-26', 3),
	(73, 'DBolton', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'D Bolton', 'dbolton@apitech.com', 7, '2014-03-26', 3),
	(74, 'APinnock', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'A Pinnock', 'apinnock@apitech.com', 10, '2014-03-26', 3),
	(75, 'DDewis', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'D Dewis', 'ddewis@apitech.com', 10, '2014-03-26', 3),
	(76, 'RWright', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'R Wright', 'rwright@apitech.com', 7, '2014-03-26', 3),
	(77, 'test', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Testa', 'test@apitech.com', 1, '2014-03-26', 3),
	(78, 'SGrundy', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'S Grundy', 'sgrundy@apitech.com', 10, '2014-03-26', 3),
	(79, 'TestCalibrator', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Test Calib', 'testcalibrator@apitech.com', 11, '2014-03-26', 3),
	(80, 'TestUser2', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Test User', 'testuser2@apitech.com', 1, '2014-03-26', 3),
	(81, 'TestUser3', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Tester', 'testuser3@apitech.com', 1, '2014-03-26', 3),
	(82, 'Manager', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Test Manager', '', 1, '0000-00-00', 2),
	(83, 'Employee', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Test Employee', '', 1, '0000-00-00', 3);
/*!40000 ALTER TABLE `chronos_Users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
