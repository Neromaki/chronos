-- --------------------------------------------------------
-- Host:                         rpff.co.uk
-- Server version:               5.5.37-cll - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table rpffcouk_projects.chronos_Logs
CREATE TABLE IF NOT EXISTS `chronos_Logs` (
  `log_id` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'Unique log ID',
  `user_id` int(11) DEFAULT NULL COMMENT 'ID of the user concerned',
  `action_type` enum('add','update','delete','log','error') NOT NULL DEFAULT 'error' COMMENT 'Type of action (add, edit, delete)',
  `action_subject` enum('admin','project','department','user','time','cost','error') NOT NULL DEFAULT 'error' COMMENT 'Subject of action (project, user, time)',
  `action` varchar(255) NOT NULL COMMENT 'The action carried out',
  `time` datetime NOT NULL COMMENT 'Time and date it occured',
  PRIMARY KEY (`log_id`),
  KEY `FK_chronos_Log_chronos_Users` (`user_id`),
  KEY `action_type_action_subject` (`action_type`,`action_subject`),
  CONSTRAINT `FK_chronos_Log_chronos_Users` FOREIGN KEY (`user_id`) REFERENCES `chronos_Users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

-- Dumping data for table rpffcouk_projects.chronos_Logs: ~52 rows (approximately)
/*!40000 ALTER TABLE `chronos_Logs` DISABLE KEYS */;
INSERT IGNORE INTO `chronos_Logs` (`log_id`, `user_id`, `action_type`, `action_subject`, `action`, `time`) VALUES
	(1, 1, 'add', 'project', 'Added project \'Super Awesome\'', '2014-03-25 23:33:00'),
	(2, 1, 'add', 'user', 'Created new user Adminjsjs (Jeufi), adding them to department 1', '2014-03-25 22:46:43'),
	(3, 1, 'add', 'user', 'Created new user Testera (Tester), adding them to ', '2014-05-19 22:58:01'),
	(4, 1, 'add', 'user', 'Created new user Admindddd (sssaa), adding them to Engineering', '2014-05-19 23:01:28'),
	(5, 1, 'add', 'project', 'Created new project <a href=\'http://localhost/chronos/projects/view/388\'>Test Project MKII</a>', '2014-05-21 20:52:47'),
	(6, 1, 'update', 'project', 'Updated project <a href=\'http://localhost/chronos/projects/view/388\'>Test Project MK2</a>', '2014-05-21 20:56:51'),
	(7, 1, 'delete', 'project', 'Deleted project <a href=\'http://localhost/chronos/projects/view/388\'></a>', '2014-05-21 20:58:15'),
	(8, 1, 'add', 'project', 'Created new project <a href=\'http://localhost/chronos/projects/view/389\'>Test Project MKIII</a>', '2014-05-21 21:02:52'),
	(9, 1, 'delete', 'project', 'Deleted project <a href=\'http://localhost/chronos/projects/view/389\'>Test Project MKIII</a>', '2014-05-21 21:03:20'),
	(10, 1, 'add', 'department', 'Added department <a href=\'http://localhost/chronos/departments/view/13\'>Test Dept</a>', '2014-05-21 21:10:12'),
	(11, 1, 'update', 'department', 'Updated department <a href=\'http://localhost/chronos/departments/view/13\'>Test Dept</a>', '2014-05-21 21:21:15'),
	(12, 1, 'delete', 'department', 'Deleted department <a href=\'http://localhost/chronos/departments/view/13\'>Test Dept</a>. Deleted all users, time and costs.', '2014-05-21 21:21:54'),
	(13, 1, 'add', 'user', 'Created new user TestUser2 (Test User), adding them to Engineering', '2014-05-21 22:15:33'),
	(14, 1, 'add', 'user', 'Created new user <a href=\'http://localhost/chronos/users/view/81\'>TestUser3</a>, added them to <a href=\'http://localhost/chronos/departments/view/1\'>Engineering</a>', '2014-05-21 22:20:28'),
	(15, 1, 'update', 'user', 'Updated user <a href=\'http://localhost/chronos/users/view/52\'>ABrookes</a> ()', '2014-05-24 11:13:37'),
	(16, 1, 'update', 'user', 'Updated user <a href=\'http://localhost/chronos/users/view/52\'>ABrookes</a> ()', '2014-05-24 11:17:49'),
	(17, 1, 'update', 'user', 'Updated user <a href=\'http://localhost/chronos/users/view/52\'>ABrookes</a> ()', '2014-05-24 11:17:59'),
	(18, 1, 'update', 'user', 'Updated user <a href=\'http://localhost/chronos/users/view\'>ABrookes</a> ()', '2014-05-24 11:27:31'),
	(19, 1, 'update', 'user', 'Updated user <a href=\'http://localhost/chronos/users/view\'>ABrookes</a> ()', '2014-05-24 11:29:42'),
	(20, 1, 'update', 'project', 'Updated project <a href=\'http://localhost/chronos/projects/view/365\'>Process:  Assembly</a>', '2014-05-24 11:41:13'),
	(21, 1, 'update', 'project', 'Updated project <a href=\'http://localhost/chronos/projects/view/365\'></a>', '2014-05-24 15:58:39'),
	(22, 1, 'update', 'project', 'Updated project <a href=\'http://localhost/chronos/projects/view/365\'>Huh.</a>', '2014-05-24 15:58:52'),
	(23, 1, 'delete', 'time', 'Deleted time(2014-03-25, <a href=\'http://localhost/chronos/projects/view/242\'>9313 - Flextronics - NT3K30AAE5</a>, 0.50 hours)', '2014-06-16 20:00:56'),
	(24, 1, 'update', 'time', 'Updated time (2014-06-12, <a href=\'http://localhost/chronos/projects/view/5\'>16000 - RF2M - OPTO-Fire</a>, 1.25 hours)', '2014-06-21 19:12:41'),
	(25, 1, 'update', 'time', 'Updated time (2014-06-12, <a href=\'http://localhost/chronos/projects/view/5\'>16000 - RF2M - OPTO-Fire</a>, 1.30 hours)', '2014-06-21 19:12:58'),
	(26, 1, 'update', 'time', 'Updated time (2014-06-12, <a href=\'http://localhost/chronos/projects/view/5\'>16000 - RF2M - OPTO-Fire</a>, 1.25 hours)', '2014-06-21 20:03:27'),
	(27, 1, 'update', 'time', 'Updated time (2014-06-12, <a href=\'http://localhost/chronos/projects/view/5\'>16000 - RF2M - OPTO-Fire</a>, 1.25 hours)', '2014-06-21 20:03:42'),
	(28, 1, 'update', 'time', 'Updated time (<a href=\'http://localhost/chronos/users/view/1\'>Admin</a>, 2014-06-12, <a href=\'http://localhost/chronos/projects/view/5\'>16000 - RF2M - OPTO-Fire</a>, 1.35 hours)', '2014-06-21 20:07:54'),
	(29, 1, 'add', 'user', 'Created new user <a href=\'http://localhost/chronos/users/view/82\'>Manager</a>, added them to <a href=\'http://localhost/chronos/departments/view/1\'>Engineering</a>', '2014-06-24 22:18:02'),
	(30, 1, 'add', 'user', 'Created new user <a href=\'http://localhost/chronos/users/view/83\'>Employee</a>, added them to <a href=\'http://localhost/chronos/departments/view/1\'>Engineering</a>', '2014-06-24 22:18:33'),
	(31, 1, 'update', 'user', 'Deactivated user <a href=\'http://localhost/chronos/users/view/82\'></a>', '2014-06-26 21:12:38'),
	(32, 1, 'update', 'user', 'Deactivated user <a href=\'http://localhost/chronos/users/view/82\'></a>', '2014-06-26 21:15:55'),
	(33, 1, 'update', 'user', 'Deactivated user <a href=\'http://localhost/chronos/users/view/82\'></a>', '2014-06-26 21:18:57'),
	(34, 1, 'update', 'cost', 'Updated cost (<a href=\'http://localhost/chronos/users/view/1\'>Admin</a>, 2014-06-23, <a href=\'http://localhost/chronos/projects/view/88\'>15297 - Mier - Do Con</a>, 370 hours)', '2014-07-05 22:31:30'),
	(35, 1, 'update', 'cost', 'Updated cost (<a href=\'http://localhost/chronos/users/view/1\'>Admin</a>, 2014-06-23, <a href=\'http://localhost/chronos/projects/view/88\'>15297 - Mier - Do Con</a>, 370 hours)', '2014-07-05 22:32:48'),
	(36, 1, 'update', 'cost', 'Updated cost (<a href=\'http://localhost/chronos/users/view/1\'>Admin</a>, 2014-06-23, <a href=\'http://localhost/chronos/projects/view/88\'>15297 - Mier - Do Con</a>, 360 hours)', '2014-07-05 22:36:05'),
	(37, 1, 'update', 'cost', 'Updated cost (<a href=\'http://localhost/chronos/users/view/1\'>Admin</a>, 2014-06-23, <a href=\'http://localhost/chronos/projects/view/88\'>15297 - Mier - Do Con</a>, 370 hours)', '2014-07-05 22:36:17'),
	(38, 1, 'update', 'cost', 'Updated cost (<a href=\'http://localhost/chronos/users/view/1\'>Admin</a>, 2014-06-23, <a href=\'http://localhost/chronos/projects/view/88\'>15297 - Mier - Do Con</a>, 370 hours)', '2014-07-05 22:36:31'),
	(39, 1, 'update', 'cost', 'Updated cost (<a href=\'http://localhost/chronos/users/view/1\'>Admin</a>, 2014-07-06, <a href=\'http://localhost/chronos/projects/view/5\'>16000 - RF2M - OPTO-Fire</a>, 185 hours)', '2014-07-06 09:19:13'),
	(40, 1, 'delete', 'cost', 'Deleted cost (2014-07-06, <a href=\'http://localhost/chronos/projects/view/5\'>16000 - RF2M - OPTO-Fire</a>, 185.00 hours)', '2014-07-06 09:20:43'),
	(41, 83, 'delete', 'cost', 'Deleted cost (2014-07-04, <a href=\'http://localhost/chronos/projects/view/17\'>14688 - Ultra CCS - IF1</a>, 25.00 hours)', '2014-07-07 23:18:07'),
	(42, 82, 'add', 'cost', 'Added cost (<a href=\'http://localhost/chronos/users/view/82\'>Manager</a>, , <a href=\'http://localhost/chronos/projects/view\'></a>, &pound;10, \'cor\')', '2014-07-07 23:26:54'),
	(43, 82, 'add', 'cost', 'Added cost (2014-07-07, <a href=\'http://localhost/chronos/users/view/82\'>Manager</a>, , <a href=\'http://localhost/chronos/projects/view/269\'>14907 - Cassidian - NED Hybrid 14FP</a>, &pound;15, \'Another test one eh\')', '2014-07-07 23:33:31'),
	(44, 82, 'update', 'time', 'Updated time (<a href=\'http://localhost/chronos/users/view/82\'>Manager</a>, 2014-07-24, <a href=\'http://localhost/chronos/projects/view/138\'>12339 - RF2M - IDM3838-05-PA5</a>, 2 hours)', '2014-07-24 21:33:52'),
	(45, 82, 'delete', 'time', 'Deleted time (2014-07-24, <a href=\'http://localhost/chronos/projects/view/331\'>COSTINGS - GENERAL</a>, 1.75 hours)', '2014-07-24 21:33:59'),
	(46, 1, 'delete', 'time', 'Deleted time (2014-07-26, <a href=\'http://localhost/chronos/projects/view/5\'>16000 - RF2M - OPTO-Fire</a>, 1.00 hours)', '2014-07-26 21:43:39'),
	(47, 1, 'delete', 'time', 'Deleted time (2014-07-26, <a href=\'http://localhost/chronos/projects/view/5\'>16000 - RF2M - OPTO-Fire</a>, 1.00 hours)', '2014-07-26 21:43:46'),
	(48, 1, 'delete', 'time', 'Deleted time (2014-07-26, <a href=\'http://localhost/chronos/projects/view/5\'>16000 - RF2M - OPTO-Fire</a>, 1.00 hours)', '2014-07-26 21:43:51'),
	(49, 1, 'delete', 'time', 'Deleted time (2014-07-25, <a href=\'http://localhost/chronos/projects/view/5\'>16000 - RF2M - OPTO-Fire</a>, 1.00 hours)', '2014-07-26 21:43:56'),
	(50, 1, 'delete', 'time', 'Deleted time (2014-07-26, <a href=\'http://localhost/chronos/projects/view/297\'>15365 - RF2M - High Temp. Development</a>, 3.00 hours)', '2014-07-26 21:45:45'),
	(51, 1, 'add', 'cost', 'Added cost (2014-07-26, <a href=\'http://localhost/chronos/users/view/1\'>Admin</a>, , <a href=\'http://localhost/chronos/projects/view/192\'>8306 - Astrium - E0002327</a>, &pound;2, \'Some decent reason\')', '2014-07-26 21:47:57'),
	(52, 1, 'add', 'cost', 'Added cost (2014-07-26, <a href=\'http://localhost/chronos/users/view/1\'>Admin</a>, , <a href=\'http://localhost/chronos/projects/view/238\'>15313 - Cassidian - NED Hybrid 14SSW</a>, &pound;33, \'Awesome reason\')', '2014-07-26 21:48:27'),
	(53, 1, 'add', 'project', 'Created new project <a href=\'http://localhost/chronos/projects/view/388\'>Test Project Alpha</a>', '2014-07-27 21:12:23');
/*!40000 ALTER TABLE `chronos_Logs` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
