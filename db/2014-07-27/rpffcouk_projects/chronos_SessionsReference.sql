-- --------------------------------------------------------
-- Host:                         rpff.co.uk
-- Server version:               5.5.37-cll - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table rpffcouk_projects.chronos_SessionsReference
CREATE TABLE IF NOT EXISTS `chronos_SessionsReference` (
  `session_id` varchar(32) NOT NULL COMMENT 'The unique session ID',
  `user_id` int(10) NOT NULL COMMENT 'The user''s ID'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='Contains the reference session data of users logging in';

-- Dumping data for table rpffcouk_projects.chronos_SessionsReference: 9 rows
/*!40000 ALTER TABLE `chronos_SessionsReference` DISABLE KEYS */;
INSERT IGNORE INTO `chronos_SessionsReference` (`session_id`, `user_id`) VALUES
	('77736c4e8d11e33649b609ead46ff243', 1),
	('tn65qajphis65qobshsdmomvm4', 1),
	('ce9fe7eba2ae472238ee6ad738811c95', 33);
/*!40000 ALTER TABLE `chronos_SessionsReference` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
