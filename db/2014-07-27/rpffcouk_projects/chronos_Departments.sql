-- --------------------------------------------------------
-- Host:                         rpff.co.uk
-- Server version:               5.5.37-cll - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table rpffcouk_projects.chronos_Departments
CREATE TABLE IF NOT EXISTS `chronos_Departments` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(200) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table rpffcouk_projects.chronos_Departments: ~10 rows (approximately)
/*!40000 ALTER TABLE `chronos_Departments` DISABLE KEYS */;
INSERT IGNORE INTO `chronos_Departments` (`dept_id`, `dept_name`, `active`) VALUES
	(1, 'Engineering', 1),
	(4, 'CAD', 1),
	(6, 'Quality', 1),
	(7, 'PDG', 1),
	(9, 'Admin', 1),
	(10, 'PRG', 1),
	(11, 'Test House', 1),
	(12, 'Calibration', 0),
	(13, 'Test Dept', 0),
	(14, 'Administration', 1);
/*!40000 ALTER TABLE `chronos_Departments` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
