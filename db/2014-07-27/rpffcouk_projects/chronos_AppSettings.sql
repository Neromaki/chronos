-- --------------------------------------------------------
-- Host:                         rpff.co.uk
-- Server version:               5.5.37-cll - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table rpffcouk_projects.chronos_AppSettings
CREATE TABLE IF NOT EXISTS `chronos_AppSettings` (
  `user_id` int(10) NOT NULL COMMENT 'User''s ID',
  `projectsPerPage` tinyint(3) NOT NULL DEFAULT '15' COMMENT 'The number of projects to display per page',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `chronos_AppSettings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `unity_Users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='Contains user-specific settings for Unity';

-- Dumping data for table rpffcouk_projects.chronos_AppSettings: ~1 rows (approximately)
/*!40000 ALTER TABLE `chronos_AppSettings` DISABLE KEYS */;
INSERT IGNORE INTO `chronos_AppSettings` (`user_id`, `projectsPerPage`) VALUES
	(1, 15);
/*!40000 ALTER TABLE `chronos_AppSettings` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
