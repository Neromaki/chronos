-- --------------------------------------------------------
-- Host:                         rpff.co.uk
-- Server version:               5.5.37-cll - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table rpffcouk_projects.chronos_Cost
CREATE TABLE IF NOT EXISTS `chronos_Cost` (
  `cost_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  `reason` text NOT NULL,
  PRIMARY KEY (`cost_id`),
  KEY `project_id_user_id_dept_id` (`project_id`,`user_id`,`dept_id`),
  CONSTRAINT `FK_chronos_Cost_chronos_Projects` FOREIGN KEY (`project_id`) REFERENCES `chronos_Projects` (`project_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- Dumping data for table rpffcouk_projects.chronos_Cost: ~24 rows (approximately)
/*!40000 ALTER TABLE `chronos_Cost` DISABLE KEYS */;
INSERT IGNORE INTO `chronos_Cost` (`cost_id`, `project_id`, `user_id`, `dept_id`, `date`, `amount`, `reason`) VALUES
	(10, 88, 1, 7, '2014-06-23', 370.00, 'Graphite tools for AuSn die attach and stuff'),
	(11, 137, 1, 7, '2014-06-27', 8250.00, '20 position burn-in rack\r\n20 position life test board\r\n1 test box'),
	(12, 35, 28, 7, '2013-03-05', 140.00, 'Lids for prototypes'),
	(13, 122, 27, 7, '2013-04-22', 9372.00, 'Repair of Network Analyser discovered when equipment was calibrated'),
	(14, 174, 27, 7, '2013-06-24', 11.00, 'Bolts'),
	(15, 137, 28, 7, '2013-07-07', 350.00, 'New Burn-In Rack'),
	(16, 5, 26, 7, '2013-07-14', 340.00, 'Camera Stand for alignment stage'),
	(17, 137, 28, 7, '2013-07-24', 1400.00, '50 Trial Packages to solve lidding issue'),
	(18, 55, 30, 7, '2013-08-22', 159.00, 'test box refurb'),
	(19, 179, 30, 7, '2013-09-09', 4150.00, 'New test box'),
	(20, 238, 30, 7, '2013-11-12', 50.00, 'test box'),
	(21, 235, 30, 7, '2013-11-20', 73.00, 'Test box cover'),
	(22, 238, 30, 7, '2013-12-12', 30.00, 'Tap set for DUT socket'),
	(23, 229, 30, 7, '2014-02-26', 51.00, 'UNC fasteners'),
	(24, 225, 30, 7, '2014-03-13', 102.00, 'test box '),
	(25, 52, 30, 7, '2014-03-24', 117.00, 'test'),
	(26, 52, 30, 7, '2014-03-26', 13.00, 'test'),
	(28, 5, 83, 1, '2014-07-07', 200.00, 'Testing stuff'),
	(29, 382, 83, 1, '2014-07-07', 10.00, 'Testing multi 1'),
	(31, 276, 82, 1, '2014-07-07', 10.00, 'cor'),
	(32, 269, 82, 1, '2014-07-07', 15.00, 'Another test one eh'),
	(33, 269, 82, 1, '2014-07-07', 15.00, 'Another test one eh'),
	(34, 192, 1, 14, '2014-07-26', 2.00, 'Some decent reason'),
	(35, 238, 1, 14, '2014-07-26', 33.00, 'Awesome reason');
/*!40000 ALTER TABLE `chronos_Cost` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
